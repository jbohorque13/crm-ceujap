<?php

use Illuminate\Database\Seeder;

class ProfesionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profesiones')->delete();
    	
        DB::table('profesiones')->insert([
		  		['nombre' => 'Gerencia','descripcion' => 'Gerencia'],
		  		['nombre' => 'Ingeniería y Tecnología','descripcion' => 'Ingeniería y Tecnología'],
		  		['nombre' => 'Educación','descripcion' => 'Educación'],
		  		['nombre' => 'Salud','descripcion' => 'Salud'],
		  		['nombre' => 'Derecho','descripcion' => 'Derecho'],
		  		['nombre' => 'Tributos','descripcion' => 'Tributos'],
		  		['nombre' => 'Gastronomía','descripcion' => 'Gastronomía']
        	]);
    }
}
