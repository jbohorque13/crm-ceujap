<?php

use Illuminate\Database\Seeder;

class NucleosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('nucleus')->delete();

        DB::table('nucleus')->insert([
		  		['codigo' => '2017CE','nombre_sede' => 'CEUJAP'],
		  		['codigo' => '2017CA','nombre_sede' => 'Caracas'],
		  		['codigo' => '2017MA','nombre_sede' => 'Maracay'],
		  		['codigo' => '2017MAR','nombre_sede' => 'Maracibo'],
		  		['codigo' => '2017BA','nombre_sede' => 'Barquisimeto'],
		  		['codigo' => '2017PU','nombre_sede' => 'Punto Fijo'],
		  		['codigo' => '2017ME','nombre_sede' => 'Merida']
        	]);
    }
}
