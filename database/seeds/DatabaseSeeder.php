<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserTableSeeder::class);
        //$this->call(QuestionnairesTableSeeder::class);
        $this->call(ProfesionesSeeder::class);
        $this->call(FacilitadoresSeeder::class);
        $this->call(NucleosSeeder::class);
        $this->call(ModulosSeeder::class);
        $this->call(CiclosSeeder::class);
        $this->call(CuestionariosSeeder::class);
        
    }
}
