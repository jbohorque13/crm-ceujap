<?php

use Illuminate\Database\Seeder;

class CiclosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cycles')->delete();

        DB::table('cycles')->insert([
		  		['codigo' => 'C01','descripcion' => 'Ciclo I', 'fecha_inicio' => '2017-03-20','fecha_fin' => '2017-07-10'],
		  		['codigo' => 'C02','descripcion' => 'Ciclo II', 'fecha_inicio' => '2017-07-20','fecha_fin' => '2017-11-10'],
		  		['codigo' => 'C03','descripcion' => 'Ciclo III', 'fecha_inicio' => '2017-11-20','fecha_fin' => '2017-03-10'],
        	]);
    }
}
