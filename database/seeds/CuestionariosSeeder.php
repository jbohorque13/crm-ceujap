<?php

use Illuminate\Database\Seeder;

class CuestionariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionnaires')->delete();

        DB::table('questionnaires')->insert([
		  		[
		  		'pregunta_1' => '¿ La Puntualidad y Cumplimiento del Horario ?',
		  		'pregunta_2' => '¿ La Metodología instruccional ?',
		  		'pregunta_3' => '¿ Manejo de Contenido ?', 
	  			'pregunta_4' => '¿ Claridad para Expresar Ideas ?',
	  		  	'pregunta_5' => '¿ Aclaratoria de Dudas ?',
	  		  	'pregunta_6' => '¿ Trato y Respecto al Participante ?',
	  		  	'pregunta_7' => '¿ Se alcanzaron los Objetivos Académicos ?',
	  		  	'pregunta_8' => '¿ Las Discuciones y Análisis de Casos Fueron Completadas ?',
	  		  	'pregunta_9' => '¿ Las Láminas Utilizadas Facilito Compresión del Contenido ?'
	  		  	]
        	]);
    }
}
