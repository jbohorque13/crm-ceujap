<?php

use Illuminate\Database\Seeder;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->delete();

        DB::table('modules')->insert([
		  		['codigo' => 'M01','descripcion' => 'Modulo 1 de Gerencia', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M02','descripcion' => 'Modulo 1 de Ingeniería y Tecnología', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M03','descripcion' => 'Modulo 1 de Educación', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M04','descripcion' => 'Modulo 1 de Salud', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M05','descripcion' => 'Modulo 1 de Derecho', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M06','descripcion' => 'Modulo 1 de Tributos', 'cohorte' => '2017CRI'],
		  		['codigo' => 'M07','descripcion' => 'Modulo 1 de Gastronomía', 'cohorte' => '2017CRI'],

				['codigo' => 'M08','descripcion' => 'Modulo 2 de Gerencia', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M09','descripcion' => 'Modulo 2 de Ingeniería y Tecnología', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M10','descripcion' => 'Modulo 2 de Educación', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M11','descripcion' => 'Modulo 2 de Salud', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M12','descripcion' => 'Modulo 2 de Derecho', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M13','descripcion' => 'Modulo 2 de Tributos', 'cohorte' => '2017CRII'],
		  		['codigo' => 'M14','descripcion' => 'Modulo 1 de Gastronomía', 'cohorte' => '2017CRII']

        	]);
    }
}
