<?php

use Illuminate\Database\Seeder;

class FacilitadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('facilitators')->delete();

        DB::table('facilitators')->insert([
		  		['nombre_completo' => 'Eduardo Colmenarez','cedula' => '20198756', 'correo' => 'eduardo@hotmail.com', 'profesion_id' => '2', 'foto' => 'img0.jpg'],
		  		['nombre_completo' => 'Alexander Perez','cedula' => '14023658', 'correo' => 'alex@hotmail.com', 'profesion_id' => '4', 'foto' => 'img1.jpg'],
		  		['nombre_completo' => 'Miguel Dias','cedula' => '15247896', 'correo' => 'miguel@hotmail.com', 'profesion_id' => '1', 'foto' => 'img2.jpg'],
		  		['nombre_completo' => 'Juan Alvarez','cedula' => '19587236', 'correo' => 'ja@hotmail.com', 'profesion_id' => '2', 'foto' => 'img3.jpg'],
		  		['nombre_completo' => 'Pepe Peña','cedula' => '19587630', 'correo' => 'pepe@hotmail.com', 'profesion_id' => '3', 'foto' => 'img4.jpg'],
		  		['nombre_completo' => 'Sarah Mendoza','cedula' => '18547630', 'correo' => 'sarah@hotmail.com', 'profesion_id' => '4', 'foto' => 'img5.jpg'],
		  		['nombre_completo' => 'Ana Riquelme','cedula' => '14236985', 'correo' => 'ana@hotmail.com', 'profesion_id' => '2', 'foto' => 'img6.jpg']
        	]);
    }
}
