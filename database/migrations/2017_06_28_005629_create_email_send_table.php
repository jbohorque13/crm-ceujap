<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_send', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('email_id')->unsigned();
            $table->foreign('email_id')->references('id')->on('emails')->nullable(); 

            $table->integer('email_programado_id')->unsigned();
            $table->foreign('email_programado_id')->references('id')->on('emails_programmers')->nullable(); 
            
            $table->date('fecha_envio')->nullable();
            $table->integer('cantidad_envio');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_send');
    }
}
