<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNucleusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nucleus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',70)->unique();
            $table->string('nombre_sede',60)->nullable();
            $table->string('ciudad',60)->nullable();
            $table->enum('estatus',[0, 1])->default(1); // 0 => Desactivado el nucleo; 1=> Activado el nucleo 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nucleus');
    }
}
