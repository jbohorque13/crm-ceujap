<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results_questionnaires', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diplomado_id')->unsigned();
            $table->foreign('diplomado_id')->references('id')->on('diplomados');

            $table->integer('facilitador_id')->unsigned();
            $table->foreign('facilitador_id')->references('id')->on('facilitators');

            $table->integer('modulo_id')->unsigned();
            $table->foreign('modulo_id')->references('id')->on('modules');

            $table->integer('nucleo_id')->unsigned();
            $table->foreign('nucleo_id')->references('id')->on('nucleus');

            $table->decimal('respuesta_1', 10, 3); 
            $table->decimal('respuesta_2', 10, 3);
            $table->decimal('respuesta_3', 10, 3);
            $table->decimal('respuesta_4', 10, 3);
            $table->decimal('respuesta_5', 10, 3);
            $table->decimal('respuesta_6', 10, 3);
            $table->decimal('respuesta_7', 10, 3);
            $table->decimal('respuesta_8', 10, 3);
            $table->decimal('respuesta_9', 10, 3);

            $table->decimal('porcentaje_facilitadores', 10, 3);
            $table->decimal('porcentaje_contenido', 10, 3);

            $table->enum('estatus',[0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('results_questionnaires');
    }
}
