<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
             $table->text('diplomados')->nullable();

            $table->integer('nucleo_id')->unsigned();
            $table->foreign('nucleo_id')->references('id')->on('nucleus')->nullable();
            $table->integer('ciclo_id')->unsigned();
            $table->foreign('ciclo_id')->references('id')->on('cycles')->nullable();

            $table->integer('profesion_id')->unsigned();
            $table->foreign('profesion_id')->references('id')->on('profesiones')->nullable();

            $table->text('usuarios')->nullable(); // Este campo permitira guardar varios email como un string, ejemplo: josue@gmail.com, maria@gmail.com, stringe@gmail.com. ,  
            $table->string('asunto',80);
            $table->string('titulo',80)->nullable();
            $table->text('mensaje');
            
            $table->integer('palabra_clave_id')->unsigned();
            $table->foreign('palabra_clave_id')->references('id')->on('palabras_claves')->nullable();

            
            $table->string('imagen_uno', 220)->nullable();
            $table->string('imagen_dos', 220)->nullable();
            $table->string('imagen_tres', 220)->nullable();
            /* if(email_type==programming)
             * Entonces: send_time tendra un valo y send_finish sino sera NULL y send_finish:vacio
             *  */
            $table->enum('estatus',[0, 1])->default(1); // 0 => Desactivado el envio de este correo; 1=> Activado el envio de este correo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails');
    }
}
