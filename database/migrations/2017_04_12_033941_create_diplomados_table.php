<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiplomadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diplomados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',60)->nullable();
            $table->string('codigo',60);
            $table->text('descripcion')->nullable();
            $table->enum('estatus',[0, 1])->default(1); // 0 => Desactivado el diplomado; 1=> Activado el diplomado 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diplomados');
    }
}
