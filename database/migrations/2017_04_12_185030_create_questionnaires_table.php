<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pregunta_1',120);
            $table->string('pregunta_2',120);
            $table->string('pregunta_3',120)->nullable();
            $table->string('pregunta_4',120)->nullable();
            $table->string('pregunta_5',120)->nullable();
            $table->string('pregunta_6',120)->nullable();
            $table->string('pregunta_7',120)->nullable();
            $table->string('pregunta_8',120)->nullable();
            $table->string('pregunta_9',120)->nullable();
            $table->enum('estatus',[0, 1])->default(1); // 0 => Desactivado el cuestionario; 1=> Activado el cuestionario 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questionnaires');
    }
}
