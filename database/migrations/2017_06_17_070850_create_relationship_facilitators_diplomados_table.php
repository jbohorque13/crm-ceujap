<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipFacilitatorsDiplomadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationship_facilitators_diplomados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diplomado_id')->unsigned();
            $table->foreign('diplomado_id')->references('id')->on('diplomados');
            $table->integer('facilitador_id')->unsigned();
            $table->foreign('facilitador_id')->references('id')->on('facilitators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('relationship_facilitators_diplomados');
    }
}
