<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationshipNucleusDiplomadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationship_nucleus_diplomados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nucleo_id')->unsigned();
            $table->foreign('nucleo_id')->references('id')->on('nucleus');
            $table->integer('diplomado_id')->unsigned();
            $table->foreign('diplomado_id')->references('id')->on('diplomados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('relationship_nucleus_diplomados');
    }
}
