<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_completo',120)->nullable();
            $table->string('cedula',35)->unique();
            $table->string('email',80)->unique();
            $table->string('password');
            $table->string('usuario_facebook',80)->nullable();
            $table->string('usuario_twitter',80)->nullable();
            $table->integer('profesion_id')->unsigned();
            $table->foreign('profesion_id')->references('id')->on('profesiones');
            $table->integer('nucleo_id')->unsigned();
            $table->foreign('nucleo_id')->references('id')->on('nucleus');
            
            $table->string('foto',80)->nullable();
            $table->enum('estatus',[0, 1])->default(1); // 0 => Desactivado el usuario; 1=> Activado el usuario 
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
