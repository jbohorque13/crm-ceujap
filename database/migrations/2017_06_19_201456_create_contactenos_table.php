<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactenosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactenos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asunto',120);
            $table->text('mensaje');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('palabra_clave_id')->unsigned();
            $table->foreign('palabra_clave_id')->references('id')->on('palabras_claves');
            $table->enum('visto',[0, 1])->default(0); // 0 es no visto, 1 si visto 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contactenos');
    }
}
