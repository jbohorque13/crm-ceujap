<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'last_name' => $faker->lastName,
        'cedula' => $faker->uuid,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Questionnaire::class, function (Faker\Generator $faker) {
    return [
        'number_question' => $faker->randomNumber(1),
        'question0' => $faker->text(110),
        'question1' => $faker->text(110),
        'question2' => $faker->text(100),
    ];
});
