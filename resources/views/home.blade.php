@extends('layouts.app')

@section('htmlheader_title')
	Estadistica
@endsection
@section('contentheader_title')
    Estadistica
@endsection

@section('main-content')
 

<div class="row">
    <div class="container">
        <h2 style="margin-bottom: 40px">La gráfica representa la satisfacción de los participantes con respecto a sus  facilitadores en un módulo específico, <strong> Para ver la evaluación que obtuvo un facilitador en particular presione click en el nombre del facilitador que están en la tabla</strong> </h2>
        @if($count_results_questionnaires > 0)
        <div class="col-lg-12 col-md-12">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Evaluación de <strong>  {{ $resultados[0]->facilitador->nombre_completo }} </strong>  en el módulo: <strong> {{ @$resultados[0]->modulo->descripcion }} </strong> aplicando el diplomado: <strong> {{ @$resultados[0]->diplomado->titulo }} </strong> </h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button> 
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-8">
                          <div class="chart-responsive">
                            <canvas id="pieChart" height="202" width="303" style="width: 243px; height: 162px;"></canvas>
                          </div>
                          <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                          <ul class="chart-legend clearfix">
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-red"></i> {{ $preguntas[0]->pregunta_1 }}</li>
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-green"></i> {{ $preguntas[0]->pregunta_2 }}</li>
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-yellow"></i> {{ $preguntas[0]->pregunta_3 }}</li>
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-aqua"></i> {{ $preguntas[0]->pregunta_4 }}</li>
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-light-blue"></i> {{ $preguntas[0]->pregunta_5 }} </li>
                            <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-aqua"></i> {{ $preguntas[0]->pregunta_6 }}</li>
                          </ul>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                    <!-- <div class="box-footer no-padding">
                      <ul class="nav nav-pills nav-stacked">
                        <li><a href="#">A
                          <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                        <li><a href="#">B <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                        </li>
                        <li><a href="#">C
                          <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                      </ul>
                    </div> -->

                  </div>
            </div>
    @endif
    
    </div>
    
    <div class="col-md-12">
        <div id="poll_div"></div>
    </div>

    <!--     <!-- ./col -->
   <!--  <div class="col-lg-4 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $count_results_questionnaires }}</h3>

              <p>Numero de cuestionarios respondidos </p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href=#" class="small-box-footer">Ver mas informacion  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
    </div> -->
   

</div>        
<div class="hidden message-status"></div>
<table class="table table-striped">
	<tr>
		<th>Facilitadores</th>
		<th>Diplomados</th>
		<th>Módulo</th>
		<th>Cohorte</th>
		<th>Núcleo</th>
		<th>Resultado</th>
	</tr>

  @if($count_results_questionnaires > 0)
    @for($i=0; $i < $count_results_questionnaires; $i++)
       @if($i == 0)
            <tr class="facilitador-seleccionado">
                
                <td> <span data-facilitador-id="{{ $resultados[$i]->facilitador->id }}" data-diplomado-id="{{ $resultados[$i]->diplomado->id }}" data-modulo-id="{{ @$resultados[$i]->modulo->id }}" class="facilitador-nombre"> {{ $resultados[$i]->facilitador->nombre_completo }} </span> </td>
                <td> {{ $resultados[$i]->diplomado->titulo }}</td>
                <td> {{ @$resultados[$i]->modulo->descripcion }} </td>
                <td> {{ @$resultados[$i]->modulo->cohorte }} </td>
                <td>  {{ $resultados[$i]->nucleo->nombre_sede }} </td>
                <td> {{ @$resultados[$i]->porcentaje_facilitadores }} % </td>
            </tr>   
       
       @else
            <tr>
                <td> <span data-facilitador-id="{{ $resultados[$i]->facilitador->id }}" data-diplomado-id="{{ $resultados[$i]->diplomado->id }}" data-modulo-id="{{ @$resultados[$i]->modulo->id }}" class="facilitador-nombre">{{ $resultados[$i]->facilitador->nombre_completo }} </span> </td>
                <td> {{ $resultados[$i]->diplomado->titulo }}</td>
                <td> {{ @$resultados[$i]->modulo->descripcion }} </td>
                <td> {{ @$resultados[$i]->modulo->cohorte }} </td>
                <td>  {{ $resultados[$i]->nucleo->nombre_sede }} </td>
                <td> {{ @$resultados[$i]->porcentaje_facilitadores }} % </td>
            </tr>  
       @endif
     
    @endfor
  @endif

</table>



@endsection

@section('push_script')
  <script type="text/javascript">
      
   
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChart       = new Chart(pieChartCanvas);

    var PieData        = [
      {
        value    : '{{ @$cuestionarios[0]->respuesta_1 }}',
        color    : '#f56954',
        highlight: '#f56954',
        label    : '% Satisfacción'
      },
      {
        value    : '{{ @$cuestionarios[0]->respuesta_2 }}',
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : '% Satisfacción'
      },
      {
        value    : '{{ @$cuestionarios[0]->respuesta_3 }}',
        color    : '#f39c12',
        highlight: '#f39c12',
        label    : '% Satisfacción'
      },
      {
        value    : '{{ @$cuestionarios[0]->respuesta_4 }}',
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : '% Satisfacción'
      },
      {
        value    : '{{ @$cuestionarios[0]->respuesta_5 }}',
        color    : '#3c8dbc',
        highlight: '#3c8dbc',
        label    : '% Satisfacción'
      },
      {
        value    : '{{ @$cuestionarios[0]->respuesta_6 }}',
        color    : '#d2d6de',
        highlight: '#d2d6de',
        label    : '% Satisfacción'
      }
    ];
    var pieOptions     = {
      // Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      // String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      // Number - The width of each segment stroke
      segmentStrokeWidth   : 1,
      // Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      // Number - Amount of animation steps
      animationSteps       : 100,
      // String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      // Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      // Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      // Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : false,
      // String - A legend template
      legendTemplate       : '',
      // String - A tooltip template
      tooltipTemplate      : '<%=value %> <%=label%>'
    };
    // Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
    $('.facilitador-nombre').click(function () {
            var diplomado_id = $(this).attr('data-diplomado-id');
            var facilitador_id = $(this).attr('data-facilitador-id');
            var modulo_id = $(this).attr('data-modulo-id');
            var referencia = $(this);

             var dataURL = '{{url("ajax_estadistica_facilitadores")}}';
             //console.log(currency);
             $.ajax({
               url: dataURL,
               data: {
                       'diplomado_id': diplomado_id,
                       'facilitador_id':facilitador_id,
                       'modulo_id':modulo_id,
                       '_token': $('#token').val()
               },
               type: 'post',
               async: false,
               dataType: 'json',
               success: function (result) {
                   if(result.success){
                       $('.facilitador-seleccionado').removeClass('facilitador-seleccionado');
                       referencia.parent().parent().addClass('facilitador-seleccionado'); 
                       
                       $('#pieChart').remove();
                        $('.chart-responsive').html('<canvas id="pieChart" height="156" width="672" style="width: 717px; height: 167px;"></canvas>');
                        $('.box-title').html('Evaluación de <strong>  '+ result.resultados[0].facilitador.nombre_completo +' </strong>  en el modulo: <strong> '+result.resultados[0].modulo.descripcion+' </strong> aplicando el diplomado: <strong> '+result.resultados[0].diplomado.titulo+' ')
                        
                        pieChartCanvas = $('#pieChart').get(0).getContext('2d');
                        pieChart       = new Chart(pieChartCanvas);
                        
                         PieData    = [
                            {
                              value    : result.resultados[0].respuesta_1,
                              color    : '#f56954',
                              highlight: '#f56954',
                              label    : '% Satisfacción'
                            },
                            {
                              value    : result.resultados[0].respuesta_2,
                              color    : '#00a65a',
                              highlight: '#00a65a',
                              label    : '% Satisfacción'
                            },
                            {
                              value    : result.resultados[0].respuesta_2,
                              color    : '#f39c12',
                              highlight: '#f39c12',
                              label    : '% Satisfacción'
                            },
                            {
                              value    : result.resultados[0].respuesta_2,
                              color    : '#00c0ef',
                              highlight: '#00c0ef',
                              label    : '% Satisfacción'
                            },
                            {
                              value    : result.resultados[0].respuesta_2,
                              color    : '#3c8dbc',
                              highlight: '#3c8dbc',
                              label    : '% Satisfacción'
                            },
                            {
                              value    : result.resultados[0].respuesta_2,
                              color    : '#d2d6de',
                              highlight: '#d2d6de',
                              label    : '% Satisfacción'
                            }
                             ];
                            
                            pieChart.Doughnut(PieData, this.pieOptions);
                    }
                    
                   
               },
               error: function (request, error) {

               }
        });
    });

  </script>
@endsection
