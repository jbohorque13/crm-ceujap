<div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"
        id="favoritesModalLabel">Eliminar dato</h4>
      </div>
      <div class="modal-body">
        <br>
        <div id="textModal">
          <h3 class="text text-danger">Estas seguro que desea eliminar este dato?</h3>
        </div>
        <br><br>
        <button type="button"
           class="btn btn-default"
           data-dismiss="modal">Cerrar</button>
        <span class="pull-right">
          <button type="button" class="btn btn-success delete-modal-btn" data-dismiss="modal">
            Eliminar
          </button>
      </div>

    </div>
  </div>
</div>
