<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                </div>
            </div>
        @endif
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li><a href="{{ url('home') }}"> <i class="fa fa-home" aria-hidden="true"></i></i> <span>Inicio</span></a></li>
            
            <li class="treeview">
            <a href="#"<i class="fa fa-book" aria-hidden="true"></i></i> <span>Diplomado</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('ciclos') }}"><i class="fa fa-table" aria-hidden="true"></i> <span>Ciclos </span></a></li>
                    <li><a href="{{ url('nucleos') }}"><i class="fa fa-table" aria-hidden="true"></i> <span>Núcleos </span></a></li>
                    <li><a href="{{ url('modulos') }}"><i class="fa fa-table" aria-hidden="true"></i><span>Módulos </span></a></li>
                    <li><a href="{{ url('diplomados') }}"><i class="fa fa-table" aria-hidden="true"></i> <span>Diplomados </span></a></li>
                </ul>
            </li>
            <li class="treeview"> 
                <a href="#"><i class="fa fa-users" aria-hidden="true"></i> <span>Usuarios </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('participantes') }}"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>Participantes </span></a></li>
                    <li><a href="{{ url('facilitadores') }}"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>Facilitadores </span></a></li>
                </ul>
            </li>
            <li><a href="{{ url('cuestionarios') }}"><i class="fa fa-file-text" aria-hidden="true"></i> <span>Cuestionario</span></a></li>
            
            <li class="treeview">
                <a href="#"><i class="fa fa-pie-chart" aria-hidden="true"></i><span>{{ trans('messages.sidebar.statistics') }} </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ url('estadistica/facilitador') }}"><i class="fa fa-pie-chart" aria-hidden="true"></i> Estadísticas de facilitador<span></span></a></li>
                    <li><a href="{{ url('estadistica/diplomado') }}"><i class="fa fa-pie-chart" aria-hidden="true"></i><span>Estadísticas de diplomado </span></a></li>
 
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i> <span>Contacto o acceso <br> al participante </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('mensajes') }}"><i class="fa fa-envelope-open" aria-hidden="true"></i> <span>Mensajes  </span></a></li>
                    <li><a href="{{ url('mensajes_programados') }}"><i class="fa fa-envelope-open" aria-hidden="true"></i><span>Mensajes Programados </span></a></li>
                </ul>
            </li>   

<!--            <li class="col-md-12"><a href="{{ url('mensajes') }}"><i class='fa fa-link'></i> <span>Contacto o acceso <br> al participante </span></a></li>-->
            
            <li class="col-md-12"><a href="{{ url('sugerencias') }}"><i class="fa fa-eye" aria-hidden="true"></i> <span >Atención y seguimiento <br> al participante</span></a></li>
<!--             <li><a href="#"><i class='fa fa-link'></i> <span>Redes Sociales</span></a></li> -->

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
