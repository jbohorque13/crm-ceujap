<!-- REQUIRED JS SCRIPTS -->


<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>

<script src="{{ asset('/js/show_date.js') }}" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="{{ asset('/plugins/chartjs/Chart.min.js') }}"></script>

<script src="{{ asset('/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/datatables/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/datatables/js/dataTables.fixedHeader.min.js') }}"></script>

<script src="{{ asset('/datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/datatables/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    var table = $('#table').DataTable( {
        responsive: true,
         "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",  
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
@yield('push_script')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->