@extends('layouts.app')

@section('htmlheader_title')
  Crear Facilitador 
@endsection

@section('contentheader_title')
    Crear Facilitador 
@endsection

@section('main-content')
<div class="container">
    <a href="{{ url('facilitadores') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los facilitadores</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
            <div class="panel panel-default">
                <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Facilitador</h2>
                <div class="panel-body panel-facilitador" style="margin-left: 50px;">
                    {!! Form::open(['url' => 'crear_facilitador', 'method' => 'POST', 'files' => true]) !!}
                    {!! Form::token() !!}
                        <div class="col-md-10">
                            {!! Form::label('nombre_completo', 'Nombre Completo') !!} <span class="required"> * </span>
                            <br>
                            {!! Form::text('nombre_completo', null, ['placeholder' => 'Ejemplo: Juan Osorio', 'class' => 'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('nombre_completo') }}</span>
                          </div>
                      	<br>
                          <div class="col-md-10">
                            {!! Form::label('cedula', 'Cedula') !!} <span class="required"> * </span>
                            <br>
                            {!! Form::text('cedula', null, ['placeholder' => 'Ejemplo: 21896345', 'class' => 'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('cedula') }}</span>
                          </div>

                          <div class="col-md-10">
                            {!! Form::label('correo', 'Correo') !!} <span class="required"> * </span>
                            <br>
                            {!! Form::text('correo', null, ['placeholder' => 'Ejemplo: ejemplo@ejemplo', 'class' => 'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('correo') }}</span>
                          </div>
                          <div class="col-md-10">
                            {!! Form::label('profesion', 'Profesion') !!} <span class="required"> * </span> 
                            <br>
                            {!! Form::select('profesion', $profesiones, '', ['class' => 'form-control']) !!}
                          </div>	
                          <div class="col-md-12">
                            {!! Form::label('foto', 'Foto de perfil del facilitador') !!} <span class="required"> * </span>
                            <br>
                            {!! Form::file('foto') !!}
                          </div>
                      <div class="col-md-12">
                          {!! Form::submit('Guardar Facilitador', ['class' => 'btn btn-success']) !!}
                      	</div>
                      	{!! Form::close() !!}
       				 </div>
            </div>
         </div>
    </div>
</div>
@endsection
