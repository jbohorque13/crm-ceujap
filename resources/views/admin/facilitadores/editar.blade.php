@extends('layouts.app')

@section('htmlheader_title')
  Editar Facilitador 
@endsection

@section('contentheader_title')
    Editar Facilitador 
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
          <div class="panel panel-default">
            <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Facilitador</h2>
            <div class="panel-body panel-facilitador" style="margin-left: 50px;">
                {!! Form::open(['url' => "editar_facilitador/$facilitador->id", 'method' => 'POST', 'files' => true]) !!}
                    <div class="col-md-10">
                        {!! Form::label('nombre_completo', 'Nombre Completo') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('nombre_completo', $facilitador->nombre_completo, ['placeholder' => 'Ejemplo: Juan Osorio', 'class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('nombre_completo') }}</span>
                    </div>
                  	<br>
                      <div class="col-md-10">
                        {!! Form::label('cedula', 'Cedula') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('cedula', $facilitador->cedula, ['placeholder' => 'Ejemplo: 21896345', 'class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('cedula') }}</span>
                      </div>

                      <div class="col-md-10">
                        {!! Form::label('correo', 'Correo') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('correo', $facilitador->correo, ['placeholder' => 'Ejemplo: ejemplo@ejemplo', 'class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('correo') }}</span>
                      </div>
                      <div class="col-md-10">
                        {!! Form::label('profesion', 'Profesion') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::select('profesion', $profesiones, $facilitador->profesion->id, ['class' => 'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('profesion') }}</span>
                      </div>	
                      <div class="col-md-12">
                        {!! Form::label('foto', 'Foto de perfil ') !!} <span class="required"> (Si no seleccionas ninguna imagen seguira guardada esta) </span>
                        <br>
                        {!! Form::file('foto') !!} <img src="{{ asset($facilitador->foto) }}" class="responsive" style="width: 100px; height: 80px;"></img>
                      </div>
                  <div class="col-md-12">
                      {!! Form::submit('Editar Facilitador', ['class' => 'btn btn-success']) !!}
                  	</div>
                  	{!! Form::close() !!}
				        </div>
          </div>
         </div>
    </div>
</div>
@endsection
