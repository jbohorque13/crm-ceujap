@extends('layouts.app')

@section('htmlheader_title')
Facilitadores
@endsection
@section('contentheader_title')
Facilitadores
@endsection

@section('main-content')

<a href="{{ url('crear_facilitador') }}" type="button" class="btn btn-success btn-add"> Crear Facilitadores</a>

<div class="hidden message-status"></div> 
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Foto de perfil</th>    
            <th>Nombre Completo</th>
            <th>Cedula</th>
            <th>Profesion</th>
            <th>Correo</th>
<!--            <th>Estatus</th>-->
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_facilitadores > 0)
    <tbody>
        @foreach($facilitadores as $row_facilitadores)
        <tr>
            <td> <img src="{{ asset($row_facilitadores->foto) }}" class="responsive" style="width: 100px; height: 80px;"> </td>
            <td> {{ $row_facilitadores->nombre_completo}}</td>
            <td> {{ $row_facilitadores->cedula }} </td>
            <td> {{ @$row_facilitadores->profesion->nombre }} </td>
            <td> {{ @$row_facilitadores->correo }} </td>
<!--            <td>
                @if($row_facilitadores->estatus == 0)
                <input data-toggle="toggle" data-id="{{ $row_facilitadores->id }}" type="checkbox">
                @elseif($row_facilitadores->estatus == 1)
                <input data-toggle="toggle" data-id="{{ $row_facilitadores->id }}" type="checkbox" checked>
                @endIf
            </td>-->
            <td> <a class="btn btn-xs btn-primary edit_nucleo"  href="{{ url('editar_facilitador/'. @$row_facilitadores->id)  }}"><i class="glyphicon glyphicon-edit"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_facilitadores == 0) 
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
