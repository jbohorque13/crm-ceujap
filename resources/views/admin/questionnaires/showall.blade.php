@extends('layouts.app')

@section('htmlheader_title')
	Cuestionarios
@endsection
@section('contentheader_title')
    Cuestionarios
@endsection

@section('main-content')
@if($numero_cuestionario > 0)
	<a href="{{ url('editar_cuestionario') }}" type="button" class="btn btn-info btn-add"> Modificar cuestionario</a>	
@else
	<a href="{{ url('crear_cuestionario') }}" type="button" class="btn btn-success btn-add"> Crear cuestionario</a>
@endif

<div class="hidden message-status"></div>
<h2 class="title-panel"> Preguntas para evaluar al facilitador</h2>

<table class="table table-striped">
	<tr>
		<th class="col-md-4 text-center"># Preguntas </th>
		<th class="col-md-8 text-center">Pregunta</th>
	</tr>
	@if($numero_cuestionario > 0)
			<tr>
				<td class="text-center"> Pregunta # 1</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_1 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 2</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_2 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 3</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_3 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 4</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_4 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 5</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_5 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 6</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_6 }} </td>
			</tr>
	@endif
</table>
@if($numero_cuestionario == 0)
	<p class="container table-nothing">No hay datos</p>
@endif
<h2 class="title-panel"> Preguntas para evaluar al contenido</h2>
<table class="table table-striped">
	<tr>
		<th class="col-md-4 text-center"># Preguntas </th>
		<th class="col-md-8 text-center">Pregunta</th>
	</tr>
	@if($numero_cuestionario > 0)
			<tr>
				<td class="text-center"> Pregunta # 7</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_7 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 8</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_8 }} </td>
			</tr>
			<tr>
				<td class="text-center"> Pregunta # 9</td>
				<td class="text-center"> {{ $cuestionarios[0]->pregunta_9 }} </td>
			</tr>
				
	@endif
</table>
@include('modals.delete')

{!! $cuestionarios->render() !!}
@if($numero_cuestionario == 0)
	<p class="container table-nothing">No hay datos</p>
@endif

@endsection

@section('push_script')
    <script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection

