@extends('layouts.app')

@section('htmlheader_title')
  Crear Cuestionario
@endsection


@section('main-content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
      
			<div class="panel panel-default">
				<div class="panel-body panel-questionnaire">
          <h2 class="title-panel"> Evaluacion para Facilitadores </h2  >
          {!! Form::open(['url' => 'crear_cuestionario', 'method' => 'POST']) !!}
              {!! Form::token() !!}
              
              @for($i=1; $i <= $numero_de_preguntas; $i++)
                @if($i == 7)
                    <hr>
                    <h2 class="title-panel col-md-12" style="margin-top:40px;"> Evaluacion para el Contenido </h2>

                    <div class="form-group col-md-6">
                      {!! Form::label('pregunta_'.$i, 'Pregunta #'.($i) ) !!}
                      <br>
                      {!! Form::text('pregunta_'.$i, null,['class' => 'col-md-12']) !!}
                    </div>
                @else
                    <div class="form-group col-md-6">
                      {!! Form::label('pregunta_'.$i, 'Pregunta #'.($i) ) !!}
                      <br>
                      {!! Form::text('pregunta_'.$i, null,['class' => 'col-md-12']) !!}
                    </div>
                @endif
                
              @endfor
              
              <div class="clearfix"></div>
              <br>
              <div class="form-group col-md-12">
                {!! Form::submit('Crear Cuestionario', ['class' => 'btn btn-success']) !!}
              </div>

          {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
