@extends('layouts.app')

@section('htmlheader_title')
Sugerencias
@endsection
@section('contentheader_title')
Sugerencias
@endsection

@section('main-content')
<div class="col-md-12">
    <a href="{{ url('palabras_claves') }}" type="button" class="btn btn-success" style="margin-bottom: 50px; margin-top: 20px; font-size: 1.5em;"> Agregar palabra clave para filtrar los correos recibidos</a>
</div>

<div class="col-md-12  hidden message-status"></div> 
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nombre del remitente</th>
            <th>Asunto</th>
            <th>Palabra clave</th>
            <th>Fecha Recibido</th>
            <th>Visto</th>
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_sugerencias > 0)
    <tbody>
        @foreach($sugerencias as $row_sugerencias)
        <tr>
            <td> {{ $row_sugerencias->user->nombre_completo }}</td>
            <td> {{ $row_sugerencias->asunto }}</td>
            <td> {{ @$row_sugerencias->palabras_claves->nombre }}</td>
            <td> {!! $row_sugerencias->created_at !!} </td>
            <td>
                @if($row_sugerencias->visto == 0)
                <i class="fa fa-envelope" aria-hidden="true"></i>
                @elseif($row_sugerencias->visto == 1)
                <i class="fa fa-envelope-open" aria-hidden="true"></i>
                @endIf
            </td>
            <td> <a class="btn btn-xs btn-primary ver-sugerencia" title="Ver sugerencia" href="{{ url('ver_sugerencia',$row_sugerencias->id ) }}"><i class="fa fa-eye"></i></a>

                <a data-id="{{ $row_sugerencias->id }}" class="btn btn-xs btn-danger" data-toggle="modal" accesskey="" data-target="#ModalDelete"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_sugerencias == 0)
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
