@extends('layouts.app')

@section('htmlheader_title')
	Ver Sugerencia
@endsection
@section('contentheader_title')
    Ver Sugerencia
@endsection

@section('main-content')



<div class="container">
<a href="{{ url('sugerencias') }}" class="btn btn-info"> Regresar</a>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">Asunto {{ $sugerencia->asunto }}</div>

          <div class="panel-body">
          <h2> <strong> Persona quien envio el mensaje: </strong> {{ $sugerencia->user->nombre_completo }} </h2>
            <h2> <strong> Mensaje: </strong> {{ $sugerencia->mensaje }} </h2>
          </div>
        </div>
    </div>
    </div>
  </div>

@endsection

@section('push_script')

@endsection
