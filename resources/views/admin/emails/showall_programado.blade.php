@extends('layouts.app')

@section('htmlheader_title')
    Mensajes Programados
@endsection
@section('contentheader_title')
    Mensajes Programados
@endsection

@section('main-content')

<a href="{{ url('envio_mensaje_programado') }}" type="button" class="btn btn-success btn-add"> Enviar nuevo mensaje Programado</a>

@if(session()->has('msg-correo'))
        <div class="alert {{ session()->get('correo-class') }} alert-dismissable">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session()->get('msg-correo') }} 
        </div>
@endif

<div class="hidden message-status"></div>
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
	<tr>
            <th>Titulo</th>
            <th>Frecuencia </th>
            <th>Fecha de inicio </th>
            <th>Frecuencia de Cierre</th>
            <th>Estatus</th>
            <th>Ver correo</th>
	</tr> 
    </thead>
    @if($emails_count > 0)
        <tbody>
            @foreach($emails as $email) 
            <tr>
                <td> {{ $email->asunto}}</td>
                <td> {{ $email->frecuencia }}</td> 
                <td> {{ $email->fecha_inicio }}</td>
                <td> {{ $email->fecha_cierre }}</td>
                @if($email->estatus == 1)
                    <td> Fue enviado al menos una vez</td>
                @else
                    <td>No ha sido enviado </td>
                @endif  
                <td> <a class="btn btn-xs btn-primary ver-correo" title="Ver Mensaje Programado" href="{{ url('ver_mensaje_programado',$email->id ) }}"><i class="fa fa-eye"></i></a>
                         <a class="btn btn-xs btn-primary" title="Reenviar Mensaje" href="{{ url('reenviar_mensaje_programado',$email->id ) }}"><i class="fa fa-reply" aria-hidden="true"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody> 
    @endif
</table>
@include('modals.delete') 

@endsection

@section('push_script')
    <script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
