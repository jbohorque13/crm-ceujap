@extends('layouts.app')

@section('htmlheader_title')
  Envio de Mensaje
@endsection

@section('contentheader_title')
    Generar Información con Programación
@endsection

@section('main-content')

<div class="container">
    <a href="{{ url('mensajes_programados') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los mensajes enviados</a>
    <div class="row">
        <div class="col-md-12">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
      <div class="container" style="margin-bottom: 50px; padding: 0 20%;">
          <div class="col-md-6">
              <a href="{{ url('envio_mensaje') }}" class="btn btn-danger" style="padding: 5% 10%;">
                  Generar Información sin Programación
              </a>
          </div>
          <div class="col-md-6">
              <a href="{{ url('envio_mensaje_programado') }}" class="btn btn-danger active" style="padding: 5% 10%;">
                  Generar Información con Programación
              </a>
          </div>    
      </div>
        <div class="panel panel-default">
            <div class="panel-body panel-email">
                <h2 class="title-panel text-center" style="margin-top: 50px;"> Envio de Correo Programado</h2>
                {!! Form::open(['url' => 'enviar_mensaje_programado', 'method' => 'POST', 'files' => true]) !!}
                <div class="col-md-12">
                    <div class="col-md-12 line"></div>
                    <h2 class="title-panel col-md-12" style="margin-top: 50px; display: block;"> Filtros para el correo </h2>
                    <div class="form-group col-md-12">
                        {!! Form::label('ciclo_id', 'Ciclo para el cual quiere enviar ') !!}<strong style="color:red;"> (no es obligatorio) </strong>
                        <br><br>
                        {!! Form::select('ciclo_id', @$ciclos, '', ['class' => 'col-md-12 form-control', 'id' => 'ciclo_id', 'placeholder' => 'Seleccionar...']) !!}
                        <br><br>
                    </div>

                    <div class="form-group col-md-12">
                        {!! Form::label('nucleo_id', 'Nucleo para el cual quiere enviar') !!} <strong style="color:red;"> (no es obligatorio) </strong>
                        
                        <br><br>
                        {!! Form::select('nucleo_id', @$nucleos, '', ['class' => 'col-md-12 form-control', 'id' => 'nucleo_id', 'placeholder' => 'Seleccionar...']) !!}
                    </div>

                    <div class="form-group col-md-12 diplomado_id hidden">
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="diplomados">
                          
                    </div>
                </div>

                <div class="col-md-12 line"></div>

                <h2 class="title-panel col-md-12" style="margin-top: 50px; display: block;"> Información para el correo </h2>

                <div class="col-md-12">
                    <div class="form-group col-md-12">
                        {!! Form::label('titulo', 'Escribe el titulo de este correo') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('titulo', null, ['class' => 'col-md-12 form-control', 'id' => 'titulo']) !!}
                        <span class="text-danger">{{ $errors->first('titulo') }}</span>
                    </div>

                    <div class="form-group col-md-12">
                        {!! Form::label('asunto', 'Escribe el asunto de este correo') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('asunto', null,['class' => 'col-md-12 form-control', 'id' => 'asunto']) !!}
                        <span class="text-danger">{{ $errors->first('asunto') }}</span>
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('mensaje', 'Escribe el mensaje del correo que deseas enviar') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::textarea('mensaje', null,['class' => 'col-md-12 form-control', 'id' => 'mensaje']) !!}
                        <span class="text-danger">{{ $errors->first('mensaje') }}</span>
                    </div>
                    <div class="form-group col-md-12"> 
                        {!! Form::label('imagen_uno', 'Envia una imagen si deseas') !!}
                        <br>
                        {!! Form::file('imagen_uno', null,['class' => 'col-md-12 form-control', 'id' => 'imagen_uno']) !!}
                    </div>
                    
                    <div class="form-group col-md-12">
                        <h2 class="title-panel text-center" style="margin-top: 20px; margin-bottom: 20px!important;">Programación </h2>  
                         <p class="title-panel text-center" style="color:red; font-size: 1.2em;">(Estos campos son estrictamente requeridos) </p>
                         <div class="col-md-4">
                            {!! Form::label('frecuencia', 'Selecciona la frecuencia con la que se enviará el correo') !!}
                            <br>
                            {!! Form::select('frecuencia', ['hora' => 'Por Hora', 'dia' => 'Por Dia', 'semana' => 'Por Semana'], '', ['class' => 'col-md-12 form-control', 'id' => 'frecuencia', 'placeholder' => 'Seleccionar...']) !!}
                            <span class="text-danger col-md-12">{{ $errors->first('frecuencia') }}</span>
                         </div>
                         <div class="col-md-4">
                             {!! Form::label('fecha_inicio', 'Selecciona la fecha de inicio donde a partir de ahí se enviará los correos programados') !!}
                            <br>
                            {!! Form::date('fecha_inicio') !!}
                            <span class="text-danger col-md-12">{{ $errors->first('fecha_inicio') }}</span>
                         </div>
                         <div class="col-md-4">
                             {!! Form::label('fecha_cierre', 'Selecciona la fecha de cierre donde se dejará de enviar los correos programados') !!}
                            <br> 
                            {!! Form::date('fecha_cierre') !!}
                            <span class="text-danger col-md-12">{{ $errors->first('fecha_cierre') }}</span>
                         </div>
                         </div> 
                        
                    </div>
                    
                    <div class="clearfix"></div>
                    <br>
                    <div class="form-group col-md-12">
                        
                        <div class="col-md-6">
                            {!! Form::submit('Enviar Correo', ['class' => 'btn btn-success col-md-5']) !!}   
                        </div>
                        <div class="col-md-6 btn-redes-sociales-container">
                            <div class="col-md-6">
                                <button type="button" disabled class="btn btn-facebook"> <i class="fa fa-facebook-square" aria-hidden="true"></i>  Compartir Imagen Con Facebook </button> 
                            </div>
                            <div class="col-md-6">
<!--                                <a class="twitter-share-button"
                                    href="https://twitter.com/share"
                                    data-size="large"
                                    data-text="custom share text"
                                    data-url="https://dev.twitter.com/web/tweet-button"
                                    data-hashtags="example,demo"
                                    data-via="twitterdev"
                                    data-related="twitterapi,twitter">
                                  Tweet
                                  </a>-->
                                <a href="https://twitter.com/share?text=" disabled target="_blank" class="btn btn-twitter twitter popup"> <i class="fa fa-twitter" aria-hidden="true"></i>  Compartir Mensaje Con Twitter </a>
                            </div>
                        </div>
                    </div>
                    

                </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('push_script')
  <script type="text/javascript">
        $('#imagen_uno').change(function(){
         console.log($(this).val());
         if($(this).val() != ''){
              $('.btn-facebook').removeAttr('disabled');
         }
         else{
                $('.btn-facebook').attr('disabled', ''); 
            }
      });
        $('#mensaje').keyup(function() { 
            if($(this).val() != ''){
                $('.btn-twitter').removeAttr('disabled', '');
            }
            else{
                $('.btn-twitter').attr('disabled', '');
            }
           $('.btn-twitter').attr('href', 'https://twitter.com/intent/tweet?text=' + $(this).val()); 
        });
        
        $('.btn-twitter').click(function () {
           var href_twitter = $(this).attr('href'); 
           $(this).attr('href', href_twitter+'&hashtags=Eficiencia,Diplomas&screen_name=LA_UJAP'); 
        });
        $(document).on('change', '#nucleo_id', function(){
            var nucleo_id = $(this).val();
            console.log(nucleo_id);

          var dataURL = '{{url("ajax_get_diplomados")}}';
          //console.log(currency);
          $.ajax({
            url: dataURL,
            data: {
                'nucleo_id': nucleo_id,
                '_token': $('input[name="_token"]').val()
            },
            type: 'post',
            async: false,
            dataType: 'json',
            success: function (result) {
              console.log(result);
              if(result.success){
                $('.diplomado_id').addClass('hidden');

                if(result.diplomados.length == 0){
                    $('.diplomado_id').html('');
                    $('.diplomado_id').html('<p> No hay ningun diplomado registrado para este nucleo</p><br>').removeClass('hidden');
                    console.log('cdcsd');
                }
                else{
                    $('.diplomado_id').removeClass('hidden');

                    $('.diplomado_id').html('<label for="diplomado_id"> Diplomado(s) <strong style="color:red;"> (no es obligatorio) </strong></label><br>');

                    for(var i=0; i < result.diplomados.length; i++){
                        $('.diplomado_id').append('<input type="checkbox" name="diplomado_id[]" value="'+ result.diplomados[i].diplomados[0].id +'">'+ result.diplomados[i].diplomados[0].titulo);
                    }
                }

              }
            },
            error: function (request, error) {

            }
          });
      });
      $('.btn-facebook').click(function(){
//        FB.login(function(){
//            // Note: The call will only work if you accept the permission request
//            FB.api('/me/feed', 'post', {message: '  Titulo: ' + $('#titulo').val() +'    '+ $('#mensaje').val() +'  ' });  
//        }, {scope: 'publish_actions'});  
    var image = '{{ url("images/emails/13/1498424677_baner.jpg") }}';
    
    console.log(image);
         FB.ui(
            {
              method: 'feed',
              name: 'This is the content of the "name" field.',
              picture: 'http://sinimagen.com/crm/public/images/facilitadores/5/1498314462_tomas.jpg', 
              caption: 'Caption like which appear as title of the dialog box',
              description: 'Small description of the post',
              message: 'khcbdsjkcbdsjcbdjkscbdjksbcjkdsbckjbsdckjbdscjkbdskjcbd'
            },
            function(response) {
                if (response && response.post_id) {
                  alert('Post was published.');
                } else {
                  alert('Post was not published.');
                }
            }
          );
          $('.innerWrap textarea').html($('#mensaje').val()); 
    });
  </script>
@endsection