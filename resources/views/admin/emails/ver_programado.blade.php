@extends('layouts.app')

@section('htmlheader_title')
Ver Correo Programado
@endsection
@section('contentheader_title')
Ver Correo Programado
@endsection

@section('main-content')



<div class="container">
    <a href="{{ url('mensajes_programados') }}" class="btn btn-info"> Regresar</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Asunto {{ $correo->asunto }}</div>

                <div class="panel-body">
                    <h2 style="margin-bottom: 20px;"> <strong> Titulo del mensaje:  </strong> {{ $correo->titulo }} </h2>

                    <h2> <strong> Persona a quien fue enviado este correo: </strong> {{ $correo->usuarios }} </h2>
                    <h2> <strong> Mensaje: </strong> {{ $correo->mensaje }} </h2>

                    @if(!empty($correo->imagen_uno))
                    <hr>
                    <h2 class="text-center"> Imagen </h2>
                    <img class="img-responsive" src="{{ url($correo->imagen_uno) }}"> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('push_script')

@endsection
