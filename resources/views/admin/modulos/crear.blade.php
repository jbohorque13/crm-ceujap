@extends('layouts.app')

@section('htmlheader_title')
Crear Modulo 
@endsection

@section('contentheader_title')
Crear Modulo 
@endsection

@section('main-content')
<div class="container">
    <a href="{{ url('modulos') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los modulos</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(session()->has('messageError'))
            <div class="alert alert-danger"> {{ session('messageError') }}</div>
            @endif
            <div class="panel panel-default">
                <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Modulo</h2>
                <div class="panel-body panel-modulo" style="margin-left: 50px;">
                    {!! Form::open(['url' => 'crear_modulo', 'method' => 'POST']) !!}
                    {!! Form::token() !!}
                    <div class="col-md-10">
                        {!! Form::label('codigo', 'Codigo') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('codigo') }}</span>
                    </div>
                    <br>
                    <div class="col-md-10">
                        {!! Form::label('descripcion', 'Descripcion') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('descripcion') }}</span>
                    </div>
                    <div class="col-md-10">
                        {!! Form::label('cohorte', 'Cohorte ') !!} <span class="required"> * </span>
                        <br>
                        {!! Form::text('cohorte', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('cohorte') }}</span>
                    </div>
                    <br>

                    <br>
                    <div class="col-md-12">
                        {!! Form::submit('Guardar modulo', ['class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
