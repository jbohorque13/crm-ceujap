@extends('layouts.app')

@section('htmlheader_title')
Modulos
@endsection
@section('contentheader_title')
Modulos
@endsection

@section('main-content')

<a href="{{ url('crear_modulo') }}" type="button" class="btn btn-success btn-add"> Crear Modulo</a>

<div class="hidden message-status"></div>
    <table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Cohorte</th>
<!--                <th>Estatus</th>-->
                <th>Acciones</th>
            </tr>
        </thead>
        @if($numero_modulo > 0)
        <tbody>
            @foreach($modulos as $row_modulos)
            <tr>
                <td> {{ $row_modulos->codigo}}</td>
                <td> {{ $row_modulos->descripcion}}</td>
                <td> {!! $row_modulos->cohorte !!} </td>
<!--                <td>
                    @if($row_modulos->estatus == 0)
                    <input data-toggle="toggle" data-id="{{ $row_modulos->id }}" type="checkbox">
                    @elseif($row_modulos->estatus == 1)
                    <input data-toggle="toggle" data-id="{{ $row_modulos->id }}" type="checkbox" checked>
                    @endIf
                </td>-->
                <td> <a class="btn btn-xs btn-primary edit_modulo" href="{{ url('editar_modulo/' . $row_modulos->id) }}"><i class="glyphicon glyphicon-edit"></i></a>

          <!-- <a data-id="{{ $row_modulos->id }}" class="btn btn-xs btn-danger" data-toggle="modal" accesskey="" data-target="#ModalDelete"><i class="glyphicon glyphicon-trash"></i></a> -->
                </td>
            </tr>
            @endforeach
        </tbody>
        @endif
    </table>

    @include('modals.delete')
    @if($numero_modulo == 0)
    @endif

    @endsection

    @section('push_script')
    <script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
    @endsection
