@extends('layouts.app')

@section('htmlheader_title')
  Editar Modulo 
@endsection

@section('contentheader_title')
    Editar Modulo 
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Modulo</h2>
                    <div class="panel-body panel-modulo" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'editar_modulo/'.$modulos->id, 'method' => 'POST']) !!}
                            <div class="col-md-10">
                                {!! Form::label('codigo', 'Codigo') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('codigo', $modulos->codigo, ['class' => 'form-control']) !!}
                              </div>
                          <br>
                              <div class="col-md-10">
                                {!! Form::label('descripcion', 'Descripcion') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::textarea('descripcion', $modulos->descripcion, ['class' => 'form-control']) !!}
                              </div>
                           
                              <div class="col-md-10">
                                {!! Form::label('cohorte', 'Cohorte') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('cohorte',$modulos->cohorte, ['class' => 'form-control']) !!}
                              </div>
                          <br>
                          
                        <br>
                        <div class="col-md-12">
                            {!! Form::submit('Editar Modulo', ['class' => 'btn btn-success']) !!}
                          </div>
                        {!! Form::close() !!}
                    </div>
                </div>
         </div>
    </div>
</div>
@endsection
