@extends('layouts.app')

@section('htmlheader_title')
  Crear Participante 
@endsection

@section('contentheader_title')
    Crear Participante 
@endsection

@section('main-content')
<div class="container">
  <a href="{{ url('participantes') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los participantes</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Participante</h2>
                    <div class="panel-body panel-participante" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'crear_participante', 'method' => 'POST']) !!}

                          <div class="col-md-6">
                            <div class="col-md-12">
                                {!! Form::label('nombre_completo', 'Nombre Completo') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('nombre_completo', null, ['placeholder' => 'Ejemplo: Juan Osorio', 'class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('nombre_completo') }}</span>
                            </div>
                                
                            <br>
                              <div class="col-md-12">
                                {!! Form::label('cedula', 'Cedula') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('cedula', null, ['placeholder' => 'Ejemplo: 21036987', 'class' => 'form-control']) !!} 
                                <span class="text-danger">{{ $errors->first('cedula') }}</span>
                              </div>
                            
                              <div class="col-md-12">
                                {!! Form::label('email', 'Correo') !!}
                                <br> 
                                {!! Form::text('email', null, ['placeholder' => 'Ejemplo: ejemplo@ejemplo.com', 'class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                              </div>
                                
                              <div class="col-md-12">
                                {!! Form::label('profesion', 'Profesión') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::select('profesion', $profesiones, '', ['class' => 'form-control', 'placeholder' => 'Seleccione...']) !!}
                                <span class="text-danger">{{ $errors->first('profesion') }}</span>
                              </div>

                              <div class="col-md-12">
                                {!! Form::label('diplomado_id', 'Diplomado') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::select('diplomado_id', $diplomados, '', ['id' => 'diplomado_id', 'placeholder' => 'Seleccione...', 'class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('diplomado_id') }}</span>
                              </div>
                                
                            </div>
                            <div class="col-md-6">
<!--                                   <div class="col-md-12">
                                  {!! Form::label('usuario_facebook', 'Nombre De Como Sale En Facebook') !!}
                                  <br>
                                  {!! Form::text('usuario_facebook') !!}
                                </div>
                              <br>
                                <div class="col-md-12">
                                  {!! Form::label('usuario_twitter', 'Nombre De Como Sale En Twitter') !!}
                                  <br>
                                  {!! Form::text('usuario_twitter') !!}
                                </div>
 -->
                                <div class="col-md-12 nucleos">
                                  
                                </div>

                            </div>
                          <div class="col-md-12">
                            @if(count($diplomados) > 0)
                              {!! Form::submit('Guardar Participante', ['class' => 'btn btn-success']) !!}
                            @else
                              <p>No tienes ningun diplomado creado, crea primero un diploma y luego agregas al participante</p>
                              <a href="{{ url('crear_diplomado') }}" class="btn btn-info"> Crear Diplomado </a>
                            @endif
                              
                            </div>
                            {!! Form::close() !!}
                        </div>
                </div>
         </div>
    </div>
</div>
@endsection
@section('push_script')
  <script type="text/javascript">
  console.log('hola');
      $(document).on('change', '#diplomado_id', function(){
        var diplomado_id = $(this).val();
        console.log(diplomado_id);

        var dataURL = '{{url("ajax_get_nucleos")}}';
        //console.log(currency);
        $.ajax({
          url: dataURL,
          data: {
            'diplomado_id': diplomado_id,
            '_token': $('input[name="_token"]').val()
          },
          type: 'post',
          async: false,
          dataType: 'json',
          success: function (result) {
            console.log(result);
            console.log(result.success);
            if(result.success){
              $('.nucleos').html('');
              if(result.nucleos.length > 0){
                $('.nucleos').append('<strong style="1em; display: block;"> Nucleo(s) </strong>');
                for(var i=0; i < result.nucleos.length; i++ ){
                  $('.nucleos').append(
                    '<div class="col-md-6">'
                        +'<input name="nucleo_id" type="radio" value="'+ result.nucleos[i].nucleus[0].id +'">'
                        +'<span style="margin-left: 10px; font-weight: 600;"> '+ result.nucleos[i].nucleus[0].nombre_sede +'</span>' 
                    +'</div>'
                  );    
                }
              }
            }
          },
          error: function (request, error) {

          }

        });
      });
  </script>
@endsection()
