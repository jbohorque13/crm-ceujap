@extends('layouts.app')

@section('htmlheader_title')
  Crear Participante 
@endsection

@section('contentheader_title')
    Crear Participante 
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Participante</h2>
                    <div class="panel-body panel-participante" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'editar_participante/'.$usuarios->id, 'method' => 'POST']) !!}
                          <div class="col-md-6">
                            <div class="col-md-12">
                                {!! Form::label('nombre_completo', 'Nombre Completo') !!}
                                <br>
                                {!! Form::text('nombre_completo', $usuarios->nombre_completo) !!}
                                <span class="text-danger">{{ $errors->first('nombre_completo') }}</span>
                              </div>
                            <br>
                              <div class="col-md-12">
                                {!! Form::label('cedula', 'Cedula') !!}
                                <br>
                                {!! Form::text('cedula', $usuarios->cedula, ['placeholder' => 'Ejemplo: 24472145']) !!}
                                <span class="text-danger">{{ $errors->first('cedula') }}</span>
                              </div>

                              <div class="col-md-12">
                                {!! Form::label('email', 'Correo') !!}
                                <br>
                                {!! Form::text('email', $usuarios->email, ['placeholder' => 'ejemplo@ejemplo.com']) !!}
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                              </div>
                              <div class="col-md-12">
                                {!! Form::label('profesion', 'Profesion') !!}
                                <br>
                                {!! Form::select('profesion', $profesiones, $usuarios->profesion) !!}
                              </div>

                              <div class="col-md-12">
                                {!! Form::label('diplomado_id', 'Diplomado') !!}
                                <br>
                                {!! Form::select('diplomado_id', $diplomados) !!}
                              </div>  
                            </div>
                            <div class="col-md-6">
                                <!--   <div class="col-md-12">
                                  {!! Form::label('usuario_facebook', 'Nombre De Como Sale En Facebook') !!}
                                  <br>
                                  {!! Form::text('usuario_facebook', $usuarios->usuario_facebook) !!}
                                </div>
                              <br>
                                <div class="col-md-12">
                                  {!! Form::label('usuario_twitter', 'Nombre De Como Sale En Twitter') !!}
                                  <br>
                                  {!! Form::text('usuario_twitter', $usuarios->usuario_twitter) !!}
                                </div>
 -->
                            </div>
                          <div class="col-md-12">
                            @if(count($diplomados) > 0)
                              {!! Form::submit('Guardar Participante', ['class' => 'btn btn-success']) !!}
                            @else
                              <p>No tienes ningun diplomado creado, crea primero un diploma y luego agregas al participante</p>
                              <a href="{{ url('crear_diplomado') }}" class="btn btn-info"> Crear Diplomado </a>
                            @endif
                              
                            </div>
                            {!! Form::close() !!}
                        </div>
                </div>
         </div>
    </div>
</div>
@endsection
