@extends('layouts.app')

@section('htmlheader_title')
Participante
@endsection
@section('contentheader_title')
Participante
@endsection

@section('main-content')

<a href="{{ url('crear_participante') }}" type="button" class="btn btn-success btn-add"> Crear Un Nuevo Participante</a>

<div class="hidden message-status"></div> 
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Cedula</th>
            <th>Profesion</th>
            <th>Correo</th>
            <th>Fecha de Registrado</th>
<!--            <th>Estatus</th>-->
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_usuarios > 0)
    <tbody>
        @foreach($usuarios as $row_usuarios)
        <tr>
            <td> {{ $row_usuarios->nombre_completo}}</td>
            <td> {{ $row_usuarios->cedula }} </td>
            <td> {{ @$row_usuarios->profesion->nombre }} </td>
            <td> {{ @$row_usuarios->email }} </td>
            <td> {{ @$row_usuarios->created_at }} </td>
<!--            @if(session()->has('clave_usuario'))
            <td> {{ session()->get('clave_usuario') }} </td>
            @endif-->

<!--            <td>
                @if($row_usuarios->estatus == 0)
                <input data-toggle="toggle" data-id="{{ $row_usuarios->id }}" type="checkbox">
                @elseif($row_usuarios->estatus == 1)
                <input data-toggle="toggle" data-id="{{ $row_usuarios->id }}" type="checkbox" checked>
                @endIf
            </td>-->
            <td> <a class="btn btn-xs btn-primary edit_nucleo"  href="{{ url('editar_participante/'. @$row_usuarios->id)  }}"><i class="glyphicon glyphicon-edit"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_usuarios == 0)
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
