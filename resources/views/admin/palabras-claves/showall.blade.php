@extends('layouts.app')

@section('htmlheader_title')
	Palabras Claves
@endsection
@section('contentheader_title')
    Palabras Claves
@endsection

@section('main-content')

<div class="container">
<a href="{{ url('sugerencias') }}" class="btn btn-info"> Regresar a las sugerencias recibidas</a>
<h2>Estás son las palabras claves que has agregado hasta ahora, si quieres puedes agregar más <a href="{{ url('crear_palabra_clave') }}"> aquí </a></h2>
 
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">Palabras Claves </div>

          <div class="panel-body">
            <div class="list-group">
            @if($numero_palabras_claves > 0)
              @for($i=0; $i < $numero_palabras_claves; $i++)
                   <span  class="list-group-item"><strong style="font-size: 1.5em;"> {{ $i + 1 }} - {{ $palabras_claves[$i]->nombre }} </strong>  </span>
              @endfor
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection


