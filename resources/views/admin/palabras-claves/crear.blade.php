@extends('layouts.app')

@section('htmlheader_title')
  Crear Palabra Clave 
@endsection

@section('contentheader_title')
    Crear Palabra Clave 
@endsection

@section('main-content')
<div class="container">
<a href="{{ url('palabras_claves') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver las palabras claves</a>
<br>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Palabra Clave</h2>
                    <div class="panel-body panel-palabra_clave" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'crear_palabra_clave', 'method' => 'POST']) !!}
                        {!! Form::token() !!}
                          <div class="col-md-12">
                              {!! Form::label('nombre', 'Sea Específico a la hora de escribir la palabra clave') !!}
                              <br>
                              {!! Form::text('nombre') !!} <span> Ejemplo: Sugerencia, Inquietud </span>
                          </div>
                          <br>
	                        <div class="col-md-12">
	                            {!! Form::submit('Guardar Palabra Clave ', ['class' => 'btn btn-success']) !!}
                        	</div>
                      	 {!! Form::close() !!}
       				 </div>
                </div>
         </div>
    </div>
</div>
@endsection
