@extends('layouts.app')

@section('htmlheader_title')
	Inicio
@endsection
@section('contentheader_title')
    Inicio
@endsection

@section('main-content')

@if(session()->has('msg-nucleo'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
        {{ session()->get('msg-nucleo') }}
    </div>
@endif


<div class="container">
    <img class="img-responsive" src="{{ asset('img/banner.jpg') }}" style="margin-bottom: 50px;">

    <div class="row">
      <div class="col-md-11">
        <div class="panel panel-default">
          <div class="panel-heading"> <h1> Inicio </h1></div>

          <div class="panel-body">
            <h1>  Bienvenido al modulo de CRM </h1>
            
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-xs-5">
          <div class="small-box bg-yellow">
            <div class="inner">

              <h3> <i class="fa fa-check-square" aria-hidden="true"></i>  {{ @$count_notification_questionnaires }}</h3>

              <p>Numero de cuestionarios respondidos hoy </p>
            </div>
            <a href="{{ url('estadistica/facilitador') }}" class="small-box-footer">Ver mas informacion  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
      </div> 
      <div class="col-lg-6 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner"> 

              <h3> <i class="fa fa-check-square" aria-hidden="true"></i>  {{ @$count_contactenos }}</h3>

              <p>Numero de sugerencia recibidas hoy </p>
            </div>
            <a href="{{ url('sugerencias') }}" class="small-box-footer">Ver mas informacion  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
      </div> 

    </div>
  </div>

@endsection

@section('push_script')
<script>
    $(".alert").alert()
</script>
@endsection
