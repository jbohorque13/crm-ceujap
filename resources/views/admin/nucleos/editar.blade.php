@extends('layouts.app')

@section('htmlheader_title')
Editar Nucleo 
@endsection

@section('contentheader_title')
Editar Nucleo 
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(session()->has('messageError'))
            <div class="alert alert-danger"> {{ session('messageError') }}</div>
            @endif
            <div class="panel panel-default">
                <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Nucleo</h2>
                <div class="panel-body panel-nucleo" style="margin-left: 50px;">
                    {!! Form::open(['url' => 'editar_nucleo/'.$nucleo->id, 'method' => 'POST']) !!}
                    {!! Form::token() !!}
                    <div class="col-md-10">
                        {!! Form::label('codigo', 'Codigo') !!}
                        <br>
                        {!! Form::text('codigo', $nucleo->codigo, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('codigo') }}</span>
                    </div>
                    <br>
                    <div class="col-md-10">
                        {!! Form::label('nombre_sede', 'Nombre de Sede') !!}
                        <br>
                        {!! Form::text('nombre_sede', $nucleo->nombre_sede, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('nombre_sede') }}</span>
                    </div>

                    <div class="col-md-12">
                        {!! Form::submit('Editar Nucleo', ['class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
