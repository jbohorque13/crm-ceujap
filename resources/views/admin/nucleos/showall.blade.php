@extends('layouts.app')

@section('htmlheader_title')
Nucleos
@endsection
@section('contentheader_title')
Nucleos
@endsection

@section('main-content')

<a href="{{ url('crear_nucleo') }}" type="button" class="btn btn-success btn-add"> Crear Nucleos</a>

<div class="hidden message-status"></div> 
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Nombre Sede</th>
<!--            <th>Estatus</th>-->
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_nucleos > 0)
    <tbody>
        @foreach($nucleos as $row_nucleos)
        <tr>
            <td> {{ $row_nucleos->codigo}}</td>
            <td> {{ $row_nucleos->nombre_sede}}</td>
<!--            <td>
                @if($row_nucleos->estatus == 0)
                <input data-toggle="toggle" data-id="{{ $row_nucleos->id }}" type="checkbox">
                @elseif($row_nucleos->estatus == 1)
                <input data-toggle="toggle" data-id="{{ $row_nucleos->id }}" type="checkbox" checked>
                @endIf
            </td>-->
            <td> <a class="btn btn-xs btn-primary edit_nucleo" href="{{ url('editar_nucleo/' . $row_nucleos->id) }}"><i class="glyphicon glyphicon-edit"></i></a>

          <!-- <a data-id="{{ $row_nucleos->id }}" class="btn btn-xs btn-danger" data-toggle="modal" accesskey="" data-target="#ModalDelete"><i class="glyphicon glyphicon-trash"></i></a> -->
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_nucleos == 0)
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
