@extends('layouts.app')

@section('htmlheader_title')
Estadistica
@endsection
@section('contentheader_title')
Estadistica
@endsection

@section('main-content')


<div class="row">

    <div class="container">
        <h2 style="margin-bottom: 40px">La gráfica representa la satisfacción de los participantes con respecto al contenido del diplomado en un módulo especifico, <strong> Para ver la evaluación que obtuvo un diplomado en particular presione click en el nombre del diplomado que están en la tabla</strong> </h2>

        <!--     <!-- ./col -->
        <!--  <div class="col-lg-4 col-xs-6">
               <div class="small-box bg-yellow">
                 <div class="inner"> 
                   <h3>{{ $count_results_questionnaires }}</h3>
     
                   <p>Numero de cuestionarios respondidos </p>
                 </div>
                 <div class="icon">
                   <i class="ion ion-person-add"></i>
                 </div>
                 <a href=#" class="small-box-footer">Ver mas informacion  <i class="fa fa-arrow-circle-right"></i></a>
               </div>
         </div> -->

        @if(@$count_results_questionnaires > 0)
        <div class="col-lg-12 col-md-12">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">s
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Evaluación de <strong> {{ @$resultados[0]->diplomado->titulo }}  </strong>  en el módulo: <strong> {{ @$resultados[0]->modulo->descripcion }}   </strong> para el nucleo de: <strong> {{ @$resultados[0]->nucleo->nombre_sede }}   </strong> </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="chart-responsive">
                                <canvas id="pieChart" height="202" width="303" style="width: 243px; height: 162px;"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <ul class="chart-legend clearfix">
                                <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-red"></i> {{ @$preguntas[0]->pregunta_7 }}</li>
                                <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-green"></i> {{ @$preguntas[0]->pregunta_8 }}</li>
                                <li style="margin-bottom: 10px;"><i class="fa fa-circle-o text-yellow"></i> {{ @$preguntas[0]->pregunta_9 }}</li>
                            </ul>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <!-- <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">A
                      <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                    <li><a href="#">B <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                    </li>
                    <li><a href="#">C
                      <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                  </ul>
                </div> -->

            </div>
        </div>
    </div>
</div>      
@endif
<div class="hidden message-status"></div>
<table class="table table-striped">
    <tr>
        <th>Diplomado</th>
        <th>Módulo</th> 
        <th>Núcleo</th>
        <th>Cohorte</th>
        <th>Resultado</th>
    </tr>

    @if(@$count_results_questionnaires > 0)
    @for($i=0; $i < @$count_results_questionnaires; $i++)
    @if($i == 0)
    <tr class="diplomado-seleccionado">
        <td> <span data-nucleo-id="{{ @$resultados[$i]->nucleo->id }}" data-diplomado-id="{{ @$resultados[$i]->diplomado->id }}" data-modulo-id="{{ @$resultados[$i]->modulo->id }}" class="diplomado-nombre">{{ @$resultados[$i]->diplomado->titulo }} </span></td>
        <td>  {{ @$resultados[$i]->modulo->descripcion }} </td>
        <td>  {{ @$resultados[$i]->nucleo->nombre_sede }} </td>
        <td> {{ @$resultados[$i]->modulo->cohorte }} </td>
        <td> {{ @$resultados[$i]->porcentaje_contenido }} % </td>
    </tr>
    @else
    <tr>
        <td> <span data-nucleo-id="{{ @$resultados[$i]->nucleo->id }}" data-diplomado-id="{{ @$resultados[$i]->diplomado->id }}" data-modulo-id="{{ @$resultados[$i]->modulo->id }}" class="diplomado-nombre">{{ @$resultados[$i]->diplomado->titulo }} </span></td>
        <td>  {{ @$resultados[$i]->modulo->descripcion }} </td>
        <td>  {{ @$resultados[$i]->nucleo->nombre_sede }} </td>
        <td> {{ @$resultados[$i]->modulo->cohorte }} </td>
        <td> {{ @$resultados[$i]->porcentaje_contenido }} % </td>
    </tr>
    @endif
    @endfor
    @endif

</table>



@endsection

@section('push_script')
<script type="text/javascript">

    // -------------
    // - PIE CHART -
    // -------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChart = new Chart(pieChartCanvas);

    var PieData = [
        {
            value: '{{ @$cuestionarios[0]->respuesta_7 }}',
            color: '#f56954',
            highlight: '#f56954',
            label: '%'
        },
        {
            value: '{{ @$cuestionarios[0]->respuesta_8 }}',
            color: '#00a65a',
            highlight: '#00a65a',
            label: '%'
        },
        {
            value: '{{ @$cuestionarios[0]->respuesta_9 }}',
            color: '#f39c12',
            highlight: '#f39c12',
            label: '%'
        }
    ];
    var pieOptions = {
        // Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        // String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        // Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        // Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        // Number - Amount of animation steps
        animationSteps: 100,
        // String - Animation easing effect
        animationEasing: 'easeOutBounce',
        // Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        // Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        // Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        // String - A legend template
        legendTemplate: '',
        // String - A tooltip template
        tooltipTemplate: '<%=value %> <%=label%>'
    };
    // Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
    $('.diplomado-nombre').click(function () {
        var diplomado_id = $(this).attr('data-diplomado-id');
        var nucleo_id = $(this).attr('data-nucleo-id');
        var modulo_id = $(this).attr('data-modulo-id');
        var referencia = $(this);

        console.log(diplomado_id + ' ' + nucleo_id + ' ' + modulo_id);

        var dataURL = '{{url("ajax_estadistica_diplomado")}}';
        //console.log(currency);
        $.ajax({
            url: dataURL,
            data: {
                'diplomado_id': diplomado_id,
                'nucleo_id': nucleo_id,
                'modulo_id': modulo_id,
                '_token': $('#token').val()
            },
            type: 'post',
            async: false,
            dataType: 'json',
            success: function (result) {
                if (result.success) {
                    $('.diplomado-seleccionado').removeClass('diplomado-seleccionado');
                    referencia.parent().parent().addClass('diplomado-seleccionado');
                    $('#pieChart').remove();
                    $('.chart-responsive').html('<canvas id="pieChart" height="156" width="1081" style="width: 1154px; height: 167px;"></canvas>');
                    $('.box-title').html('Evaluación de <strong>  ' + result.resultados[0].diplomado.titulo + ' </strong>  en el modulo: <strong> ' + result.resultados[0].modulo.descripcion + ' </strong> para el nucleo de: <strong> ' + result.resultados[0].nucleo.nombre_sede + ' </strong>');

                    pieChartCanvas = $('#pieChart').get(0).getContext('2d');
                    pieChart = new Chart(pieChartCanvas);

                    PieData = [
                        {
                            value: result.resultados[0].respuesta_7,
                            color: '#f56954',
                            highlight: '#f56954',
                            label: '%'
                        },
                        {
                            value: result.resultados[0].respuesta_8,
                            color: '#00a65a',
                            highlight: '#00a65a',
                            label: '%'
                        },
                        {
                            value: result.resultados[0].respuesta_9,
                            color: '#f39c12',
                            highlight: '#f39c12',
                            label: '%'
                        }
                    ];

                    pieChart.Doughnut(PieData, this.pieOptions);
                }
            },
            error: function (request, error) {

            }
        });
    });

</script>
@endsection
