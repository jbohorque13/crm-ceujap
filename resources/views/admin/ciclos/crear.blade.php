@extends('layouts.app')

@section('htmlheader_title')
Crear Ciclo 
@endsection

@section('contentheader_title')
Crear Ciclo 
@endsection

@section('main-content')
<div class="container">
    <a href="{{ url('ciclos') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los ciclos</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(session()->has('messageError'))
            <div class="alert alert-danger"> {{ session('messageError') }}</div>
            @endif
            <div class="panel panel-default">
                <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Ciclo</h2>
                <div class="panel-body panel-ciclo" style="margin-left: 50px;">
                    {!! Form::open(['url' => 'crear_ciclo', 'method' => 'POST']) !!}
                    {!! Form::token() !!}
                    <div class="col-md-10">
                        {!! Form::label('codigo', 'Codigo') !!}
                        <br>
                        {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('codigo') }}</span>
                    </div>
                    <br>
                    <div class="col-md-10">
                        {!! Form::label('descripcion', 'Descripcion') !!}
                        <br>
                        {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('descripcion') }}</span>
                    </div>
                    <div class="col-md-5">
                        {!! Form::label('fecha_inicio', 'Fecha de inicio') !!}
                        <br>
                        {!! Form::date('fecha_inicio', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('fecha_inicio') }}</span>
                    </div>
                    <div class="col-md-5">
                        {!! Form::label('fecha_fin', 'Fecha de Culminacion') !!}
                        <br>
                        {!! Form::date('fecha_fin', null, ['class' => 'form-control']) !!}
                        <span class="text-danger col-md-10">{{ $errors->first('fecha_fin') }}</span>
                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">
                        {!! Form::submit('Guardar Ciclo', ['class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
