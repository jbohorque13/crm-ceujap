@extends('layouts.app')

@section('htmlheader_title')
Ciclos
@endsection
@section('contentheader_title')
Ciclos
@endsection

@section('main-content')

<a href="{{ url('crear_ciclo') }}" type="button" class="btn btn-success btn-add"> Crear Ciclo</a>

<div class="hidden message-status"></div>
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Codigo</th>
            <th>Descripcion</th>
            <th>Fecha de Inicio</th>
            <th>Fecha de Culminacion</th>
<!--            <th>Estatus</th>-->
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_ciclo > 0)
    <tbody>
        @foreach($ciclos as $row_ciclos)
        <tr>
            <td> {{ $row_ciclos->codigo}}</td>
            <td> {{ $row_ciclos->descripcion}}</td>
            <td> {!! $row_ciclos->fecha_inicio !!} </td>
            <td> {{ $row_ciclos->fecha_fin }} </td>
<!--            <td>
                @if($row_ciclos->estatus == 0)
                <input data-toggle="toggle" data-id="{{ $row_ciclos->id }}" type="checkbox">
                @elseif($row_ciclos->estatus == 1)
                <input data-toggle="toggle" data-id="{{ $row_ciclos->id }}" type="checkbox" checked>
                @endIf
            </td>-->
            <td> <a class="btn btn-xs btn-primary edit_ciclo" href="{{ url('editar_ciclo/' . $row_ciclos->id) }}"><i class="glyphicon glyphicon-edit"></i></a>

                    <!-- <a data-id="{{ $row_ciclos->id }}" class="btn btn-xs btn-danger" data-toggle="modal" accesskey="" data-target="#ModalDelete"><i class="glyphicon glyphicon-trash"></i></a> -->
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_ciclo == 0)
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
