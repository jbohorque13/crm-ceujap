@extends('layouts.app')

@section('htmlheader_title')
Diplomados
@endsection
@section('contentheader_title')
Diplomados
@endsection

@section('main-content')
<div class="col-md-12">
    <a href="{{ url('crear_diplomado') }}" type="button" class="btn btn-success btn-add"> Agregar diplomado</a>
</div>
<div class="hidden message-status"></div>
<table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Diplomado</th>
            <th>Modulo</th>
            <th>Nucleo</th>
<!--            <th>Estatus</th>-->
            <th>Acciones</th>
        </tr>
    </thead>
    @if($numero_diplomado > 0)
    <tbody>
        @foreach($diplomados as $row_diplomados) 
        <tr>
            <td> 
                <span title="{{ $row_diplomados->descripcion }}"> {{ $row_diplomados->titulo}} </span></td>
            <td>
                @if(@$numero_diplomados_modulos > 0)
                    @for($i=0; $i < @$numero_diplomados_modulos; $i++ )
                        @if($diplomados_modulos[$i]->diplomado_id == $row_diplomados->id)
                            <span style="background: #00a65a; border-radius: 10px; padding: 5px 10px; color: white;"  title="{{ @$diplomados_modulos[$i]->codigo }}"> {{ @$diplomados_modulos[$i]->descripcion }}  </span> <span>&nbsp;&nbsp;</span>
                        @endif
                    @endfor
                @endif
            </td>
            <td> 
                @if($numero_diplomados_nucleos > 0)
                    @for($i=0; $i < @$numero_diplomados_nucleos; $i++ )
                        @if($diplomados_nucleos[$i]->diplomado_id == $row_diplomados->id)
                            <span style="background: #00a65a; border-radius: 10px; padding: 5px 10px; color: white;" title="{{ @$diplomados_nucleos[$i]->codigo }}">{{ $diplomados_nucleos[$i]->nombre_sede }} </span> <span>&nbsp;&nbsp;</span>
                        @endif
                    @endfor
                @endif
            </td>

<!--            <td>
                @if($row_diplomados->estatus == 0)
                <input data-toggle="toggle" data-id="{{ $row_diplomados->id }}" type="checkbox">
                    @elseif($row_diplomados->estatus == 1)
                <input data-toggle="toggle" data-id="{{ $row_diplomados->id }}" type="checkbox" checked>
                @endIf
            </td>-->
            <td> <a class="btn btn-xs btn-primary edit_diplomado" href="{{ url('editar_diplomado', $row_diplomados->id) }}"><i class="glyphicon glyphicon-edit"></i></a>

                            <!-- <a data-id="{{ $row_diplomados->id }}" class="btn btn-xs btn-danger" data-toggle="modal" accesskey="" data-target="#ModalDelete"><i class="glyphicon glyphicon-trash"></i></a> -->
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
@include('modals.delete')
@if($numero_diplomado == 0)
@endif

@endsection

@section('push_script')
<script src="{{ asset('/js/ajax_delete.js') }}" type="text/javascript"></script>
@endsection
