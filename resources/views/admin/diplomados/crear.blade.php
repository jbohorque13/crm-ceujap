@extends('layouts.app')

@section('htmlheader_title')
  Crear Diplomado 
@endsection

@section('contentheader_title')
    Crear Diplomado 
@endsection

@section('main-content')
<div class="container">

    <a href="{{ url('diplomados') }}" class="btn btn-info" style="margin-bottom: 50px;"> Regresar a ver los diplomados</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Diplomado</h2>
                    <div class="panel-body panel-diplomado" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'crear_diplomado', 'method' => 'POST']) !!}
                        {!! Form::hidden('numero_de_modulos', 1, ['id' => 'numero_de_modulos']) !!}
                            <div class="col-md-10">
                                {!! Form::label('titulo', 'Titulo') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('titulo') }}</span>
                              </div>
                              <div class="col-md-2">
                                {!! Form::label('codigo', 'Codigo') !!} <span class="required"> * </span>
                                <br>
                                {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('codigo') }}</span>
                              </div>
                              <div class="col-md-12">
                                  {!! Form::label('descripcion', 'Descripcion') !!} <span class="required"> * </span>
                                  <br>
                                  {!! Form::textarea('descripcion', null, ['class' => 'form-control'] ) !!}
                                  <span class="text-danger">{{ $errors->first('descripcion') }}</span>
                              </div>
                              <div class="col-md-12 line"></div>
                              <div class="col-md-12 container-module">
                                  <h2 class="title-panel text-center" style="margin-top: 20px; margin-bottom: 20px!important;">Modulo  </h2>  
                                  <p class="title-panel text-center" style="color:red; font-size: 1.2em;">(Debes seleccionar al menos un modulo) </p>
                                 @if(count(@$modulos) > 0)
                                    @foreach($modulos as $row_modulos)
                                        <div class="col-md-4">
                                            {!! Form::checkbox('modulo_id[]', @$row_modulos->id) !!} 
                                            <span style="margin-left: 10px; font-weight: 600;"> {{$row_modulos->codigo}}, ( {{ $row_modulos->descripcion }} ) </span> 
                                        </div>
                                      @endforeach
                                      <span class="text-danger col-md-10">{{ $errors->first('modulo_id') }}</span>
                                  @endif
                                    
                                </div>
                              <div class="col-md-12 line"></div>
                              <div class="col-md-12">
                              <h2 class="title-panel text-center" style="margin-top: 20px; margin-bottom: 20px!important;">Ciclo</h2>
                                {!! Form::label('ciclo_id', 'Seleccione un ciclo') !!}
                                <br>
                                {!! Form::select('ciclo_id', @$array_ciclo, '', ['class' => 'col-md-3 ciclo_id']) !!} 
                                <span>&nbsp;&nbsp;</span><span data-toggle="tooltip" class="fa fa-question-circle icon-question" title="{{$ciclos[0]->descripcion}}"></span>
                              </div>
                            <br>
                            <div class="col-md-12 line"></div>
                            <div class="col-md-12">
                            <h2 class="title-panel text-center" style="margin-top: 20px; margin-bottom: 20px!important;">Nucleo(s)</h2>
                            <p class="title-panel text-center" style="color:red; font-size: 1.2em;">(Debes seleccionar al menos un nucleo) </p>
                            @if(count(@$nucleos) > 0)
                              @foreach($nucleos as $row_nucleos)
                                  <div class="col-md-4">
                                      {!! Form::checkbox('nucleo_id[]', @$row_nucleos->id) !!} 
                                      <span style="margin-left: 10px; font-weight: 600;"> {{$row_nucleos->nombre_sede}}</span> 
                                  </div>
                                @endforeach
                                <span class="text-danger col-md-10">{{ $errors->first('nucleo_id') }}</span>
                            @endif
                                
                            </div>
                            <br>
                            <div class="col-md-12 line"></div>
                              <div class="col-md-12">
                              <h2 class="title-panel text-center" style="margin-top: 20px; margin-bottom: 20px!important;">Facilitador</h2>
                                {!! Form::label('facilitador_id', 'Seleccione al facilitador que impartira el diplomado') !!}
                                <br>
                                {!! Form::select('facilitador_id', @$facilitadores, '', ['class' => 'col-md-3']) !!} 
                              </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-12">
                                {!! Form::submit('Guardar Diplomado', ['class' => 'btn btn-success']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                </div>
         </div>
    </div>
</div>
@endsection
@section('push_script')
    <script> 
    var numero_module = 1;
        $('.btn-plus-module').click(function () {
          add_module();
        });
        var add_module = function(){
            this.numero_module = this.numero_module + 1;
            $('#numero_de_modulos').val(this.numero_module);
            $('.container-module').append("<div class='modulo_"+this.numero_module+"'>"
                                    +"<div class='col-md-6'>"
                                    +"<label for='codigo_"+this.numero_module+"'>Codigo</label>"
                                    +"<br>"
                                    +"<input name='codigo_"+this.numero_module+"' type='text'>"
                                    +"</div>"
                                    +"<div class='col-md-6'>"
                                    +"<label for='cohorte_"+this.numero_module+"'>Cohorte</label>"
                                    +"<br>"
                                    +"<input name='cohorte_"+this.numero_module+"' type='text id='cohorte'>"
                                    +"</div>"
                                    +"<div class='col-md-12'>"
                                    +"<label for='descripcion_"+this.numero_module+"'>Descripcion</label>"
                                    +"<br>"
                                    +"<textarea name='descripcion_"+this.numero_module+"' cols='50' rows='10' id='descripcion'></textarea>"
                                    +"</div>"
                                    +"</div>");

        }
        $('.btn-minus-module').click( function (){
                delete_module();
        });
        var delete_module = function (){
            console.log(this.numero_module);
              $('.modulo_'+this.numero_module).remove();
              this.numero_module = this.numero_module - 1;
              $('#numero_de_modulos').val(this.numero_module);
              
        } 

    </script>
@endsection