@extends('layouts.app')

@section('htmlheader_title')
  Editar Diplomado 
@endsection

@section('contentheader_title')
    Editar Diplomado 
@endsection

@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
      @if(session()->has('messageError'))
          <div class="alert alert-danger"> {{ session('messageError') }}</div>
      @endif
                <div class="panel panel-default">
                    <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Diplomado</h2>
                    <div class="panel-body panel-diplomado" style="margin-left: 50px;">
                        {!! Form::open(['url' => 'editar_diplomado/'.$diplomado[0]->id, 'method' => 'POST']) !!}
                               <div class="col-md-10">
                                {!! Form::label('titulo', 'Titulo') !!}  <span class="required"> * </span>
                                <br>
                                {!! Form::text('titulo', $diplomado[0]->titulo, ['class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('titulo') }}</span>
                              </div>
                              <div class="col-md-2">
                                {!! Form::label('codigo', 'Codigo') !!}  <span class="required"> * </span>
                                <br>
                                {!! Form::text('codigo', $diplomado[0]->codigo, ['class' => 'form-control']) !!}
                                <span class="text-danger">{{ $errors->first('codigo') }}</span>
                              </div>
                              <div class="col-md-12">
                                  {!! Form::label('descripcion', 'Descripcion') !!}  <span class="required"> * </span>
                                  <br>
                                  {!! Form::textarea('descripcion', $diplomado[0]->descripcion, ['class' => 'form-control']) !!}
                                  <span class="text-danger">{{ $errors->first('descripcion') }}</span>
                              </div>
                              <div class="col-md-12 line"></div>
                              <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Modulo </h2>
                              <p class="title-panel text-center" style="color:red; font-size: 1.2em;">(Debes seleccionar al menos un modulo) </p>
                              <div class="container-module">
                                  <?php $bandera = false; ?>
                                  <?php $aux_modulos_for_diplomado = @$modulos_for_diplomado; ?>
                                 @if(count(@$modulos) > 0)
                                    @foreach($modulos as $row_modulos)
                                        @foreach($aux_modulos_for_diplomado as $row_modulos_diplomado)
                                            @if($row_modulos_diplomado->modules[0]->id == $row_modulos->id)
                                                 <?php $bandera = true; ?>
                                                 @break
                                            @else
                                                <?php $bandera = false; ?>
                                            @endif
                                        @endforeach
                                            <?php $aux_modulos_for_diplomado = @$modulos_for_diplomado; ?>
                                            @if($bandera)
                                                <div class="col-md-4">
                                                    {!! Form::checkbox('modulo_id[]', @$row_modulos->id, ['selected' => 'selected']) !!} 
                                                    <span style="margin-left: 10px; font-weight: 600;"> {{$row_modulos->codigo}}, ( {{ $row_modulos->descripcion }} ) </span> 
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    {!! Form::checkbox('modulo_id[]', @$row_modulos->id) !!} 
                                                    <span style="margin-left: 10px; font-weight: 600;"> {{$row_modulos->codigo}}, ( {{ $row_modulos->descripcion }} ) </span> 
                                                </div>
                                            @endif
                                      @endforeach
                                      <span class="text-danger">{{ $errors->first('modulo_id') }}</span>
                                  @endif
                                    
                                </div>
                              <div class="col-md-12 line"></div>
                              <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Ciclo</h2>
                              <div class="col-md-12">
                                {!! Form::label('ciclo_id', 'Seleccione un ciclo') !!}
                                <br>
                                {!! Form::select('ciclo_id', @$array_ciclo, '', ['class' => 'col-md-3 ciclo_id']) !!} 
                                <span>&nbsp;&nbsp;</span><span data-toggle="tooltip" class="fa fa-question-circle icon-question" title="{{$ciclos[0]->descripcion}}"></span>
                              </div>
                            <br>
                            <div class="col-md-12 line"></div>
                            <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Nucleo(s)</h2>
                            <p class="title-panel text-center" style="color:red; font-size: 1.2em;">(Debes seleccionar al menos un nucleo) </p>
                            <div class="col-md-12">
                                
                                <?php $bandera = false; ?>
                                  <?php $aux_nucleos_for_diplomado = @$nucleos_for_diplomado; ?>
                                 @if(count(@$nucleos) > 0)
                                    @foreach($nucleos as $row_nucleos)
                                        @foreach($aux_nucleos_for_diplomado as $row_nucleos_diplomado)
                                            @if($row_nucleos_diplomado->nucleus[0]->id == $row_nucleos->id)
                                                 <?php $bandera = true; ?>
                                                 @break
                                            @else
                                                <?php $bandera = false; ?>
                                            @endif
                                        @endforeach
                                            <?php $aux_nucleos_for_diplomado = @$nucleos_for_diplomado; ?>
                                            @if($bandera)
                                                <div class="col-md-4">
                                                    {!! Form::checkbox('nucleo_id[]', @$row_nucleos->id, ['selected' => 'selected']) !!} 
                                                    <span style="margin-left: 10px; font-weight: 600;"> {{$row_nucleos->nombre_sede}}, ( {{ $row_nucleos->codigo }} ) </span> 
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    {!! Form::checkbox('nucleo_id[]', @$row_nucleos->id) !!} 
                                                    <span style="margin-left: 10px; font-weight: 600;"> {{$row_nucleos->nombre_sede}}, ( {{ $row_nucleos->codigo }} ) </span> 
                                                </div>
                                            @endif
                                      @endforeach
                                      <span class="text-danger">{{ $errors->first('nucleo_id_id') }}</span>
                                  @endif
                                  
                                
                            </div>
                            <br>
                            <div class="col-md-12 line"></div>
                              <h2 class="title-panel text-center" style="margin-top: 50px; margin-bottom: 10px!important;">Facilitador</h2>
                              <div class="col-md-12">
                                {!! Form::label('facilitador_id', 'Seleccione al facilitador que impartira el diplomado') !!}
                                <br>
                                {!! Form::select('facilitador_id', @$facilitadores, @$facilitador_for_diplomado[0]->facilitador_id, ['class' => 'col-md-3']) !!} 
                              </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-12">
                                {!! Form::submit('Guardar Diplomado', ['class' => 'btn btn-success']) !!}
                              </div>
                            {!! Form::close() !!}
                        </div>
                </div>
         </div>
    </div>
</div>
@endsection
@section('push_script')
    
@endsection