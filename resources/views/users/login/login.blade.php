<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Ingresar</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Ingresar a CEUJAP" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/login_css/demo.css') }}" /> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/login_css/style2.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/login_css/animate-custom.css') }}" />
        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body> 
        <div class="container">

            <header>
                <h1>Bienvenido al sistema de CEUJAP <span>Ingrese aquí</span></h1>
            </header>

            @if(session()->has('msg-login'))
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session()->get('msg-login') }} 
            </div>
            @endif

            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form action="{{ url('usuario/login') }}" method="post" autocomplete="on"> 
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <h1>Ingresar</h1> 
                                <p> 
                                    <label for="correo" class="uname" data-icon="u" > Tu Correo </label>
                                    <input id="correo" name="email" required="required" type="email" placeholder="Ejemplo@ejemplo.com"/>
                                </p>
                                <p> 
                                    <label for="clave" class="youpasswd" data-icon="p"> Tu Contraseña </label>
                                    <input id="clave" name="password" required="required" type="password" placeholder="*********" /> 
                                </p>
                                <!-- <p class="keeplogin"> 
                                                                        <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
                                                                        <label for="loginkeeping">Keep me logged in</label>
                                                                </p> -->
                                <p class="login button"> 
                                    <input type="submit" value="Login" /> 
                                </p>
                            </form>
                        </div>

                    </div>  
            </section>
        </div>
    </body>
    <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
</html>