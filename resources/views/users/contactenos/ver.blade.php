@extends('users.app')

@section('htmlheader_title')
    Contactenos
@endsection

@section('main-content')
<div id="banner" class="container">
		@if(session()->has('msg-contacto'))
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  {{ session()->get('msg-contacto') }}
				</div>
		@endif

		<div class="container">
			<div class="title" style="margin-bottom: 100px;">
				@if (!Auth::guest())
                @endif
				<span class="byline">Cualquier problema, inquietud o sugerencia sobre nuestros diplomados o profesores haznos saber para solucionar tu problema o responder tu inquietud. </span> 
				</div>
			{!! Form::open(['url' => 'enviar_contacto', 'method' => 'POST']) !!}

				{!! Form::hidden('usuario_id', Auth::user()->id) !!}
			
			<div class="container">

				<div class="col-md-9" style="margin-left: 15%; margin-bottom: 50px;">
				<h3 style="color: black; text-align: left; font-weight: 700;  margin-bottom: 0;"> Seleccione una palabra clave que describa al motivo de tu correo </h3>
                    <br>
                    {!! Form::select('palabras_claves_id', @$palabras_claves, '', ['class' => 'form-control height-50']) !!} 
              	</div>

				<div class="col-md-9" style="margin-left: 15%">
	                {!! Form::text('asunto', '', ['class' => 'form-control height-50', 'placeholder' => 'Asunto']) !!}

				</div>
				<div class="col-md-9"  style="margin-left: 15%">
	                <h3 style="color: black; text-align: left; font-weight: 700;  margin-bottom: 0;"> Escribenos tu mensaje aqui </h3>
	                <br>
	                {!! Form::textarea('mensaje', '', ['class' => 'form-control', 'placeholder' => 'Mensaje']) !!}

				</div>
				<div class="col-md-9" style="text-align: right; margin-left: 15%"">
	                {!! Form::submit('Enviar Mensaje', ['class' => 'btn-encuesta button']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	
@endsection

@section('script-push')

@endsection()
