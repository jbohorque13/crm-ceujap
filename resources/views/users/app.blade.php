<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">

@section('htmlheader')
    @include('users.partials.htmlheader')
@show

<body>
<div id="wrapper">

    @include('users.partials.mainmenu')
        
</div><!-- ./wrapper -->

<!-- Your Page Content Here -->
@yield('main-content')

<div id="footer" class="container-fluid">
    @include('users.partials.copyright')
<div>
    @include('users.partials.script')
</body>
</html>
