@extends('users.app')

@section('htmlheader_title')
Inicio
@endsection



@section('main-content')
<div id="banner" class="container">
    <div class="container">
        @if(session()->has('msg-cuestionario'))
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('msg-cuestionario') }}
        </div>
        @endif
        <div class="title">
            @if (!Auth::guest())
            <h3 style="color: black; margin-bottom: 50px;">Hola, {{ Auth::user()->nombre_completo }}</h3>
            @endif
            <span class="byline">Evalua el desempeño de tus diplomados cursados y los profesores que lo impartieron, de esta forma nos ayudara a ofrecer un mejor servicio </span> 
        </div>
        <h1 class="sub-title" style="color: red;">
            <strong>(No podras evaluar si no has finalizado el curso) </strong>
        </h1>
        {!! Form::open(['url' => 'cuestionario/respondido', 'id'=> 'form-cuestionario', 'method' => 'POST']) !!}
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="nucleo_id" value="{{ @$usuario->nucleo_id }}">
        <div class="col-md-12 container-fields">
            <div class="col-md-6">
                <div class="col-md-12 margin-bottom-80 margin-top-100">
                    {!! Form::label('diplomado_id', 'Selecciona el Curso a Evaluar', ['class' => 'form-control label-form']) !!}
                    <br>
                    {!! Form::select('diplomado_id', $array_diplomados, '', ['class' => 'form-control height-50', 'id' => 'diplomado_id', 'placeholder' => 'seleccionar...']) !!}
                </div>


                <div class="col-md-12 margin-bottom-80">
                    {!! Form::label('modulo_id', 'Selecciona el Modulo a Evaluar', ['class' => 'form-control label-form']) !!}
                    <br>
                    {!! Form::select('modulo_id', [], '', ['class' => 'form-control height-50', 'id' => 'modulo_id']) !!}
                </div>

            </div>
            <div class="col-md-6">
                <div class="col-md-12 margin-bottom-80 margin-top-100">
                    <img style="display:inline-block; width: 150px; height: 120px; margin-bottom: 66px;" class="responsive foto-falicitador hidden" src="">

                    {!! Form::label('facilitador_id', 'Selecciona el Profesor a Evaluar', ['class' => 'form-control label-form']) !!}
                    <br>
                    {!! Form::select('facilitador_id', [], '', ['class' => 'form-control height-50', 'id' => 'facilitador_id']) !!} 

                </div>

                <div class="col-md-12">
                    {!! Form::button('Siguiente', ['class' => 'btn-cuestionario button disabled']) !!}
                </div>
            </div>
        </div>

        <div class="col-md-12 table-preguntas hidden">
            <h1> Evalua al Facilitador o Profesor </h1>
            <table class="table" style="margin-top: 50px;">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th class="text-center" style="font-size: 1.5em;">Pregunta</th>
                        <th class="text-center" style="font-size: 1.5em;">Excelente</th>
                        <th class="text-center" style="font-size: 1.5em;">Bueno</th>
                        <th class="text-center" style="font-size: 1.5em;">Regular</th>
                        <th class="text-center" style="font-size: 1.5em;">Malo</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i=1; $i < 7; $i++)
                    <?php $index_pregunta = 'pregunta_' . $i; ?>
                    <tr>
                        <th scope="row">{{ $i }}</th>
                        <td style="font-weight: 800; font-size: 1.3em;">{{ $preguntas[0][$index_pregunta] }}</td>
                        <td><input class="form-control" type="radio" name="{{ $index_pregunta }}" value="4"> </td>
                        <td><input class="form-control" type="radio" name="{{ $index_pregunta }}" value="3"></td>
                        <td><input class="form-control" type="radio" name="{{ $index_pregunta }}" value="2"></td>
                        <td><input class="form-control" type="radio" name="{{ $index_pregunta }}" value="1"></td>
                    </tr>

                    @endfor
                </tbody>
            </table>
            <br>
            <br>
            <h1> Evalua el Contenido del Diplomado o Curso</h1>

            <table class="table" style="margin-top: 50px;">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th class="text-center" style="font-size: 1.5em;">Pregunta</th>
                        <th class="text-center" style="font-size: 1.5em;">Excelente</th>
                        <th class="text-center" style="font-size: 1.5em;">Bueno</th>
                        <th class="text-center" style="font-size: 1.5em;">Regular</th>
                        <th class="text-center" style="font-size: 1.5em;">Malo</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i=7; $i < 10; $i++)
                    <?php $index_pregunta = 'pregunta_' . $i; ?>
                    <tr>
                        <th scope="row">{{ $i }}</th>
                        <td style="font-weight: 800; font-size: 1.3em;">{{ $preguntas[0][$index_pregunta] }}</td>
                        <td><input type="radio" class="form-control" name="{{ $index_pregunta }}" value="4"> </td>
                        <td><input type="radio" class="form-control" name="{{ $index_pregunta }}" value="3"></td>
                        <td><input type="radio" class="form-control" name="{{ $index_pregunta }}" value="2"></td>
                        <td><input type="radio" class="form-control" name="{{ $index_pregunta }}" value="1"></td>
                    </tr>

                    @endfor
                </tbody>
            </table>
            <br>
            <div class="col-md-12">
                {!! Form::button('Guardar', ['class' => 'btn-enviar-cuestionario button disabled']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<hr style="width: 1000px">
<div id="extra" class="container">
    <div class="title">
        <h2>Si deseas hacernos una sugerencia puedes hacerlo en nuestro seccion de <strong style="color: #c10909;"> contactenos.</strong> </h2>
    </div>

    <ul class="actions">
        <li><a href="{{ url('usuario/contactenos') }}" class="button">Contactenos</a></li>
    </ul>
</div>
@endsection

@section('script-push')
<script type="text/javascript">
    $(document).on('change', '#diplomado_id', function () {
        var array_radio_buttons = new Array();
        var diplomado_id = $(this).val();
        console.log(diplomado_id);

        var dataURL = '{{url("get_facilitadores_modulos")}}';
        //console.log(currency);
        $.ajax({
            url: dataURL,
            data: {
                'diplomado_id': diplomado_id,
                '_token': $('#token').val()
            },
            type: 'post',
            async: false,
            dataType: 'json',
            success: function (result) {
                console.log(result);
                console.log(result.success);
                if (result.success) {
                    $('.foto-falicitador').addClass('hidden');
                    var facilitadores_on = false;

                    var modulos_on = false;

                    if (result.facilitadores.length == 0) {
                        $('#facilitador_id').html('');
                        facilitadores_on = false;
                    } else {
                        var image = '{{ URL::to('/') }}';
                        for (var i = 0; i < result.facilitadores.length; i++) {
                            if (i == 0) {
                                var src_foto = image +'/'+ result.facilitadores[0].facilitators[0].foto;

                                $('#facilitador_id').append('<option value="' + result.facilitadores[i].facilitators[0].id + '">' + result.facilitadores[i].facilitators[0].nombre_completo + '</option>');
                                $('.foto-falicitador').attr('src', src_foto).removeClass('hidden');
                            } else {
                                $('#facilitador_id').append('<option value="' + result.facilitadores[i].facilitators[0].id + '">' + result.facilitadores[i].facilitators[0].nombre_completo + '</option>');
                            }

                        }
                        facilitadores_on = true;
                    }

                    if (result.modulos.length == 0) {
                        $('#modulo_id').html('');
                        modulos_on = false;
                    } else {
                        for (var i = 0; i < result.modulos.length; i++) {

                            $('#modulo_id').append('<option value="' + result.modulos[i].modules[0].id + '">' + result.modulos[i].modules[0].descripcion + '</option>');
                        }
                        modulos_on = true;
                    }
                    if (modulos_on && facilitadores_on) {
                        $('.btn-cuestionario').removeClass('disabled');
                        $('.btn-cuestionario').addClass('evento-cuestionario');
                    } else {
                        $('.btn-cuestionario').addClass('disabled');
                        $('.btn-cuestionario').removeClass('evento-cuestionario');
                    }


                }
            },
            error: function (request, error) {

            }
        });

        $('.evento-cuestionario').click(function () {

            $('.sub-title').html('');
            $('.sub-title').append('<strong style="margin-top:50px; color:#c10909; display:block;"> Debes seleccionar por pregunta una casilla que corresponda con el grado de satisfación que tuviste al hacer el diplomado.</strong>');
            $('#form-cuestionario').prepend('<input type="hidden" name="diplomado_id" value="' + $('#diplomado_id option:checked').val() + '">');
            $('#form-cuestionario').prepend('<input type="hidden" name="facilitador_id" value="' + $('#facilitador_id option:checked').val() + '">');
            $('#form-cuestionario').prepend('<input type="hidden" name="modulo_id" value="' + $('#modulo_id option:checked').val() + '">');
            $('.container-fields').remove();
            $('.table-preguntas').removeClass('hidden');

            console.log($('input[type="radio"]'));
            $('input[type="radio"]').click(function () {
                console.log($(this).attr('name'));
                console.log('longitud del array ' + array_radio_buttons.length);

                console.log(jQuery.inArray($(this).attr('name'), array_radio_buttons));
                if (jQuery.inArray($(this).attr('name'), array_radio_buttons) == -1) {
                    array_radio_buttons.push($(this).attr('name'));
                    console.log('agregado');
                    console.log('length ' + array_radio_buttons.length);
                    if (array_radio_buttons.length == 9) {
                        $('.btn-enviar-cuestionario').removeClass('disabled').attr('type', 'submit');
                    }
                }
            });
            1
        });

        if ($('.msg-status').html() != '') {
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                    $('.msg-status').addClass('hidden');
                });
            }, 400);
        }
    });
</script>
@endsection()
