	<div id="banner-ceujap" class="container">
		<img src="{{ asset('img/banner.jpg') }}">
	</div>

	<div id="header-wrapper" class="container">
		<div id="header" class="container">
		@if (!Auth::guest())
			<div class="col-md-2">
				<a href="{{ url('usuario/inicio') }}"> <img src="{{ asset('img/logo.png') }}"></a>
			</div>
			
			<div class="col-md-7 menu-a">
				<ul class="col-md-11" style="margin-top: 2%;">
					<li class="col-md-3 text-left"><a href="{{ url('usuario/inicio') }}" accesskey="1" title="">Inicio</a></li>
					<li class="col-md-3 text-left"><a href="{{ url('usuario/contactenos') }}" accesskey="5" title="">Contactenos</a></li>
				</ul>
			</div>
			<div class="col-md-2 text-right menu-a" style="margin-top: 1%;">
				<a href="{{ url('logout') }}" accesskey="5" title="">Salir</a>
			</div>
		@endif 
		</div>
	</div>
