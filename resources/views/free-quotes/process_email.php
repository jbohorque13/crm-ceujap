<?php 

$data = $_POST['data'];

$options = "";

foreach($data['options_project'] as $option){
	$options.= '<span style="    background-color: #179bd2!important;
    padding: 12px;
    border-radius: 8px;
    margin: 0 10px;
    color: white;">' .$option . '</span>';		
}	

$to_Email = 'herdzphoto@gmail.com, josuebohorquezc@gmail.com';
$subject = 'GET STARTED PROJECT';
  $headers = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

  $headers .= 'From: ' . $data['email'];

  $message = '<div style="background-color: rgb(234, 234, 234);padding:20px 100px;">
            	<div style="margin: 30px 0">
                   <h2 style="text-shadow:2px 2px #ccc;letter-spacing: 2px; text-align:center;color:#179bd2;font-weight:600;font-size:2em;"> New project </h2>
           		 </div>
        		<div style="background-color: white; margin: 20px auto;padding:40px 100px;">
              		<p style="font-size:1.2em;margin:1.5em 0">Full Name  : <strong> '. $data['full_name'] .  ' </strong></p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Email: <strong> '. $data['email'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Tags: '. $options .  '</p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Description: <strong> '. $data['text_area'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Expected Budget : <strong> '. $data['money_expected'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Phone : <strong> '. $data['phone'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Company : <strong> '. $data['company'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Company type : <strong> '. $data['type_company'] .  ' </strong> </p>';
$message.= '<p style="font-size:1.2em;margin:1.5em 0"> Link : <strong> '. $data['link_value'] .  ' </strong> </p>';

$message.= '</div> </div>';

$sentMail = @mail($to_Email, $subject, $message, $headers);

echo json_encode($sentMail);