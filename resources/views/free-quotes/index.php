<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="Profesional Web development company located in Miami FL. Graphic design, animations, logo & branding are also included in our plans. Website design creative agency, check our prices, contact us for a web developer 24/7">
        <meta name="keywords" content="web,website development,miami,herdz design,logo,animation,web design,florida,agency,company" />
        <meta name="author" content="Herdz Design">
        <title>Web design in Miami FL. HERDZ DESIGN is Website development, Logo & Animation</title>

        <!-- Bootstrap core CSS -->
        <link rel="shortcut icon" href="../favicon.png">
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Google Web Font -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="../
              css/normalize.css" />

        <!-- Owl-Carousel -->
        <link rel="stylesheet" type="text/css" href="../css/normalize.css" />
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/contact.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/cs-select.css" />
        <link rel="stylesheet" type="text/css" href="../css/cs-skin-elastic.css" />


        <!--[if IE 9]>
                <script src="js/PIE_IE9.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="js/PIE_IE678.js"></script>
        <![endif]-->

        <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
        <![endif]-->

    </head>

    <body id="home">

        <!-- Preloader -->
        <div id="preloader">
            <div id="status"></div>
        </div>

        <div class="FauxModalLayout wrapper wrapper-sm wrapper-one">
            <div class="layout">
                <div class="layout-item">
                    <a class="float-right" href="/demos/web_desarrollo">
                        <img src="../img/ic-close.svg" alt="Back to main page">
                    </a>
                </div>
                <div class="layout-item">
                    <div class="mb-lg">
                        <div class="">
                            <div class="layout-item text-align-center">
                                <span class="icon-web-mobile">

                                </span>
                            </div>
                            <div class="layout-item text-align-center">
                                <h5 class="FauxModalLayout__Subheading"></h5>
                            </div>
                            <h4 class="headline headline-md mb-none text-align-center mb-sm">Tell us about your project</h4>
                            <div class="wrapper wrapper-xxs">
                                <div>
                                    <div class="layout-item width-1 text">Choose at least one<div class="float-right text-light text-xs text-bold">Required</div>
                                    </div>
                                    <div class="layout-item item-options width-1">
                                        <div class="pillbox mr-platform mb-sm" data-value="android">
                                            android
                                        </div>
                                        <div class="pillbox mr-platform mb-sm" data-value="ios">
                                            ios
                                        </div>
                                        <div class="pillbox mr-platform mb-sm" data-value="web">
                                            web
                                        </div>
                                        <div class="pillbox mr-platform mb-sm" data-value="logos">
                                            logos
                                        </div>
                                        <div class="pillbox mr-platform mb-sm" data-value="animations">
                                            animations
                                        </div>
                                        <div class="pillbox mr-platform mb-sm" data-value="social media">
                                            social media
                                        </div>
                                        <div class="text text-align-left layout-item gutters-none text-danger mb-none"></div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="layout-item width-1 text mt-sm mb-none">Describe Your Project
                                        <div class="float-right text-light text-xs text-bold">Required</div>
                                    </div>
                                    <div class="layout-item width-1">
                                        <textarea type="textarea" placeholder="Enter a short description of your idea. e.g. I’d like to build an Android app that allows dog owners to order grooming products easily. I know roughly how it should look, but don’t have full designs." class="form-input form-textarea-fixed"></textarea>
                                        <div class="text text-align-left layout-item gutters-none text-danger mb-none"></div>
                                    </div>
                                </div>

                                <section>
                                    <div class="layout-item width-1 text mt-sm mb-none">Expected Budget
                                        <select class="cs-select cs-skin-elastic" id="cs-select-one">
                                            <option value="500-1000" selected> $300-$500 </option>
                                            <option value="500-1000" selected> $500-$1000 </option>
                                            <option value="1000-3000">$1000-$3000</option>
                                            <option value="3000+" > $3000+</option>
                                        </select>
                                </section>
                                <div class="layout-item clearfix mt-md"><button class="btn btn-full btn-full-cero" type="button">NEXT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="FauxModalLayout wrapper wrapper-sm wrapper-two hidden">
        <div class="layout">
            <div class="layout-item">
                <a class="float-right" href="/demos/web_desarrollo">
                    <img src="../img/ic-close.svg" alt="Back to main page">
                </a>
            </div>
            <div class="layout-item">
                <div class="mb-lg">
                    <div class="">
                        <div class="layout-item text-align-center">
                            <span class="icon-attachment">

                            </span>
                        </div>
                        <div class="layout-item text-align-center">
                            <h5 class="FauxModalLayout__Subheading">get started</h5>
                        </div>
                        <h4 class="headline headline-md mb-none text-align-center mb-sm">Anything you want to attach?</h4>
                        <div class="wrapper wrapper-xxs">
                            <div class="asset-btn-wrapper layout-item width-1 gutters-none">
                                <div class="layout-item gutters-none">
                                    <div class="form wrapper text-align-center layout">
                                        <div class="layout-item width-1 form-input-btn-group gutters-none">
                                            <button class="asset-btn-highlight asset-btn-left btn-inverted _left width-1-of-2-small btn-left" type="button">
                                                <span class="fa fa-unlink"></span>

                                                <span class="va-middle">Add Link</span>
                                            </button>
                                            <button class="asset-btn-highlight asset-btn-right btn-inverted _right width-1-of-2-small btn-right" type="button">
                                                <span class="fa fa-files-o"></span>
                                                <span class="va-middle">Add File</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="LinkAssets hidden">

                                <label class="text">URL</label>
                                <div class="LinkAssets__Wrapper width-1 gutters-none">
                                    <input type="text" placeholder="Paste URL Here" class="form-input LinkAssets__Input form-input-grouped" value="">
                                    <button class="LinkAssets__Button btn-inverted" type="button">
                                        <span class="fa fa-upload"></span>
                                    </button>
                                </div>
                                <div class="text text-align-left layout-item gutters-none text-danger mb-none"></div>
                            </div>
                            <div class="layout-item width-1 gutters-none mb-sm FileAssets hidden">
                                <div class="mt-xs">
                                    <div class="FileAssets__Error">
                                    </div>
                                    <div class="file-upload">
                                        <span class="text text-tertiary-dark mb-none">Drag files here, or&nbsp;
                                            <span class="link">Browse files</span>
                                        </span>
                                        <input type="file" multiple="" style="display: none;">
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="layout-item width-1 text-align-center gaps-md">
                            <span class="text text-tertiary text-light text-bold">No attachments yet.</span>
                            </div> -->

                            <div class="layout">
                                <div class="layout-item">
                                    <div>
                                    </div>
                                </div>
                                <div class="layout-item">
                                    <div>
                                    </div>
                                </div>
                                <div class="layout-item clearfix mt-md">
                                    <a class="previous-link btn-previous btn-with-skip float-left"> <i class="fa fa-angle-left" aria-hidden="true"></i> PREVIOUS</a>
                                    <button class="btn float-right btn-skip" type="button">SKIP</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="FauxModalLayout wrapper wrapper-sm wrapper-three hidden">
        <div class="layout">
            <div class="layout-item">
                <a class="float-right" href="/demos/web_desarrollo"><img src="../img/ic-close.svg" alt="Back to main page"></a>
            </div>
            <div class="layout-item">
                <div class="mb-lg">
                    <div class="">
                        <div class="layout-item text-align-center">
                            <span class="icon-business-card"></span>
                        </div>
                        <div class="layout-item text-align-center">

                        </div>
                        <h4 class="headline headline-md mb-none text-align-center mb-sm">How can we reach you?</h4>
                        <div class="wrapper wrapper-xxs"><div class="gaps-sm gutters">
                                <button id="my-sign-gmail-herdzdesign" class="btn btn-icon btn-google btn-full">
                                    <span>Connect with Google</span>
                                </button>
                            </div>
                            <div class="layout-item text text-align-center text-lg text-medium-gray mb-none">OR</div>
                            <div class="">
                                <div class="layout-item width-1 text mt-sm mb-none">Name
                                    <div class="float-right text-light text-xs text-bold">Required</div>

                                </div>
                                <div class="layout-item width-1">
                                    <input type="text" name="full-name" placeholder="Enter your full name e.g. Alex Doe" class="form-input" value="">
                                    <div class="text text-align-left layout-item gutters-none text-danger mb-none">
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="layout-item width-1 text mt-sm mb-none">Email
                                    <div class="float-right text-light text-xs text-bold">Required</div>
                                </div>
                                <div class="layout-item width-1">
                                    <input name="email" type="email" placeholder="example@example.com" class="form-input" value="">
                                    <div class="text text-align-left layout-item gutters-none text-danger mb-none"></div>
                                </div>
                            </div>
                            <div class="layout-item width-1-of-2 mt-sm" style="margin-top: 0!important;">
                                <div>
                                    <div class="layout-item width-1 text mt-sm mb-none" style="padding-left: 0 !important;">Phone</div>
                                    <input name="phone" type="text" placeholder="+1-432-222-3201" value="" class="form-input">
                                </div><div class="text text-align-left layout-item gutters-none text-danger mb-none">

                                </div>

                            </div>
                            <div class="layout-item width-1-of-2">
                                <div class="layout-item width-1 gutters-none text mt-sm mb-none">Company</div>
                                <div class="layout-item width-1 gutters-none"><input name="company" type="text" placeholder="Your Company Name" class="form-input" value="">
                                    <div class="text text-align-left layout-item gutters-none text-danger mb-none">
                                    </div>
                                </div>
                            </div>
                            <div class="layout-item width-1 text mb-none mt-sm">Company type</div> 
                            <div class="layout-item"><div> <section>
                                        <select class="cs-select cs-skin-elastic" id="cs-select-two">
                                            <option value="100-300" selected>Enterprise</option>
                                            <option value="300-500">Agency</option>
                                            <option value="500-and over" >Media</option>
                                        </select>
                                    </section>
                                    <div class="text text-align-left layout-item gutters-none text-warning mb-none"></div>

                                </div>

                            </div>
                            <div class="layout-item">
                            </div><div class="layout-item clearfix mt-md">
                            <a class="previous-link btn-previous-two float-left"> <i class="fa fa-angle-left" aria-hidden="true"></i> PREVIOUS</a>
                            <button class="btn btn-submit float-right" type="button">SUBMIT</button></div></div></div></div></div></div></div>


    <!-- JavaScript -->

    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/selectFx.js"></script>
    <script src="../js/classie.js"></script>
    <script>
        $(document).ready(function () {
            var $text_area = '';
            var money_expected = '';
            var link_value = '';
            var array_options = {
                number: 0,
                data: [],
                set_data: function ($data) {
                    this.data[this.number] = $data;
                    this.number = this.number + 1;
                },
                delete_data: function ($data) {
                    this.number = this.number - 1;
                    this.data = jQuery.grep(this.data, function (value) {
                        return value != $data;
                    });
                },
            };

            $(".pillbox").click(function () {
                console.log($(this));
                if (!$(this).hasClass('is-active'))
                {
                    $(this).addClass('is-active');
                    console.log($(this).attr('data-value'));
                    array_options.set_data($(this).attr('data-value'));
                } else
                {
                    $(this).removeClass('is-active');
                    console.log($(this).attr('data-value'));
                    array_options.delete_data($(this).attr('data-value'));
                }
                console.log(array_options);
            });
            $('.btn-full-cero').click(function () {

                var validate = true;

                $text_area = $('.form-textarea-fixed')[0].value;

                if ($text_area == '')
                {
                    validate = false;
                    $('.form-textarea-fixed').next('div').html('This field is Required').addClass('error-active');
                } else
                {
                    if ($('.form-textarea-fixed').next('div').hasClass('error-active'))
                    {
                        $('.form-textarea-fixed').next('div').html('');
                    }
                }
                if (array_options.number == 0)
                {
                    validate = false;
                    $('.item-options').children('.text-danger').html('This field is Required').addClass('error-active');
                } else
                {
                    if ($('.item-options').next('div').hasClass('error-active'))
                    {
                        $('.item-options').next('div').html('');
                    }
                }
                money_expected = $('#cs-select-one').parent().children('span.cs-placeholder').html()

                if (validate)
                {
                    $('.text-danger').html('');
                    $('.wrapper-one').addClass('hidden');
                    $('.wrapper-two').removeClass('hidden');
                }

            });
            $('.btn-left').click(function () {
                if (!$('.FileAssets').hasClass('hidden'))
                {
                    $('.FileAssets').addClass('hidden')
                }
                $('.LinkAssets').removeClass('hidden');
            });
            $('.btn-right').click(function () {
                if (!$('.LinkAssets').hasClass('hidden'))
                {
                    $('.LinkAssets').addClass('hidden')
                }
                $('.FileAssets').removeClass('hidden');
            });

            $('.LinkAssets__Button').click(function () {
                link_value = $('.LinkAssets__Input').val();
                if (link_value != '')
                {
                    $(this).children('span').removeClass('fa fa-upload').addClass('fa fa-check');
                    setTimeout(function () {
                        $(this).children('span').removeClass('fa fa-check').addClass('fa fa-upload');
                        $('.wrapper-three').removeClass('hidden');
                        $('.wrapper-two').addClass('hidden');
                        return;
                    }, 2000);
                } else
                {
                    $(this).parent().next('.text-danger').html('This field is Required');
                }

            });

            $('.btn-skip').click(function () {
                $('.wrapper-three').removeClass('hidden');
                $('.wrapper-two').addClass('hidden');
            });
            $('.btn-previous').click(function () {
                $('.wrapper-one').removeClass('hidden');
                $('.wrapper-two').addClass('hidden');

            });
            $('.btn-previous-two').click(function () {
                $('.wrapper-two').removeClass('hidden');
                $('.wrapper-three').addClass('hidden');
            });

            $('.btn-submit').click(function () {
                var validate = true;
                full_name = $('input[name="full-name"]').val();
                if (full_name == '') {
                    validate = false;
                    $('input[name="full-name"]').next('.text-danger').html('This field is Required');
                }
                email = $('input[name="email"]').val();
                if (email == '') {
                    validate = false;
                    $('input[name="email"]').next('.text-danger').html('This field is Required');
                } else
                {
                    if (!isEmail(email))
                    {
                        validate = false;
                        $('input[name="email"]').next('.text-danger').html('Invalid format');
                    }
                }
                phone = $('input[name="phone"]').val();
                if (phone == '') {
                    phone = 'no ingresado';
                }
                company = $('input[name="company"]').val();
                if (company == '') {
                    company = 'no ingresado';
                }
                type_company = $('#cs-select-two').parent().children('span.cs-placeholder').html();
                if (type_company == '') {
                    type_company = 'no ingresado';
                }
                if (link_value == '') {
                    link_value = 'no ingresado';
                }

                $data_for_send = {
                    options_project: array_options.data,
                    text_area: $text_area,
                    money_expected: money_expected,
                    link_value: link_value,
                    full_name: full_name,
                    email: email,
                    phone: phone,
                    company: company,
                    type_company: type_company,

                };
                if (validate)
                {
                    send_data($data_for_send);
                }

            });
            var send_data = function ($data) {
                $.ajax({
                    type: "POST",
                    url: "process_email.php",
                    data: {data: $data},
                    dataType: 'JSON',
                    async: false,
                    success: function (msg) {

                    },
                    error: function (request, error) {
                        console.log(error);
                    }
                });
            }
            function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
        });
    </script>
    <script>
        (function (w, i, d, g, e, t, s) {
            w[d] = w[d] || [];
            t = i.createElement(g);
            t.async = 1;
            t.src = e;
            s = i.getElementsByTagName(g)[0];
            s.parentNode.insertBefore(t, s);
        })(window, document, '_gscq', 'script', '//widgets.getsitecontrol.com/70039/script.js');
    </script>

    <script>
        (function () {
            [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
                new SelectFx(el);
            });
            //event contact
        })();

    </script>

    <script>

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-80014600-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script>
        var googleUser = {};
        var startApp = function () {
            gapi.load('auth2', function () {
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init({
                    client_id: '284919746639-r92fieeni9msrsqgp5bnti15p67lvnat.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                    // Request scopes in addition to 'profile' and 'email'
                    //scope: 'additional_scope'
                });
                attachSignin(document.getElementById('my-sign-gmail-herdzdesign'));
            });
        };

        function attachSignin(element) {
            console.log(element.id);
            auth2.attachClickHandler(element, {},
                    function (googleUser) {
                        $('input[name="full-name"]').val(googleUser.getBasicProfile().getName());
                        $('input[name="email"]').val(googleUser.getBasicProfile().getEmail());
                    }, function (error) {
                alert(JSON.stringify(error, undefined, 2));
            });
        }
    </script>
    <script>startApp();</script>
</body>

</html>
