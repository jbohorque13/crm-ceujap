@extends('users.app')

@section('htmlheader_title')
    Bienvenido
@endsection


@section('main-content')
<div id="banner" class="container">
		<div class="container">
			<div class="title">
				<span class="byline">Bienvenido a CEUJAP </span> 
			</div>
			<h1 class="sub-title" style="color: red;">
				<strong> Eres participante o administrador? </strong>
			</h1>
			<h3 class="sub-title">
				<strong> Si eres un participante o estudiante presione click <a href="{{ url('usuario/ingresar') }}"> aquí </a></strong>

			</h3>
			
		</div>
	</div>
@endsection

@section('script-push')
	
@endsection()
