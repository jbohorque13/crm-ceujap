<?php

return [

  'sidebar' => [
      'users'           	    	=> 'Usuarios',
      'statistics'  	         	=> 'Estadistica',
      'diplomados'		            => 'Diplomados',
      'questionnaires'           	=> 'Cuestionarios',
      'questionnaires_create'    	=> 'Crear cuestionarios',
      'questionnaires_view'      	=> 'Ver cuestionarios',
      'emails'						=> 'Correos',
      'emails_create'				=> 'Crear correos',
	  'emails_view'					=> 'Ver correos',
      'notifications'						=> 'Notificationes',
	  'notifications_create'		=> 'Crear notificaciones',
	  'notifications_view'			=> 'Ver notificaciones',
	  'settings'					=> 'Configuraciones',
  ],
];
