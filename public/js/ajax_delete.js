$(document).ready(function (){
    $('a[data-target="#ModalDelete"]').click(function (){
      var idElement = $(this).attr('data-id');
      console.log(idElement);
      $('.delete-modal-btn').click(function(){
        var urlActual = window.location.pathname;
        var route = getRoute(urlActual);
        console.log("route " +route);
        processData(route,idElement);
      });
    });

    var processData = function (route,idElement) {

      $.ajax({
        url: route,
        data : {
            id:idElement,
          },
          type: "GET",
          dataType: "json",
          success: function(jsonResponse){
            html = '<div class="alert alert-success" role="alert">';
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            html += '<strong>'+ jsonResponse +'</strong> ';
            html += '</div>';
            $('.message-status').removeClass('hidden').html(html);

            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                    $('.message-status').addClass('hidden');
                    location.reload(true);
                });
            }, 4000);

          },
          error : function(xhr, status) {
            console.log(xhr);
              html = '<div class="alert alert-danger" role="alert">';
              html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
              html += '<strong>Error!</strong> Dato no eliminado.';
              html += '</div>';
              $('.message-status').removeClass('hidden').html(html);

              window.setTimeout(function() {
                  $(".alert").fadeTo(500, 0).slideUp(500, function(){
                      $(this).remove();
                      $('.message-status').addClass('hidden');
                  });
              }, 4000);

          }
        });
    }
    var getRoute = function (urlActual) {
        var route = '';
        switch (urlActual) {
          case '/sugerencias':
            route = 'delete_sugerencia';
            break;
          case '/cuestionarios':
            route = 'delete_questionnaire';
            break;
          case '/show-all-videos':
            route = 'video-delete';
            break;
          case '/show-all-category':
            route = 'category-delete';
            break;
          default:
            break;
        }
        return route;
    }
});
