<?php
namespace App\Http\Helpers;

use App\Models\Email;
use DateTime;
use Session;
use Config;

class EmailHelper
{
    private $from_email;	// Global variable for Facebok Helper
    
    public function __construct()
    {
        //$this->from_email = 'herdzphoto@gmail.com';
    }
    
    public function send ($data) 
    {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: herdzphoto@gmail.com';

        $message = '<div style="background-color: rgb(234, 234, 234);padding:20px 100px;">
                    <div style="margin: 30px 0">
                       <h2 style="text-shadow:2px 2px #ccc;letter-spacing: 2px; text-align:center;color:#179bd2;font-weight:600;font-size:2em;"> CENTRO DE EXTENSION CEUJAP </h2>
                             </div>
                            <div style="background-color: white; margin: 20px auto;padding:40px 100px;">
                            <h2 style="margin:2em 0; text-align:center;">'. $data['title'] . '</h2>';
        $message.= '<p style="font-size:1.2em;margin:1.5em 0">'. $data['message'] .  '</p>';
        $message.= '</div> </div>';

        return @mail($data['to_emails'], $data['subject'], $message, $headers);
        
    }
    public function get_dia($fecha){
        return date('d', strtotime($fecha));
    }
    public function sumar_dia($dia, $cantidad_sumar){
        
    }
    public function cambiar_estatus($email, $value){
        $email->estatus = $value;
        return $email->save();
    }
    
    public function comprobar_envio($emails){
        $fecha_hosting = date('d-m-Y');
        $estatus = false;
        foreach($emails as $row_email){
            $fecha_inicio = date('d-m-Y', strtotime($row_email->fecha_inicio));
            $fecha_cierre = date('d-m-Y', strtotime($row_email->fecha_fecha_cierre));
            $primera_condicion = $this->compararFechas($fecha_hosting, $fecha_inicio);
            $segunda_condicion = $this->compararFechas($fecha_hosting,$fecha_cierre );
            
            if($primera_condicion >= 0 && $segunda_condicion >= 0){
                if($row_email->estatus == 0){ //cambiar a comparar a 0 es cuando no ha enviado ni un correo
                    if($row_email->frecuencia == 'hora'){
                        $data['to_emails'] = $row_email->usuarios;
                        $data['subject'] = $row_email->asunto;
                        $data['title'] = $row_email->titulo;
                        $data['message'] = $row_email->mensaje;
                       
                        if(!empty($row_email->imagen_uno)){
                            
                            $data['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($row_email->imagen_uno) .'"> ';
                        }
                        if($this->send($data)){
                            $this->cambiar_estatus($row_email, 1);
                        }
                    }
                }
                else{
                    $this->cambiar_estatus($row_email, 0); 
                }
                
            }
            
        }
        
    }
    
    function compararFechas($primera, $segunda)
    {
     $valoresPrimera = explode ("-", $primera);   
     $valoresSegunda = explode ("-", $segunda); 
     $diaPrimera    = $valoresPrimera[0];  
     $mesPrimera  = $valoresPrimera[1];  
     $anyoPrimera   = $valoresPrimera[2]; 
     $diaSegunda   = $valoresSegunda[0];  
     $mesSegunda = $valoresSegunda[1];  
     $anyoSegunda  = $valoresSegunda[2]; 
     $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
     $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
     if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
       // "La fecha ".$primera." no es válida";
       return 0;
     }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
       // "La fecha ".$segunda." no es válida";
       return 0;
     }else{
       return  $diasPrimeraJuliano - $diasSegundaJuliano;
     } 
   }
    

}

