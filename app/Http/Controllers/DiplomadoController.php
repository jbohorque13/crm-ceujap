<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Diplomado;
use App\Models\Nucleus;
use App\Models\Cycle;
use App\Models\Facilitador;
use App\Models\Module;
use App\Models\EmailProgramado;
use App\Http\Helpers\EmailHelper;
use App\Models\RelationshipCyclesModules;
use App\Models\RelationshipNucleusDiplomados;
use App\Models\RelationshipFacilitatorsDiplomados;
use App\Models\RelationshipModulesDiplomados;

use DB;
use Session;

use Validator;

class DiplomadoController extends Controller
{
     /**
     * @var EmailHelper
     */
    private $email;
    
    protected $email_helper; // Global variable for Helpers instance
    
    public function __construct(EmailHelper $email)
    {
        $this->email_helper = $email;  
        $emails = EmailProgramado::get();
        
        if($emails->count() > 0){
            $this->email_helper->comprobar_envio($emails);
        }
        
    }
    
    public function show () 
    {
      $data['diplomados'] = Diplomado::orderBy('created_at', 'desc')->get(); 
      $data['diplomados_nucleos'] = Diplomado::nucleos();
      $data['diplomados_modulos'] = Diplomado::modulos();
      $data['numero_diplomado'] = count($data['diplomados']);
      $data['numero_diplomados_nucleos'] = count($data['diplomados_nucleos']);

      $data['numero_diplomados_modulos'] = count($data['diplomados_modulos']);

      return view('admin.diplomados.showall', $data);
    }

   public function crear_diplomado (Request $request)
    {

      if(!$_POST) 
      {
          $redirect = false;
          $data['nucleos'] = Nucleus::get();
          $data['facilitadores'] = Facilitador::dropdown();
          $data['modulos'] = Module::active_all();
          $array_facilitadores = array();

          if(count($data['nucleos']) == 0){
            Session::flash('msg-nucleo', 'Antes de crear un diplomado, crea un nucleo al menos.');
            return redirect('home');
          }
          $ciclos = Cycle::get();
          $data['ciclos'] = $ciclos;

          $data['array_ciclo'] = Cycle::array_ciclo();

          return view('admin.diplomados.crear', $data);
      }

      if($_POST)
      {
            $rules = array(
                  'titulo' => 'required',
                  'codigo' => 'required|unique:diplomados',
                  'descripcion' => 'required',
                   'modulo_id' => 'required',
                   'nucleo_id' => 'required',
                );

            // Add Validation Custom Names

            $niceNames = array(
                  'titulo' => 'Titulo',
                  'codigo' => 'Codigo',
                  'descricion' => 'Descripcion',
                  'modulo_id' => 'Modulo',
                  'nucleo_id' => 'Nucleo',
            );
            
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

        if ($validator->fails()) 
          {
              return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
          }
          else
          {

            $diplomado = new Diplomado;
            $diplomado->titulo = $request->titulo;
            $diplomado->codigo = $request->codigo;
            $diplomado->descripcion = $request->descripcion;
            $diplomado->save();

            $diplomado = Diplomado::all();
            $diplomado_id = $diplomado->last()->id;


            foreach($request->modulo_id as $row_modulo_id){

                $cliclo_modulo = new RelationshipCyclesModules;
                $cliclo_modulo->ciclo_id = $request->ciclo_id;
                $cliclo_modulo->modulo_id = $row_modulo_id;
                $cliclo_modulo->save();

                $modulo_diplomado = new RelationshipModulesDiplomados;
                $modulo_diplomado->modulo_id = $row_modulo_id;
                $modulo_diplomado->diplomado_id = $diplomado_id;
                $modulo_diplomado->save();

            }

            foreach($request->nucleo_id as $row_nucleo_id){
                $relacion_nucleo_diplomado = new RelationshipNucleusDiplomados;
                $relacion_nucleo_diplomado->nucleo_id = $row_nucleo_id;
                $relacion_nucleo_diplomado->diplomado_id = $diplomado_id;
                $relacion_nucleo_diplomado->save();
            }
            
            $facilitador_diplomado = new RelationshipFacilitatorsDiplomados;
            $facilitador_diplomado->facilitador_id = $request->facilitador_id;
            $facilitador_diplomado->diplomado_id = $diplomado_id;
            $facilitador_diplomado->save();

          return redirect('diplomados'); 
          }
        
      }
          
    }
    public function editar_diplomado (Request $request)
    {

        if(!$_POST) 
        {
            
            $data['diplomado']= Diplomado::where('id', '=', $request->id)->get();
            $data['nucleos_for_diplomado'] = RelationshipNucleusDiplomados::with(['nucleus'])->where('diplomado_id', $request->id)->get();
            $data['facilitador_for_diplomado'] = RelationshipFacilitatorsDiplomados::with(['facilitators'])->where('diplomado_id', $request->id)->get();
            $data['modulos_for_diplomado'] = RelationshipModulesDiplomados::with(['modules'])->where('diplomado_id', $request->id)->get();
            $data['nucleos'] = Nucleus::active_all();
            $data['facilitadores'] = Facilitador::dropdown();
            $data['modulos'] = Module::active_all();
            
            
            if(count($data['diplomado']) == 0){
                return redirect('diplomados');
            }
            $ciclos = Cycle::active_all();
            $data['ciclos'] = $ciclos;

            $data['array_ciclo'] = Cycle::array_ciclo();
            
            return view('admin.diplomados.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                  'titulo' => 'required',
                  'codigo' => 'required',
                  'descripcion' => 'required',
                   'modulo_id' => 'required',
                   'nucleo_id' => 'required',
                );

            // Add Validation Custom Names

            $niceNames = array(
                  'titulo' => 'Titulo',
                  'codigo' => 'Codigo',
                  'descricion' => 'Descripcion',
                  'modulo_id' => 'Modulo',
                  'nucleo_id' => 'Nucleo',
            ); 
            
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
            ];


            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $diplomado_id = $request->id;
                
                $diplomado = Diplomado::find($request->id);
                $diplomado->titulo = $request->titulo;
                $diplomado->codigo = $request->codigo;
                $diplomado->descripcion = $request->descripcion;
                $diplomado->save();
                
                RelationshipModulesDiplomados::where('diplomado_id', '=', $diplomado_id)->delete();
                

                foreach($request->modulo_id as $row_modulo_id){

                    $cliclo_modulo = new RelationshipCyclesModules;
                    $cliclo_modulo->ciclo_id = $request->ciclo_id;
                    $cliclo_modulo->modulo_id = $row_modulo_id;
                    $cliclo_modulo->save();

                    $modulo_diplomado = new RelationshipModulesDiplomados;
                    $modulo_diplomado->modulo_id = $row_modulo_id;
                    $modulo_diplomado->diplomado_id = $diplomado_id;
                    $modulo_diplomado->save();

                }
                  
                RelationshipNucleusDiplomados::where('diplomado_id', '=', $diplomado_id)->delete();
                        
                foreach($request->nucleo_id as $row_nucleo_id){
                    $relacion_nucleo_diplomado = new RelationshipNucleusDiplomados;
                    $relacion_nucleo_diplomado->nucleo_id = $row_nucleo_id;
                    $relacion_nucleo_diplomado->diplomado_id = $diplomado_id;
                    $relacion_nucleo_diplomado->save();
                } 
                
                $facilitador_diplomado = RelationshipFacilitatorsDiplomados::where('diplomado_id' , '=', $diplomado_id)->get();
                
                $facilitador_diplomado[0]->facilitador_id = $request->facilitador_id;
                $facilitador_diplomado[0]->diplomado_id = $diplomado_id;
                $facilitador_diplomado[0]->save();

                return redirect('diplomados');    
            }
            
        }
            
    }

    public function ajax_get_diplomados (Request $request)
    {
        $diplomados = RelationshipNucleusDiplomados::with(['diplomados'])->where('nucleo_id', $request->nucleo_id)->get();

        return json_encode(['success'=>'true', 'diplomados' => $diplomados]);
    }

    

}
