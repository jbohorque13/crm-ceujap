<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Nucleus;
use Validator;

class NucleusController extends Controller
{
    public function show () 
    {
    	$data['nucleos'] = Nucleus::orderBy('created_at', 'desc')->get();
    	$data['numero_nucleos'] = count($data['nucleos']); 
    	return view('admin.nucleos.showall', $data);
    }
    
    public function crear_nucleo (Request $request)
    {

        if(!$_POST) 
        {
            return view('admin.nucleos.crear');
   
        }

    	if($_POST)
    	{
    		$rules = array(
                    'codigo' => 'required',
                    'nombre_sede' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'codigo' => 'Codigo',
                    	'nombre_sede' => 'Nombre de Sede',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
	        	$nucleo = new Nucleus;
	    		$nucleo->codigo = $request->codigo;
	    		$nucleo->nombre_sede = $request->nombre_sede;
                $nucleo->ciudad = $request->ciudad;
	    		$nucleo->save();
	    			
	    		return redirect('nucleos');	
	        }
    		
    	}
	        
    }
    public function editar_nucleo (Request $request)
    {

        if(!$_POST) 
        {
            $data['nucleo'] = Nucleus::find($request->id);
            if(count($data['nucleo']) == 0){
                return redirect('nucleos');
            }
            return view('admin.nucleos.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                    'codigo' => 'required',
                    'nombre_sede' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                        'codigo' => 'Codigo',
                        'nombre_sede' => 'Nombre de Sede',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $nucleo = Nucleus::find($request->id);

                $nucleo->codigo = $request->codigo;
                $nucleo->nombre_sede = $request->nombre_sede;
                $nucleo->ciudad = $request->ciudad;
                $nucleo->save();
                    
                return redirect('nucleos');   
            }
            
        }
            
    }
}
