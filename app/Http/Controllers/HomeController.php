<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Questionnaire;

use \App\Models\ResultsQuestionnaire;
use \App\Models\Diplomado;
use \App\Models\Facilitador;
use \App\Models\Module;
use \App\Models\Cycle;
use \App\Models\Nucleus;
use \App\Models\Contactenos;
use Khill\Lavacharts\Lavacharts;

use Session;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $today = date('Y-m-d H:i:s');
        
        $tomorrow = date('Y-m-d H:i:s', strtotime($today) + 86400);

        $today = date('Y-m-d H:i:s', strtotime($today) - 46400);

        $notification_questionnaires = ResultsQuestionnaire::where('created_at', '>', $today)->where('created_at', '<', $tomorrow)->get();
        
        $count_notification_questionnaires = count($notification_questionnaires); 
        
        $data['count_notification_questionnaires'] = $count_notification_questionnaires;

        $contactenos = Contactenos::where('created_at', '>', $today)->where('created_at', '<', $tomorrow)->get();

        $data['count_contactenos'] = count($contactenos);

        session(['count_notification_questionnaires' => $count_notification_questionnaires, 'notification_questionnaires' => $notification_questionnaires]);

        $data['resultados'] = ResultsQuestionnaire::get();

        $data['count_results_questionnaires'] = count($data['resultados']);

        return view('admin.home.home', $data);
    }
    
    public function show_stadistics (Request $request)
    {
        if($request->page == 'cuestionarios')
        {
            $data['count_result_e'] = count(\App\Models\ResultsQuestionnaire::where('result', 'E'));
            $data['count_result_b'] = count(\App\Models\ResultsQuestionnaire::where('result', 'B'));
            $data['count_result_r'] = count(\App\Models\ResultsQuestionnaire::where('result', 'R'));
            $data['count_result_d'] = count(\App\Models\ResultsQuestionnaire::where('result', 'D'));
        }
    }

    public function estadistica_facilitador()
    {

        $cuestionarios = ResultsQuestionnaire::get();

        $preguntas = Questionnaire::get();

        //$lava = new Lavacharts; // See note below for Laravel

        //$votes  = $lava->DataTable();

//        if(count($cuestionarios) > 0 && count($preguntas) > 0){
//            
//            $primer_cuestionario = $cuestionarios[0];
//            
//            $votes->addStringColumn('Food Poll')
//                ->addNumberColumn('Votes')
//                ->addRow([$preguntas[0]->pregunta_1, round(rand($primer_cuestionario->respuesta_1, 100))])
//                ->addRow([$preguntas[0]->pregunta_2, round(rand($primer_cuestionario->respuesta_2, 100))])
//                ->addRow([$preguntas[0]->pregunta_3, round(rand($primer_cuestionario->respuesta_3, 100))])
//                ->addRow([$preguntas[0]->pregunta_4, round(rand($primer_cuestionario->respuesta_4, 100))])
//                ->addRow([$preguntas[0]->pregunta_5, round(rand($primer_cuestionario->respuesta_5, 100))])
//                ->addRow([$preguntas[0]->pregunta_6, round(rand($primer_cuestionario->respuesta_6, 100))]); 
//
//            $lava->BarChart('Facilitador evaluado' . $primer_cuestionario->facilitador->nombre_completo, $votes);
//
//        }
        $data['cuestionarios'] =  $cuestionarios;
        $data['preguntas'] =  $preguntas;

        //$data['lava'] = $lava;

        //$today = date('Y-m-d H:i:s');
        
        //$tomorrow = date('Y-m-d H:i:s', strtotime($today) + 86400);

        //$today = date('Y-m-d H:i:s', strtotime($today) - 46400);

        //$notification_questionnaires = ResultsQuestionnaire::where('created_at', '>', $today)->where('created_at', '<', $tomorrow)->get();
        
        //$count_notification_questionnaires = count($notification_questionnaires); 
        
        //session(['count_notification_questionnaires' => $count_notification_questionnaires, 'notification_questionnaires' => $notification_questionnaires]);

        $data['resultados'] = ResultsQuestionnaire::get();

        $data['count_results_questionnaires'] = count($data['resultados']);
        
        if($data['count_results_questionnaires'] > 0){
            return view('home', $data);
        }
        else{
            return redirect('home');
        }
    }

    public function estadistica_diplomado()
    {

        $cuestionarios = ResultsQuestionnaire::get();

        $preguntas = Questionnaire::get();

//        $lava = new Lavacharts; // See note below for Laravel
//
//        $votes  = $lava->DataTable();
//
//        if(count($cuestionarios) > 0 && count($preguntas) > 0){
//            
//            $primer_cuestionario = $cuestionarios[0];
//            
//            $votes->addStringColumn('Food Poll')
//                ->addNumberColumn('Votes')
//                ->addRow([$preguntas[0]->pregunta_1, round(rand($primer_cuestionario->respuesta_1, 100))])
//                ->addRow([$preguntas[0]->pregunta_2, round(rand($primer_cuestionario->respuesta_2, 100))])
//                ->addRow([$preguntas[0]->pregunta_3, round(rand($primer_cuestionario->respuesta_3, 100))])
//                ->addRow([$preguntas[0]->pregunta_4, round(rand($primer_cuestionario->respuesta_4, 100))])
//                ->addRow([$preguntas[0]->pregunta_5, round(rand($primer_cuestionario->respuesta_5, 100))])
//                ->addRow([$preguntas[0]->pregunta_6, round(rand($primer_cuestionario->respuesta_6, 100))]); 
//
//            $lava->BarChart('Facilitador evaluado' . $primer_cuestionario->facilitador->nombre_completo, $votes);
//
//        }
        $data['cuestionarios'] =  $cuestionarios;
        $data['preguntas'] =  $preguntas;
//
//        $data['lava'] = $lava;
//
//        $today = date('Y-m-d H:i:s');
//        
//        $tomorrow = date('Y-m-d H:i:s', strtotime($today) + 86400);
//
//        $today = date('Y-m-d H:i:s', strtotime($today) - 46400);
//
//        $notification_questionnaires = ResultsQuestionnaire::where('created_at', '>', $today)->where('created_at', '<', $tomorrow)->get();
//        
//        $count_notification_questionnaires = count($notification_questionnaires); 
//        
//        session(['count_notification_questionnaires' => $count_notification_questionnaires, 'notification_questionnaires' => $notification_questionnaires]);

        $data['resultados'] = ResultsQuestionnaire::get();

        $data['count_results_questionnaires'] = count($data['resultados']);
        
        if($data['count_results_questionnaires'] > 0){
            return view('admin.estadistica.diplomado', $data);
        }
        else{
            return redirect('home');
        }
    }
    
    public function actualizar_estadistica_facilitador(Request $request)
    {
        $resultados = ResultsQuestionnaire::with(['facilitador', 'diplomado', 'modulo'])->where('diplomado_id', '=', $request->diplomado_id)
                                                    ->where('modulo_id', '=', $request->modulo_id)
                                                    ->where('facilitador_id', '=', $request->facilitador_id)
                                                    ->get();
        
        if(count($resultados) > 0){
            return json_encode(['success'=>'true', 'resultados' => $resultados]);
        }
        else{
            return json_encode(['success'=>'false']);
        }
                
    }
    public function actualizar_estadistica_diplomado(Request $request)
    {
        $resultados = ResultsQuestionnaire::with(['nucleo', 'diplomado', 'modulo'])->where('diplomado_id', '=', $request->diplomado_id)
                                                    ->where('modulo_id', '=', $request->modulo_id)
                                                    ->where('nucleo_id', '=', $request->nucleo_id)
                                                    ->get();
        
        if(count($resultados) > 0){
            return json_encode(['success'=>'true', 'resultados' => $resultados]);
        }
        else{
            return json_encode(['success'=>'false']);
        }
                
    }
    
}