<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Email;
use App\Models\Cycle;
use App\Models\Diplomado;
use App\Models\Nucleus;
use App\Models\User;
use App\Models\EmailProgramado;
use App\Http\Requests;
use Validator;
use App\Http\Helpers\EmailHelper;
use Session;
use DB;

class EmailController extends Controller
{

    /**
     * @var EmailHelper
     */
    private $email;
    
    protected $email_helper; // Global variable for Helpers instance
    
    public function __construct(EmailHelper $email)
    {
        $this->email_helper = $email;  
        $emails = EmailProgramado::get();
        
        if($emails->count() > 0){
            $this->email_helper->comprobar_envio($emails);
        }
        
    }
    
    public function show () 
    {
        $data['emails'] = Email::orderBy('created_at', 'desc')->get();
        $data['emails_count'] = count($data['emails']);
        return view('admin.emails.showall', $data);
    }
    
    
    
    public function show_programados () 
    {
        $data['emails'] = EmailProgramado::orderBy('created_at', 'desc')->get();
        $data['emails_count'] = count($data['emails']);
        return view('admin.emails.showall_programado', $data);
    }
    
    public function manage_emails (Request $request)
    {
        $data['ciclos'] = Cycle::array_ciclo();

        $data['nucleos'] = Nucleus::dropdown();

        return view('admin.emails.create', $data);
    	
    }
    
    public function envio_programado (Request $request)
    {
        $data['ciclos'] = Cycle::array_ciclo();

        $data['nucleos'] = Nucleus::dropdown();

        return view('admin.emails.envio_programado', $data);
    	
    }

    public function ver_correo (Request $request)
    {
        $data['correo'] = Email::find($request->id);

        return view('admin.emails.ver', $data);
        
    }
    
    public function ver_correo_programado (Request $request)
    {
        $data['correo'] = EmailProgramado::find($request->id);

        return view('admin.emails.ver_programado', $data);
        
    }

    public function envio_email_programado (Request $request)
    {
            $rules = array(
                'titulo'  => 'required',
                'asunto'  => 'required',
                'mensaje' => 'required',
                'frecuencia' => 'required',
                'fecha_inicio' => 'required|before:fecha_cierre|after:today',
                'fecha_cierre' => 'required|after:fecha_inicio'
            );

        // Add Validation Custom Names
            $niceNames = array(
                'titulo'  => 'Titulo',
                'asunto'  => 'Asunto',
                'mensaje' => 'Mensaje',
                'frecuencia' => 'Frecuencia',
                'fecha_inicio' => 'Fecha de Inicio',
                'fecha_cierre' => 'Fecha de Cierre'
            );

            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
                'before' => 'La :attribute debe ser una fecha anterior a la Fecha de Cierre.',
                'after' => 'La :attribute debe ser una fecha posterior a Fecha de Inicio.',
            ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($niceNames); 

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }
        else
        {
                $usuarios = '';
                $diplomados = '';

                $array_email = array();
                
                if(isset($request->diplomado_id) && count($request->diplomado_id) > 0){
                    $email_programado = new EmailProgramado;
                    $usuario_with_nucleo = User::where('nucleo_id', '=', $request->nucleo_id)->get();

                    for($i=0; $i < count($usuario_with_nucleo); $i++){
                            if($i == 0){
                                $usuarios .= $usuario_with_nucleo[$i]->email;

                            }
                            else{
                                $usuarios .= ', '. $usuario_with_nucleo[$i]->email;
                            }
                    }

                    for($i=0; $i < count($request->diplomado_id); $i++){
                            if($i == 0){
                                $diplomados .= $request->diplomado_id[$i];
                            } 
                            else{
                                $diplomados .= ', '. $request->diplomado_id[$i];
                            }
                    }

                    $email_programado->usuarios = $usuarios;
                    $email_programado->ciclo_id = ($request->ciclo_id !='') ? $request->ciclo_id : 0;;
                    $email_programado->nucleo_id = ($request->nucleo_id !='') ? $request->nucleo_id : 0;

                    $email_programado->diplomados = $diplomados;

                    
                    $email_programado->titulo = $request->titulo;
                    $email_programado->asunto = $request->asunto;
                    $email_programado->mensaje = $request->mensaje;
                    
                    $email_programado->frecuencia = $request->frecuencia;
                    $email_programado->fecha_inicio = $request->fecha_inicio;
                    $email_programado->fecha_cierre = $request->fecha_cierre;
                    
//                    $array_email['to_emails'] =  $usuarios;
//                    $array_email['subject'] = $request->asunto;
//                    $array_email['title'] = $request->titulo;
//                    $array_email['message'] = '<p>' . $request->mensaje . '</p>';
                    $response = $email_programado->save();
                    
                    $email_last = EmailProgramado::all()->last();
                    $email_id = $email_last->id;

                    $response_image = EmailProgramado::upload_image($_FILES, $email_id);

//                    if($response_image){
//                        $email_imagen = EmailProgramado::find($email_id); 
//                        $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
//                    }

                    //if($response){
                        //if($this->email_helper->send($array_email) ){
                            //Session::flash('correo-class', 'alert-success');
                            //Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');

                            //$email_last = EmailProgramado::all()->last();
                            //$email_last->estatus = 1;
                            //$email_last->save();

                        //}else{
                          //  Session::flash('correo-class', 'alert-warning');
                            //Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');

                        //}
//                    }else{
//                        Session::flash('correo-class', 'alert-danger');
//                        Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
//                    }

                }else{

                    //Aqui va el codigo cuando no se ha seleccionado  ningun diplomado para enviar el correo 
                    if(isset($request->nucleo_id) && $request->nucleo_id != ''){
                        $email_programado = new EmailProgramado;
                        $usuario_with_nucleo = User::where('nucleo_id', '=', $request->nucleo_id)->get();

                        for($i=0; $i < count($usuario_with_nucleo); $i++){
                                if($i == 0){
                                    $usuarios .= $usuario_with_nucleo[$i]->email;

                                }
                                else{
                                    $usuarios .= ', '. $usuario_with_nucleo[$i]->email;
                                }
                        }

                        $email_programado->usuarios = $usuarios;
                        $email_programado->ciclo_id = ($request->ciclo_id !='') ? $request->ciclo_id : 0;;
                        $email_programado->nucleo_id = ($request->nucleo_id !='') ? $request->nucleo_id : 0;
                        $email_programado->diplomados = $diplomados;

                        
                        $email_programado->titulo = $request->titulo;
                        $email_programado->asunto = $request->asunto;
                        $email_programado->mensaje = $request->mensaje;
                        
                        $email_programado->frecuencia = $request->frecuencia;
                        $email_programado->fecha_inicio = $request->fecha_inicio;
                        $email_programado->fecha_cierre = $request->fecha_cierre;

//                        $array_email['to_emails'] =  $usuarios;
//                        $array_email['subject'] = $request->asunto;
//                        $array_email['title'] = $request->titulo;
//                        $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $request->mensaje . '</p>';
                         $response = $email_programado->save();
//                        
//                        $email_last = EmailProgramado::all()->last();
//                        $email_id = $email_last->id;
//                
//                        $response_image = EmailProgramado::upload_image($_FILES, $email_id);
                        
                        $email_last = EmailProgramado::all()->last();
                        $email_id = $email_last->id;

                        $response_image = EmailProgramado::upload_image($_FILES, $email_id);

//                        if($response_image){
//                            $email_imagen = Email::find($email_id); 
//                            $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
//                        }
                        
//                        if($response){
//                            if($this->email_helper->send($array_email) ){
//                                Session::flash('correo-class', 'alert-success');
//                                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');
//
//                                $email_last = EmailProgramado::all()->last();
//                                $email_last->estatus = 1;
//                                $email_last->save();
//
//                            }else{
//                                Session::flash('correo-class', 'alert-warning');
//                                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');
//
//                            }
//                        }else{
//                            Session::flash('correo-class', 'alert-danger');
//                            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
//                        }
                                
                    }
                    else{
                        $email_programado = new EmailProgramado;

                        $email_programado->usuarios = User::get_email();

                        $email_programado->ciclo_id = 0;
                        $email_programado->nucleo_id = 0;
                        $email_programado->diplomados = 0;

                        
                        $email_programado->titulo = $request->titulo;
                        $email_programado->asunto = $request->asunto;
                        $email_programado->mensaje = $request->mensaje;
                        
                        $email_programado->frecuencia = $request->frecuencia;
                        $email_programado->fecha_inicio = $request->fecha_inicio;
                        $email_programado->fecha_cierre = $request->fecha_cierre;
                        
//                        $array_email_programado['to_emails'] =  $email->usuarios;
//                        $array_email['subject'] = $request->asunto;
//                        $array_email['title'] = $request->titulo;
//                        $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $request->mensaje . '</p>';
                        $response = $email_programado->save();
                        
                        $email_last = EmailProgramado::all()->last();
                        $email_id = $email_last->id;
                
                        $response_image = EmailProgramado::upload_image($_FILES, $email_id);
                        
//                        if($response_image){  
//                            $email_imagen = EmailProgramado::find($email_id); 
//                            $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
//                        }

//                        if($response){
//                            if($this->email_helper->send($array_email) ){
//                                Session::flash('correo-class', 'alert-success');
//                                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');
//
//                                $email_last = EmailProgramado::all()->last();
//                                $email_last->estatus = 1;
//                                $email_last->save();
//
//                            }else{
//                                Session::flash('correo-class', 'alert-warning');
//                                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');
//
//                            }
//                        }else{
//                            Session::flash('correo-class', 'alert-danger');
//                            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
//                        }
                    }
                }

                return redirect('mensajes_programados');

                /*//estos va porque si.

                $data['to_emails'] =  'josuebohorquezc@gmail.com';
                $data['subject'] = $email->subject;
                $data['title'] = $email->title;
                $data['message'] = $email->message;
                //

                $response = $email->save();
                
                if($response)
                    $this->email_helper->send($data);

                return redirect('mensajes');	*/
        }
	
    	
    }
    
    public function create_email (Request $request)
    {
            $rules = array(
                'titulo'  => 'required',
                'asunto'  => 'required',
                'mensaje' => 'required',
            );

        // Add Validation Custom Names
            $niceNames = array(
                'titulo'  => 'Titulo',
                'asunto'  => 'Asunto',
                'mensaje' => 'Mensaje',
            );

            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
            ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($niceNames); 

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }
        else
        {
                $usuarios = '';
                $diplomados = '';

                $array_email = array();



                if(isset($request->diplomado_id) && count($request->diplomado_id) > 0){
                    $email = new Email;
                    $usuario_with_nucleo = User::where('nucleo_id', '=', $request->nucleo_id)->get();

                    for($i=0; $i < count($usuario_with_nucleo); $i++){
                            if($i == 0){
                                $usuarios .= $usuario_with_nucleo[$i]->email;

                            }
                            else{
                                $usuarios .= ', '. $usuario_with_nucleo[$i]->email;
                            }
                    }

                    for($i=0; $i < count($request->diplomado_id); $i++){
                            if($i == 0){
                                $diplomados .= $request->diplomado_id[$i];
                            }
                            else{
                                $diplomados .= ', '. $request->diplomado_id[$i];
                            }
                    }

                    $email->usuarios = $usuarios;
                    $email->ciclo_id = ($request->ciclo_id !='') ? $request->ciclo_id : 0;;
                    $email->nucleo_id = ($request->nucleo_id !='') ? $request->nucleo_id : 0;

                    $email->diplomados = $diplomados;

                    
                    $email->titulo = $request->titulo;
                    $email->asunto = $request->asunto;
                    $email->mensaje = $request->mensaje;
                    $email->estatus = 0;


                    $array_email['to_emails'] =  $usuarios;
                    $array_email['subject'] = $request->asunto;
                    $array_email['title'] = $request->titulo;
                    $array_email['message'] = '<p>' . $request->mensaje . '</p>';
                    $response = $email->save();
                    
                    $email_last = Email::all()->last();
                    $email_id = $email_last->id;

                    $response_image = Email::upload_image($_FILES, $email_id);

                    if($response_image){
                        $email_imagen = Email::find($email_id);
                        $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
                    }

                    if($response){
                        if($this->email_helper->send($array_email) ){
                            Session::flash('correo-class', 'alert-success');
                            Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');

                            $email_last = Email::all()->last();
                            $email_last->estatus = 1;
                            $email_last->save();

                        }else{
                            Session::flash('correo-class', 'alert-warning');
                            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');

                        }
                    }else{
                        Session::flash('correo-class', 'alert-danger');
                        Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
                    }

                }else{

                    //Aqui va el codigo cuando no se ha seleccionado  ningun diplomado para enviar el correo 
                    if(isset($request->nucleo_id) && $request->nucleo_id != ''){
                        $email = new Email;
                        $usuario_with_nucleo = User::where('nucleo_id', '=', $request->nucleo_id)->get();

                        for($i=0; $i < count($usuario_with_nucleo); $i++){
                                if($i == 0){
                                    $usuarios .= $usuario_with_nucleo[$i]->email;

                                }
                                else{
                                    $usuarios .= ', '. $usuario_with_nucleo[$i]->email;
                                }
                        }
                        
                        $email->usuarios = $usuarios;
                        $email->ciclo_id = ($request->ciclo_id !='') ? $request->ciclo_id : 0;;
                        $email->nucleo_id = ($request->nucleo_id !='') ? $request->nucleo_id : 0;
                        $email->diplomados = $diplomados;

                        
                        $email->titulo = $request->titulo;
                        $email->asunto = $request->asunto;
                        $email->mensaje = $request->mensaje;
                        $email->estatus = 0;


                        $array_email['to_emails'] =  $usuarios;
                        $array_email['subject'] = $request->asunto;
                        $array_email['title'] = $request->titulo;
                        $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $request->mensaje . '</p>';
                        $response = $email->save();
                        
                        $email_last = Email::all()->last();
                        $email_id = $email_last->id;
                
                        $response_image = Email::upload_image($_FILES, $email_id);
                        
                        if($response_image){
                            $email_imagen = Email::find($email_id);
                            $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
                        }
                        
                        if($response){
                            if($this->email_helper->send($array_email) ){
                                Session::flash('correo-class', 'alert-success');
                                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');

                                $email_last = Email::all()->last();
                                $email_last->estatus = 1;
                                $email_last->save();

                            }else{
                                Session::flash('correo-class', 'alert-warning');
                                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');

                            }
                        }else{
                            Session::flash('correo-class', 'alert-danger');
                            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
                        }
                                
                    }
                    else{
                        $email = new Email;

                        $email->usuarios = User::get_email();

                        $email->ciclo_id = 0;
                        $email->nucleo_id = 0;
                        $email->diplomados = '';

                        
                        $email->titulo = $request->titulo;
                        $email->asunto = $request->asunto;
                        $email->mensaje = $request->mensaje;
                        $email->estatus = 0;


                        $array_email['to_emails'] =  $email->usuarios;
                        $array_email['subject'] = $request->asunto;
                        $array_email['title'] = $request->titulo;
                        $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $request->mensaje . '</p>';
                        $response = $email->save();
                        
                        $email_last = Email::all()->last();
                        $email_id = $email_last->id;
                
                        $response_image = Email::upload_image($_FILES, $email_id);
                        
                        if($response_image){
                            $email_imagen = Email::find($email_id); 
                            $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email_imagen->imagen_uno) .'"> ';
                        }

                        if($response){
                            if($this->email_helper->send($array_email) ){
                                Session::flash('correo-class', 'alert-success');
                                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');

                                $email_last = Email::all()->last();
                                $email_last->estatus = 1;
                                $email_last->save();

                            }else{
                                Session::flash('correo-class', 'alert-warning');
                                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente, aunque si se guardo puedes reenviarlo de nuevo');

                            }
                        }else{
                            Session::flash('correo-class', 'alert-danger');
                            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente ni se ha guardado');
                        }
                    }
                }

                return redirect('mensajes');

                /*//estos va porque si.

                $data['to_emails'] =  'josuebohorquezc@gmail.com';
                $data['subject'] = $email->subject;
                $data['title'] = $email->title;
                $data['message'] = $email->message;
                //

                $response = $email->save();
                
                if($response)
                    $this->email_helper->send($data);

                return redirect('mensajes');	*/
        }
	
    	
    }
    public function reenviar_mensaje(Request $request){
        $email = Email::find($request->id);
        if($email->count() > 0){
            $array_email['to_emails'] =  $email->usuarios;
            $array_email['subject'] = $email->asunto;
            $array_email['title'] = $email->titulo;
            $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $email->mensaje . '</p>';
            
            if(!empty($email->imagen_uno )){
                $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email->imagen_uno) .'"> ';
            }
            if($this->email_helper->send($array_email) ){
                if($email->estatus == 0){
                    $email->estatus = 1;
                    $email->save();
                }
                Session::flash('correo-class', 'alert-success');
                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');
                return redirect('mensajes');
            }else{
                Session::flash('correo-class', 'alert-danger');
                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente');
                return redirect('mensajes');

            }
        }else{
            Session::flash('correo-class', 'alert-danger');
            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente');
            return redirect('mensajes');
        }
        
    }
    
    
    public function reenviar_mensaje_programado(Request $request){
        $email = EmailProgramado::find($request->id);
        if($email->count() > 0){
            $array_email['to_emails'] =  $email->usuarios;
            $array_email['subject'] = $email->asunto;
            $array_email['title'] = $email->titulo;
            $array_email['message'] = '<p style="font-size:1.5em;"> Mensaje: ' . $email->mensaje . '</p>';
            if(!empty($email->imagen_uno )){
                $array_email['message'] .= '<img style="display:block; width:500px; height: 500px;" src="'. url($email->imagen_uno) .'"> ';
            }
            if($this->email_helper->send($array_email) ){
                Session::flash('correo-class', 'alert-success');
                Session::flash('msg-correo', 'El correo se ha enviado satisfactoriamente');
                return redirect('mensajes_programados');
            }else{
                Session::flash('correo-class', 'alert-danger');
                Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente');
                return redirect('mensajes_programados');

            }
        }else{
            Session::flash('correo-class', 'alert-danger');
            Session::flash('msg-correo', 'El correo no se enviado satisfactoriamente');
            return redirect('mensajes_programados');
        }
        
    }
    /**
     * Delete Property Type
     *
     * @param array $request    Input values
     * @return redirect     to Correos crear
     */
    public function delete(Request $request)
    {
        if($request->ajax())
        {
            $response = Email::destroy($request->id);
            if($response)
              return response()->json('El correo ha sido eliminado');
            else 
              return response()->json('El correo no se ha podido eliminar recargue la pagina e intente de nuevo.');
        }
        
    }
    
    
    
}
