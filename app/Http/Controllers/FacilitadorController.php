<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Facilitador;
use App\Models\Profesion;
use Validator;
use Session;

class FacilitadorController extends Controller
{
    public function show () 
    {
    	$data['facilitadores'] = Facilitador::orderBy('created_at', 'desc')->get(); 
    	$data['numero_facilitadores'] = count($data['facilitadores']); 
    	return view('admin.facilitadores.showall', $data);
    }
    
    public function crear_facilitador (Request $request)
    {

        if(!$_POST) 
        {
            $data['profesiones'] = Profesion::dropdown();
            return view('admin.facilitadores.crear', $data);
        }

    	if($_POST)
    	{ 
    		$rules = array(
                    'nombre_completo' => 'required',
                    'cedula' => 'numeric|required|unique:facilitators|min:1000000|max:99999999',
                    'correo' => 'required|email|unique:facilitators',
                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'nombre_completo' => 'Nombre',
                    	'cedula' => 'Cedula',
                    	'correo' => 'Correo ',
                    		
                    );
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'numeric' => 'La :attribute debe ser un número.',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 numeros.',
                'min' => 'La :attribute debe tener al menos 7 numeros.',
                'email' => 'El :attribute debe ser una dirección de correo electrónico válida.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
                    $facilitador = new Facilitador;
                    $facilitador->nombre_completo = $request->nombre_completo;
                    $facilitador->cedula = $request->cedula;
                    $facilitador->correo = $request->correo;
                    $facilitador->profesion_id = $request->profesion;
                    $facilitador->save();

                $facilitador_last = Facilitador::all()->last();
                $facilitador_id = $facilitador_last->id;

                if(isset($_FILES["foto"]["name"]) && $_FILES["foto"]["name"] != '')
                {
                   
                    $tmp_name = $_FILES["foto"]["tmp_name"];

                    $name = str_replace(' ', '_', $_FILES["foto"]["name"]);
                    
                    $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                    $name = time().'_'.$name;

                    $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/facilitadores/'.$facilitador_id;
                                    
                    if(!file_exists($filename))
                    {
                        mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/facilitadores/'.$facilitador_id, 0777, true);
                    }
                                               
                    if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')   
                    {            
                        if(move_uploaded_file($tmp_name, "images/facilitadores/".$facilitador_id."/".$name))
                        {
                            $facilitador_last          = Facilitador::find($facilitador_id);
                            $facilitador_last->foto = "images/facilitadores/".$facilitador_id."/".$name;
                            $facilitador_last->save();
                        }
                    }
                    else
                    {
                        Session::flas('msg-error-foto'. 'El formato que intentaste subir de imagen no es valido');
                        return redirect('facilitadores');
                    }
                }
                return redirect('facilitadores');	
	        }
    		
    	}
	        
    }
    public function editar_facilitador (Request $request)
    {

        if(!$_POST) 
        {
            $data['facilitador'] = Facilitador::find($request->id);
            $data['profesiones'] = Profesion::dropdown();
            if(count($data['facilitador']) == 0){
                return redirect('facilitadores');
            }
            return view('admin.facilitadores.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                'nombre_completo' => 'required',
                'cedula' => 'numeric|required|min:100000|max:99999999',
                'correo' => 'required|email', 
            );

            // Add Validation Custom Names
            $niceNames = array(
                'nombre_completo' => 'Nombre',
                'cedula' => 'Nombre',
                'correo' => 'Apellido',
                    		
            );

            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'numeric' => 'La :attribute debe ser un número.',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 numeros.',
                'min' => 'La :attribute debe tener al menos 7 numeros.',
                'email' => 'El :attribute debe ser una dirección de correo electrónico válida.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $facilitador = Facilitador::find($request->id);
	    		$facilitador->nombre_completo = $request->nombre_completo;
                $facilitador->cedula = $request->cedula;
	    		$facilitador->correo = $request->correo;
	    		$facilitador->profesion_id = $request->profesion;
                $facilitador->save();

                $facilitador_id = $request->id;

                
                

                if(isset($_FILES["foto"]["name"]) && $_FILES["foto"]["name"] != '')
                {
                    $tmp_name = $_FILES["foto"]["tmp_name"];

                    $name = str_replace(' ', '_', $_FILES["foto"]["name"]);
                    
                    $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                    $name = time().'_'.$name;

                    $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/facilitadores/'.$facilitador_id;
                                    
                    if(!file_exists($filename))
                    {
                        mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/facilitadores/'.$facilitador_id, 0777, true);
                    }
                                               
                    if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')   
                    {            
                        if(move_uploaded_file($tmp_name, "images/facilitadores/".$facilitador_id."/".$name))
                        {
                            $facilitador_last          = Facilitador::find($facilitador_id);
                            $facilitador_last->foto = "images/facilitadores/".$facilitador_id."/".$name;
                            $facilitador_last->save();
                        }
                    }
                    else
                    {
                        Session::flas('msg-erro-foto'. 'El formato que intentaste subir de imagen no es valido');
                        return redirect('facilitadores');
                    }

                }
	    	
                return redirect('facilitadores');   
            }
            
        }
            
    }
}
