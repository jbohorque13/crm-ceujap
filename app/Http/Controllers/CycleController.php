<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Cycle;
use Validator;


class CycleController extends Controller
{
    public function show () 
    {
    	$data['ciclos'] = Cycle::orderBy('created_at', 'desc')->get();
    	$data['numero_ciclo'] = count($data['ciclos']);
    	return view('admin.ciclos.showall', $data);
    }
   
    public function crear_ciclo (Request $request)
    {

        if(!$_POST) 
        {
            return view('admin.ciclos.crear');
   
        }

    	if($_POST)
    	{
    		$rules = array(
                    'codigo' => 'required',
                    'descripcion' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'codigo' => 'Codigo',
                    	'descripcion' => 'Descripcion',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
	        	$ciclo = new Cycle;
	    		$ciclo->codigo = $request->codigo;
	    		$ciclo->descripcion = $request->descripcion;
                $ciclo->fecha_inicio = $request->fecha_inicio;
	    		$ciclo->fecha_fin 	= $request->fecha_fin;
	    		$ciclo->save();
	    			
	    		return redirect('ciclos');	
	        }
    		
    	}
	        
    }
    public function editar_ciclo (Request $request)
    {

        if(!$_POST) 
        {
            $data['ciclos'] = Cycle::find($request->id);
            if(count($data['ciclos']) == 0){
                return redirect('ciclos'); 
            }
            return view('admin.ciclos.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                    'codigo' => 'required',
                    'descripcion' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'codigo' => 'Codigo',
                    	'descripcion' => 'Descripcion',
                );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $ciclo = Cycle::find($request->id);
                $ciclo->codigo = $request->codigo;
                $ciclo->descripcion = $request->descripcion;
                $ciclo->fecha_inicio = $request->fecha_inicio;
                $ciclo->fecha_fin = $request->fecha_fin;
                $ciclo->save();
                    
                return redirect('ciclos');   
            }
            
        }
            
    }

    
}
