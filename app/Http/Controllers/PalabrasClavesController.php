<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\PalabrasClaves;
use Validator;

class PalabrasClavesController extends Controller
{
    public function show () 
    {
    	$data['palabras_claves'] = PalabrasClaves::active_all();
    	$data['numero_palabras_claves'] = count($data['palabras_claves']); 
    	return view('admin.palabras-claves.showall', $data);
    }
    
    public function crear_palabra_clave (Request $request)
    {
    	$palabra_clave_en_minuscula = strtolower($request->nombre);
    	$request->nombre = ucwords($palabra_clave_en_minuscula);

        if(!$_POST) 
        {
            return view('admin.palabras-claves.crear');
   
        }

    	if($_POST)
    	{
    		$rules = array(
                    'nombre' => 'required|unique:palabras_claves',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'nombre' => 'Nombre',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
	        	$palabra_clave = new PalabrasClaves;
	    		$palabra_clave->nombre = $request->nombre;
                $palabra_clave->descripcion = $request->nombre;
	    		$palabra_clave->save();
	    			
	    		return redirect('palabras_claves');	
	        }
    		
    	}
	        
    }
    public function editar_nucleo (Request $request)
    {

        if(!$_POST) 
        {
            $data['nucleo'] = Nucleus::find($request->id);
            if(count($data['nucleo']) == 0){
                return redirect('nucleos');
            }
            return view('admin.nucleos.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                    'codigo' => 'required',
                    'nombre_sede' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                        'codigo' => 'Codigo',
                        'nombre_sede' => 'Nombre de Sede',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $nucleo = Nucleus::find($request->id);

                $nucleo->codigo = $request->codigo;
                $nucleo->nombre_sede = $request->nombre_sede;
                $nucleo->ciudad = $request->ciudad;
                $nucleo->save();
                    
                return redirect('nucleos');   
            }
            
        }
            
    }
}
