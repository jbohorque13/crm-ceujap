<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Contactenos;
use Validator;
use Session;

class ContactenosController extends Controller
{
    public function show () 
    {
    	$data['sugerencias'] = Contactenos::orderBy('created_at', 'desc')->paginate(7);
    	$data['numero_sugerencias'] = count($data['sugerencias']); 
    	return view('admin.sugerencias.showall', $data);
    }
    


    public function enviar_contacto (Request $request)
    {


    	if($_POST)
    	{
    		$rules = array(
                  

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
	        	$contacto = new Contactenos;
	    		$contacto->asunto = $request->asunto;
                $contacto->mensaje = $request->mensaje;
	    		$contacto->usuario_id = $request->usuario_id;
                $contacto->palabra_clave_id = $request->palabras_claves_id;
	    		$response = $contacto->save();

    			if($response){
                    Session::flash('msg-contacto', 'Su mensaje se ha enviado satistafactoriamente, pronto le responderemos');

                }
                else{
                    Session::flash('msg-contacto', 'Su mensaje no se ha enviado');
                }
	    		return redirect('usuario/contactenos');	
	        }
    		
    	}
	        
    }
    public function ver_sugerencia (Request $request)
    {

        $sugerencia = Contactenos::find($request->id);
        if($sugerencia->visto == 0){
            $sugerencia->visto = 1;
            $sugerencia->update();
            $sugerencia = Contactenos::find($request->id);
        }
        $data['sugerencia'] = $sugerencia;

        if(count($data['sugerencia']) == 0){
            return redirect('sugerencias');
        }
        return view('admin.sugerencias.ver', $data);
   
    }
    public function delete (Request $request)
    {
        $response = Contactenos::find($request->id)->delete();
        
        if($response)
        {
          return response()->json('El mensaje fue eliminado exictosamente');
        }
        else {
          return response()->json('No se pudo eliminar el mensaje, porfavor recarge la pagina he intente de nuevo.');
        }
   
    }
}
