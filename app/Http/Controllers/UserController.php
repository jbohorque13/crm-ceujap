<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Diplomado;
use App\Models\Module;
use App\Models\Facilitador;
use App\Models\Questionnaire;
use App\Models\RelationshipUsersDiplomados;
use App\Models\RelationshipFacilitatorsDiplomados;
use App\Models\PalabrasClaves;
use App\Models\RelationshipNucleusDiplomados;

use App\Models\RelationshipModulesDiplomados;

use App\Models\Profesion;

use App\Http\Helpers\EmailHelper;

use Validator;
use Session;
use Auth;
use DB;

class UserController extends Controller
{
    private $email;
    
    protected $email_helper; // Global variable for Helpers instance
    
    public function __construct(EmailHelper $email)
    {
        $this->email_helper = $email;
    }
    
    public function index(){

        if(session()->has('msg')){
            Session::flash('msg', '');    
        }

        $usuarios_diplomado = DB::table('diplomados')
                ->join('relationship_users_diplomados', function($join) {
                    $join->on('diplomados.id', '=', 'relationship_users_diplomados.diplomado_id');
                })
                ->join('users', 'users.id', '=', 'relationship_users_diplomados.usuario_id')
                ->get();

        $array_diplomados = array();
        for($i=0; $i < count($usuarios_diplomado); $i++){
            if($usuarios_diplomado[$i]->usuario_id == Auth::user()->id){

                $array_diplomados = array_add($array_diplomados, $usuarios_diplomado[$i]->diplomado_id, $usuarios_diplomado[$i]->titulo);
            }
        }
        $data['array_diplomados'] = $array_diplomados;

        $data['preguntas'] = Questionnaire::active_all();

        $data['usuario'] = User::find(Auth::user()->id);

        return view('users.home.home', $data); 
    }
    public function show () 
    {
      $data['usuarios'] = User::orderBy('created_at', 'desc')->get();
      $data['numero_usuarios'] = count($data['usuarios']); 
      return view('admin.usuarios.showall', $data);
    }
    
    public function crear_usuario (Request $request)
    {

        if(!$_POST) 
        {
            $data['diplomados'] = Diplomado::dropdown();
            $data['profesiones'] = Profesion::dropdown();

            return view('admin.usuarios.crear', $data);
   
        }

      if($_POST)
      { 
            $rules = array(
               'nombre_completo' => 'required',
               'cedula' => 'numeric|required|unique:users|min:100000|max:99999999',
               'email' => 'required|email|unique:users',
               'nucleo_id' => 'required',
                'diplomado_id' => 'required',
                'profesion' => 'required',
            );

            // Add Validation Custom Names
            $niceNames = array(
                'nombre_completo' => 'Nombre Completo',
                'cedula' => 'Cedula',
                'email' => 'Correo',
                'diplomado_id' => 'Diplomado',
                'profesion' => 'Profesion'
            );
            
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'numeric' => 'La :attribute debe ser un número.',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 numeros.',
                'min' => 'La :attribute debe tener al menos 7 numeros.',
                'email' => 'El :attribute debe ser una dirección de correo electrónico válida.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
              return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
            $clave = str_random(6);
            Session::put('clave_usuario', $clave);
            $email_user =  $request->email; 
            $usuario = new User;
            $usuario->nombre_completo = $request->nombre_completo;
            $usuario->password = bcrypt($clave);
            $usuario->cedula = $request->cedula;
            $usuario->email = $request->email;
            $usuario->profesion_id = $request->profesion;
            $usuario->nucleo_id = $request->nucleo_id;
            /*$usuario->usuario_facebook = $request->usuario_facebook;
            $usuario->usuario_twitter = $request->usuario_twitter;
            */
            $relacion_usuario_diplomado = new RelationshipUsersDiplomados;
            $relacion_usuario_diplomado->diplomado_id = $request->diplomado_id;
            $relacion_usuario_diplomado->usuario_id = $request->diplomado_id;

            if($usuario->save()){

                $usuario_last = User::all();
                $usuario_id = $usuario_last->last()->id;

                $relacion_usuario_diplomado = new RelationshipUsersDiplomados;
                $relacion_usuario_diplomado->diplomado_id = $request->diplomado_id;
                $relacion_usuario_diplomado->usuario_id = $usuario_id;

                if($relacion_usuario_diplomado->save()){

                }
                $data['to_emails'] =  $email_user;
                $data['subject'] = 'Haz sido agregado en el sistema CEUJAP';
                $data['title'] = 'Credencial de registro';
                $data['message'] = '<h1> Credenciales para ingresar al sistema </h1>';
                $data['message'] .= '<p> Usuario: '. $email_user . ' </p>';
                $data['message'] .= '<p> Clave: '. $clave . ' </p>';

                $this->email_helper->send($data);
                
            }
            
          return redirect('participantes'); 
          }
        
      }
          
    }
    public function editar_usuario (Request $request)
    {

        if(!$_POST) 
        {
            $data['usuarios'] = User::find($request->id);
            if(count($data['usuarios']) == 0){
                return redirect('usuarios');
            }
            $data['profesiones'] = Profesion::dropdown();
            $data['diplomados'] = Diplomado::dropdown();
            return view('admin.usuarios.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                'nombre_completo' => 'required',
                'cedula' => 'required|min:100000|max:99999999',
                'email' => 'required|email',
                'nucleo_id' => 'required',
            );

            // Add Validation Custom Names
            $niceNames = array(
                'nombre_completo' => 'Nombre Completo',
                'cedula' => 'Cedula',
                'email' => 'Correo ',
            );
            
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'numeric' => 'La :attribute debe ser un número.',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 numeros.',
                'min' => 'La :attribute debe tener al menos 7 numeros.',
                'email' => 'El :attribute debe ser una dirección de correo electrónico válida.'
            ];
            

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $usuario = User::find($request->id);
                $usuario->nombre_completo = $request->nombre_completo;
                $usuario->cedula = $request->cedula;
                $usuario->email = $request->email;
                $usuario->profesion_id = $request->profesion;
                $usuario->nucleo_id = $request->nucleo_id;/*
                $usuario->usuario_facebook = $request->usuario_facebook;
                $usuario->usuario_twitter = $request->usuario_twitter;
                
        */
                if($usuario->save()){
                    $data['to_emails'] =  $request->email;
                    $data['subject'] = 'Los nuevos datos de acceso al sistema CEUJAP';
                    $data['title'] = 'Credencial de registro';
                    $data['message'] = '<h1> Tu correo fue modificado y asi que debes guardar tu nuevo usuario </h1>';
                    $data['message'] .= '<p> Usuario: '.$request->email . ' </p>';

                    $this->email_helper->send($data);
                }

                return redirect('participantes');   
            }
            
        }
            
    }
    public function contactenos()
    {
        $data['palabras_claves'] = PalabrasClaves::dropdown();
        return view('users.contactenos.ver', $data);
        
    }
    public function login(){
        return view('users.login.login'); 
        
    }

    public function authenticate(Request $request){

        // Correo login validation rules
        $rules = array(
        'email'           => 'required|email',
        'password'        => 'required'
        );

        // Correo login validation custom messages
        $messages = array(
        'required'        => 'El Campo :attribute es requerido.'
        );  

        // correo login validation custom Fields name
        $niceNames = array(
        'email'           => 'Usuario',
        'password'        => 'Clave',
        );

        // set the remember me cookie if the user check the box

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }
        else
        {
            // Get user status
            $users = User::where('email', $request->email)->first();
            if(count($users) == 0){
                Session::flash('msg-login', 'Tus datos no tienen acceso a nuestro sistema, porfavor comunicate con el administrador para que te asigne un usuario y clave');
            }
            // Check user is active or not
            if(@$users->estatus != 0)
            {
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
                {
                    return redirect()->intended('usuario/inicio'); // Redirect to dashboard page
                }
                else
                {
                    Session::flash('msg-login', 'El correo o la Contraseña  son incorrectas. intente de nuevo');
                    return redirect('usuario/ingresar'); // Redirect to login page
                } 
            }
            else    // Call Disabled view file for Inactive user
            {
                return redirect('usuario/ingresar');
            }
        }
        
    }

    public function ajax_get_facilitadores_modulos(Request $request)
    {

        $modulos         = RelationshipModulesDiplomados::with(['modules'])->where('diplomado_id', $request->diplomado_id)->get();
        /*$array_modulo = array();
        if(count($modulos) > 0){
            foreach($modulos as $row_modulo){
                $array_modulo = array_add($array_modulo, 'modulo_id', $row_modulo->id);
                $array_modulo = array_add($array_modulo, 'modulo_codigo', $row_modulo->codigo);
            }
        }*/

        $facilitadores = RelationshipFacilitatorsDiplomados::with(['facilitators'])->where('diplomado_id', $request->diplomado_id)->get();
/*
        $array_facilitadores = array();
        
        $array_facilitador = array();

        $contador = 0;

        dd($facilitadores[1]->facilitators[0]->nombre_completo);
        $variable_de_prueba = '';

        if(count($facilitadores) > 0){
            for($i=0; $i < count($facilitadores); $i++){
                
                $array_facilitadores = array_add($array_facilitadores, 'diplomado_id', $facilitadores[$i]->facilitators[0]->id);
                $array_facilitadores = array_add($array_facilitadores, 'diplomado_nombre', $facilitadores[$i]->facilitators[0]->nombre_completo);

                $variable_de_prueba .= $facilitadores[$i]->facilitators[0]->nombre_completo . ' ';

                $array_facilitador = array_add($array_facilitador, $contador++ , $array_facilitadores);
            }
        }*/

        //dd($array_facilitador);

        return json_encode(['success'=>'true', 'modulos' => $modulos, 'facilitadores' => $facilitadores]);
    }

    public function ajax_get_nucleos(Request $request)
    {

        $nucleos = RelationshipNucleusDiplomados::with(['nucleus'])->where('diplomado_id', $request->diplomado_id)->get();
        
        return json_encode(['success'=>'true', 'nucleos' => $nucleos]);
    }


    
    
    
}
