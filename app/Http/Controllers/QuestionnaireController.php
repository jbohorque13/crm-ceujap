<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Questionnaire;
use App\Models\ResultsQuestionnaire;
use Validator;
use Session;

class QuestionnaireController extends Controller
{
    public function show () 
    {
    	$data['cuestionarios'] = Questionnaire::paginate(4);
    	$data['numero_cuestionario'] = count($data['cuestionarios']);
    	return view('admin.questionnaires.showall', $data);
    }
    
    public function crear_cuestionario (Request $request)
    {

        if(!$_POST) 
        {
            $data['numero_de_preguntas'] = 9;
            return view('admin.questionnaires.create', $data);
   
        }

    	if($_POST)
    	{
    		$rules = array(
                    'pregunta_1' => 'required',
                    'pregunta_2' => 'required',

                    'pregunta_3' => 'required',
                    'pregunta_4' => 'required',

                    'pregunta_5' => 'required',
                    'pregunta_6' => 'required',

                    'pregunta_7' => 'required',
                    'pregunta_8' => 'required',

                    'pregunta_9' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'pregunta_1' => 'Pregunta #1',
                    	'pregunta_2' => 'Pregunta #2',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {
	        	$cuestionario = new Questionnaire;
	    		$cuestionario->pregunta_1 = $request->pregunta_1;
	    		$cuestionario->pregunta_2 = $request->pregunta_2;
                $cuestionario->pregunta_3 = $request->pregunta_3;
	    		$cuestionario->pregunta_4 = $request->pregunta_4;
	    		$cuestionario->pregunta_5 = $request->pregunta_5;
                $cuestionario->pregunta_6 = $request->pregunta_6;
                $cuestionario->pregunta_7 = $request->pregunta_7;
                $cuestionario->pregunta_8 = $request->pregunta_8;
                $cuestionario->pregunta_9 = $request->pregunta_9;
	    		$cuestionario->save();
	    			
	    		return redirect('cuestionarios');	
	        }
    		
    	}
	        
    }
    public function editar_cuestionario (Request $request)
    {

        if(!$_POST) 
        {
            $data['cuestionario'] = Questionnaire::get();
            if(count($data['cuestionario']) == 0){
                return redirect('cuestionarios');
            }
            $data['numero_de_preguntas'] = 9;
            return view('admin.questionnaires.edit', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                    
                    'pregunta_1' => 'required',
                    'pregunta_2' => 'required',

                    'pregunta_3' => 'required',
                    'pregunta_4' => 'required',

                    'pregunta_5' => 'required',
                    'pregunta_6' => 'required',

                    'pregunta_7' => 'required',
                    'pregunta_8' => 'required',

                    'pregunta_9' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                        'pregunta_1' => 'Pregunta #1',
                        'pregunta_2' => 'Pregunta #2',
                    );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $cuestionario = Questionnaire::find($request->id);
                $cuestionario->pregunta_1 = $request->pregunta_1;
                $cuestionario->pregunta_2 = $request->pregunta_2;
                $cuestionario->pregunta_3 = $request->pregunta_3;
                $cuestionario->pregunta_4 = $request->pregunta_4;
                $cuestionario->pregunta_5 = $request->pregunta_5;
                $cuestionario->pregunta_6 = $request->pregunta_6;
                $cuestionario->pregunta_7 = $request->pregunta_7;
                $cuestionario->pregunta_8 = $request->pregunta_8;
                $cuestionario->pregunta_9 = $request->pregunta_9;

                $cuestionario->save();
                    
                return redirect('cuestionarios');   
            }
            
        }
            
    }

    
    public function delete (Request $request) 
    {
        if($request->ajax())
        {
            $response = Questionnaire::destroy($request->id);
            if($response)
              return response()->json('El cuestionario ha sido eliminado');
            else 
              return response()->json('El cuestionario no se ha podido eliminar');
        }
    }

    public function responder_cuestionario(Request $request)
    {

        $rules = array(
                'pregunta_1' => 'required',
                'pregunta_2' => 'required',

            );

        // Add Validation Custom Names
        $niceNames = array(
                    'pregunta_1' => 'Pregunta #1',
                    'pregunta_2' => 'Pregunta #2',
                );

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($niceNames); 

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }
        else
        {

            $resultado_cuestionario = ResultsQuestionnaire::where('diplomado_id', '=', $request->diplomado_id)
                                                            ->where('modulo_id', '=', $request->modulo_id)
                                                            ->where('facilitador_id', '=', $request->facilitador_id)
                                                            ->get();

            if(count($resultado_cuestionario) > 0){
                $resultado_cuestionario[0]->porcentaje_facilitadores = ($resultado_cuestionario[0]->porcentaje_facilitadores + (($request->pregunta_1 + $request->pregunta_2 +  $request->pregunta_3 + $request->pregunta_4 + $request->pregunta_5 +$request->pregunta_6) * 100) / 24) / 2;

                $resultado_cuestionario[0]->porcentaje_contenido = ($resultado_cuestionario[0]->porcentaje_contenido + (( $request->pregunta_7 + $request->pregunta_8 + $request->pregunta_9 ) * 100) / 12) / 2;

                $resultado_cuestionario[0]->respuesta_1 = ($resultado_cuestionario[0]->respuesta_1 + ($request->pregunta_1 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_2 = ($resultado_cuestionario[0]->respuesta_2 + ($request->pregunta_2 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_3 = ($resultado_cuestionario[0]->respuesta_3 + ($request->pregunta_3 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_4 = ($resultado_cuestionario[0]->respuesta_4 + ($request->pregunta_4 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_5 = ($resultado_cuestionario[0]->respuesta_5 + ($request->pregunta_5 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_6 = ($resultado_cuestionario[0]->respuesta_6 + ($request->pregunta_6 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_7 = ($resultado_cuestionario[0]->respuesta_7 + ($request->pregunta_7 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_8 = ($resultado_cuestionario[0]->respuesta_8 + ($request->pregunta_8 * 100) / 4) / 2;
                $resultado_cuestionario[0]->respuesta_9 = ($resultado_cuestionario[0]->respuesta_9 + ($request->pregunta_9 * 100) / 4) / 2;
                $response = $resultado_cuestionario[0]->save();

            }
            else{
                $respuesta = new ResultsQuestionnaire;
                $respuesta->diplomado_id = $request->diplomado_id;
                $respuesta->modulo_id = $request->modulo_id;
                $respuesta->facilitador_id = $request->facilitador_id;
                $respuesta->nucleo_id = $request->nucleo_id;

                $respuesta->respuesta_1 = ($request->pregunta_1 * 100) / 4;
                $respuesta->respuesta_2 = ($request->pregunta_2 * 100) / 4;
                $respuesta->respuesta_3 = ($request->pregunta_3 * 100) / 4;
                $respuesta->respuesta_4 = ($request->pregunta_4 * 100) / 4;
                $respuesta->respuesta_5 = ($request->pregunta_5 * 100) / 4;
                $respuesta->respuesta_6 = ($request->pregunta_6 * 100) / 4;
                $respuesta->respuesta_7 = ($request->pregunta_7 * 100) / 4;
                $respuesta->respuesta_8 = ($request->pregunta_8 * 100) / 4;
                $respuesta->respuesta_9 = ($request->pregunta_9 * 100) / 4;

                $respuesta->porcentaje_facilitadores = (($request->pregunta_1 + $request->pregunta_2 +  $request->pregunta_3 + $request->pregunta_4 + $request->pregunta_5 +$request->pregunta_6) * 100) / 24;


                $respuesta->porcentaje_contenido = (( $request->pregunta_7 + $request->pregunta_8 + $request->pregunta_9 ) * 100) / 12;
                $response = $respuesta->save();
            }

            if($response){
                Session::flash('msg-cuestionario', 'Se ha enviado el cuestionario exictosamente.');    
            }
            else{
                 Session::flash('msg-cuestionario', 'No se ha enviado el cuestionario debido a un problema, intente de nuevo.');  
            }
            

            return redirect('usuario/inicio');   
        }
    }
            
}
