<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Module;
use App\Models\Cycle;
use Validator;


class ModulesController extends Controller
{
    public function show () 
    {
    	$data['modulos'] = Module::orderBy('created_at', 'desc')->get();
    	$data['numero_modulo'] = count($data['modulos']);
    	return view('admin.modulos.showall', $data);
    }
   
    public function crear_modulo (Request $request)
    {

        if(!$_POST) 
        {
            return view('admin.modulos.crear');
   
        }

    	if($_POST)
    	{
    		$rules = array(
                    'codigo' => 'required',
                    'descripcion' => 'required',
                    'cohorte' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'codigo' => 'Codigo',
                    	'descripcion' => 'Descripcion',
                        'cohorte' => 'Cohorte'
                    );
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
	        {
	            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
	        }
	        else
	        {	

	        	$modulo = new Module;
	    		$modulo->codigo = $request->codigo;
	    		$modulo->descripcion = $request->descripcion;
                        $modulo->cohorte = $request->cohorte;
	    		$modulo->save();
	    			
	    		return redirect('modulos');	
	        }
    		
    	}
	        
    }
    public function editar_modulo (Request $request)
    {

        if(!$_POST) 
        {
            $data['modulos'] = Module::find($request->id);
            if(count($data['modulos']) == 0){
                return redirect('modulos'); 
            }
            return view('admin.modulos.editar', $data);
   
        }

        if($_POST)
        {
            $rules = array(
                    'codigo' => 'required',
                    'descripcion' => 'required',
                    'cohorte' => 'required',

                );

            // Add Validation Custom Names
            $niceNames = array(
                    	'codigo' => 'Codigo',
                    	'descripcion' => 'Descripcion',
                        'cohorte' => 'Cohorte'
                    );
            $messages = [
                'required' => 'El Campo :attribute es requerido',
                'unique' => 'Esta :attribute ya esta registrada.',
                'max' => 'La :attribute no puede tener más de 8 caracteres.',
                'min' => 'La :attribute debe tener al menos 7 caracteres.',
                ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $modulo = Module::find($request->id);
                $modulo->codigo = $request->codigo;
                $modulo->descripcion = $request->descripcion;
                $modulo->cohorte = $request->cohorte;
                $modulo->save();
                    
                return redirect('modulos');   
            }
            
        }
            
    }

}
