<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('usuario/ingresar','UserController@login');

Route::post('usuario/login', 'UserController@authenticate');

Route::group(['middleware' => 'auth'], function () {
/* ROUTES PARA LA PARTE DEL FRONT USER */

	Route::get('usuario/contactenos','UserController@contactenos');
 
	Route::get('usuario/inicio', 'UserController@index');

	Route::post('get_facilitadores_modulos', 'UserController@ajax_get_facilitadores_modulos');

/* ROUTES PARA LA PARTE DEL FRONT CUESTIONARIO */

	Route::post('cuestionario/respondido', 'QuestionnaireController@responder_cuestionario');

	//Route::get('cuestionario/respondido', 'QuestionnaireController@responder_cuestionario');
}); 


/* ROUTES PARA SUGERENCIA */
Route::get('sugerencias','ContactenosController@show');

Route::match(['get', 'post'], 'enviar_contacto', 'ContactenosController@enviar_contacto');  

Route::get('ver_sugerencia/{id}', 'ContactenosController@ver_sugerencia');

Route::get('delete_sugerencia', 'ContactenosController@delete');


/* ROUTES PARA USARIO */
Route::get('participantes','UserController@show');

Route::match(['get', 'post'], 'crear_participante', 'UserController@crear_usuario');  

Route::match(['get', 'post'], 'editar_participante/{id}', 'UserController@editar_usuario');

Route::post('ajax_get_nucleos', 'UserController@ajax_get_nucleos');

Route::get('delete_questionnaire', 'QuestionnaireController@delete');

/* ROUTES PARA MODULO */
Route::get('modulos','ModulesController@show');

Route::match(['get', 'post'], 'crear_modulo', 'ModulesController@crear_modulo');  

Route::match(['get', 'post'], 'editar_modulo/{id}', 'ModulesController@editar_modulo');

Route::post('delete_modulo', 'ModulesController@delete');


/* ROUTES PARA FACILITADORES */
Route::get('facilitadores','FacilitadorController@show');


Route::match(['get', 'post'], 'crear_facilitador', 'FacilitadorController@crear_facilitador');  

Route::match(['get', 'post'], 'editar_facilitador/{id}', 'FacilitadorController@editar_facilitador');

Route::get('delete_questionnaire', 'QuestionnaireController@delete');

/* ROUTES PARA DIPLOMADOS */
Route::get('diplomados','DiplomadoController@show');


Route::match(['get', 'post'], 'crear_diplomado', 'DiplomadoController@crear_diplomado');  

Route::match(['get', 'post'], 'editar_diplomado/{id}', 'DiplomadoController@editar_diplomado');

Route::post('ajax_get_diplomados', 'DiplomadoController@ajax_get_diplomados');

Route::get('delete_questionnaire', 'QuestionnaireController@delete');

/* ROUTES PARA CUESTIONARIO */
Route::get('cuestionarios','QuestionnaireController@show');


Route::match(['get', 'post'], 'crear_cuestionario', 'QuestionnaireController@crear_cuestionario');

Route::match(['get', 'post'], 'editar_cuestionario', 'QuestionnaireController@editar_cuestionario');

Route::get('delete_questionnaire', 'QuestionnaireController@delete');

/* ROUTES PARA CICLO */

Route::get('ciclos','CycleController@show');

Route::match(['get', 'post'], 'crear_ciclo', 'CycleController@crear_ciclo');

Route::match(['get', 'post'], 'editar_ciclo/{id}', 'CycleController@editar_ciclo'); 

Route::get('delete_email', 'EmailController@delete');

/* ROUTES PARA NUCLEOS */

Route::get('nucleos','NucleusController@show');

Route::match(['get', 'post'], 'crear_nucleo', 'NucleusController@crear_nucleo');

Route::match(['get', 'post'], 'editar_nucleo/{id}', 'NucleusController@editar_nucleo');

Route::get('delete_email', 'EmailController@delete');


/* ROUTES PARA EMAILS */

Route::get('mensajes','EmailController@show');

Route::get('mensajes_programados','EmailController@show_programados'); 

Route::get('envio_mensaje', 'EmailController@manage_emails');

Route::get('envio_mensaje_programado', 'EmailController@envio_programado');

Route::post('enviar_mensaje_programado', 'EmailController@envio_email_programado');

Route::post('crear_email', 'EmailController@create_email');
 
Route::get('ver_mensaje/{id}', 'EmailController@ver_correo');
Route::get('ver_mensaje_programado/{id}', 'EmailController@ver_correo_programado');

Route::get('reenviar_mensaje/{id}', 'EmailController@reenviar_mensaje');

Route::get('reenviar_mensaje_programado/{id}', 'EmailController@reenviar_mensaje_programado');

Route::get('delete_email', 'EmailController@delete');



/* ROUTES PARA HOMECONTROLLER */

Route::get('estadistica/{page}', 'HomeController@show_stadistics')
            ->where(['page' => 'cuestionarios|correos/participantes']);

Route::get('estadistica/facilitador', 'HomeController@estadistica_facilitador');

Route::get('estadistica/diplomado', 'HomeController@estadistica_diplomado');

Route::post('ajax_estadistica_facilitadores', 'HomeController@actualizar_estadistica_facilitador');

Route::post('ajax_estadistica_diplomado', 'HomeController@actualizar_estadistica_diplomado');


/* ROUTES PARA NUCLEOS */

Route::get('palabras_claves','PalabrasClavesController@show');

Route::match(['get', 'post'], 'crear_palabra_clave', 'PalabrasClavesController@crear_palabra_clave');

Route::match(['get', 'post'], 'editar_nucleo/{id}', 'NucleusController@editar_nucleo');

Route::get('delete_email', 'EmailController@delete');



