<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ResultsQuestionnaire;

class Questionnaire extends Model
{
    protected $table = 'questionnaires';
    
    public $timestamps = false;

    public static function active_all()
    {

    	return Questionnaire::whereEstatus(1)->get();
    
    } 
}
