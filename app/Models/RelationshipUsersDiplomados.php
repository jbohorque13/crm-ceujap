<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipUsersDiplomados extends Model
{
    
    protected $table = 'relationship_users_diplomados';

    public function usuarios()
    {
        return $this->hasMany('App\Models\User','id','usuario_id');
    }

}
