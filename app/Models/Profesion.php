<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    protected $table = 'profesiones';
    
    public $timestamps = false;

    public static function get_all()
    {
    	return Profesion::get();
    
    } 
    public static function dropdown()
    {
        return Profesion::lists('nombre','id');
    }
}
