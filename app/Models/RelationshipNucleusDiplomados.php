<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipNucleusDiplomados extends Model
{
    protected $table = 'relationship_nucleus_diplomados';

    public function nucleus()
    {
        return $this->hasMany('App\Models\Nucleus','id','nucleo_id');
    }

    public function diplomados()
    {
        return $this->hasMany('App\Models\Diplomado','id','diplomado_id');
    }
}
