<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facilitador extends Model
{
    protected $table = 'facilitators';

    // Get all Active status records in lists type
    public static function dropdown()
    {
        return Facilitador::whereEstatus(1)->lists('nombre_completo','id');
    }

    public function profesion()
    {
        return $this->belongsTo('App\Models\Profesion','profesion_id','id');
    
    }

}
