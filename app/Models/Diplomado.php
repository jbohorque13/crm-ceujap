<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use DateTime;
use DateTimeZone;
use Config;
use App\Models\User;

class Diplomado extends Model
{
    protected $table = 'diplomados';

    public static function modulos()
    {
        return DB::table('diplomados')
                ->join('relationship_modules_diplomados', function($join) {
                    $join->on('diplomados.id', '=', 'relationship_modules_diplomados.diplomado_id');
                })
                ->join('modules', 'modules.id', '=', 'relationship_modules_diplomados.modulo_id')
                ->get();
    
    }

    public static function nucleos()
    {
        return DB::table('diplomados')
                ->join('relationship_nucleus_diplomados', function($join) {
                    $join->on('diplomados.id', '=', 'relationship_nucleus_diplomados.diplomado_id');
                })
                ->join('nucleus', 'nucleus.id', '=', 'relationship_nucleus_diplomados.nucleo_id')
                ->get();
    
    }
    
    public static function facilitadores () 
    {
        return DB::table('diplomados')
                ->join('relationship_facilitators_diplomados', function($join) {
                    $join->on('diplomados.id', '=', 'relationship_facilitators_diplomados.diplomado_id');
                })
                ->join('facilitators', 'facilitators.id', '=', 'relationship_facilitators_diplomados.facilitador_id')
                ->get();
    }

    public static function dropdown()
    {
        return Diplomado::whereEstatus(1)->lists('titulo','id');
    }
}
