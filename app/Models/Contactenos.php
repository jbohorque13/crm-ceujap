<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contactenos extends Model
{
    protected $table = 'contactenos';

    public function user()
    {
        return $this->belongsTo('App\Models\User','usuario_id','id');
    }

    public function palabras_claves()
    {
        return $this->belongsTo('App\Models\PalabrasClaves','palabra_clave_id','id');
    }

}
