<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
     protected $table = 'cycles';

     public static function array_ciclo()
    {
    	$ciclos = Cycle::whereEstatus(1)->get();
        $array_ciclo = array();

          if(count($ciclos) > 0){
              foreach($ciclos as $row_ciclo){

                  $mes_inicio = date('m', strtotime($row_ciclo->fecha_inicio));

                  switch($mes_inicio){
			                case '01':
			                  $mes_inicio = 'Enero';
			                  break;
			                case '02':
			                  $mes_inicio = 'Febrero';
			                  break;
			                case '03':
			                  $mes_inicio = 'Marzo';
			                  break;
			                case '04':
			                  $mes_inicio = 'Abril';
			                  break;
			                case '05':
			                  $mes_inicio = 'Mayo';
			                  break;
			                case '06':
			                  $mes_inicio = 'Junio';
			                  break;
			                case '07':
			                  $mes_inicio = 'Julio';
			                  break;
			                case '08':
			                  $mes_inicio = 'Agosto';
			                  break;
			                case '09':
			                  $mes_inicio = 'Septiembre';
			                  break;
			                case '10':
			                  $mes_inicio = 'Octubre';
			                  break;
			                case '11':
			                  $mes_inicio = 'Noviembre';
			                  break;
			                case '12':
			                  $mes_inicio = 'Diciembre';
			                  break;
			              }

                  $mes_fin = date('m', strtotime($row_ciclo->fecha_fin));

                  switch($mes_fin){
			                case '01':
			                  $mes_fin = 'Enero';
			                  break;
			                case '02':
			                  $mes_fin = 'Febrero';
			                  break;
			                case '03':
			                  $mes_fin = 'Marzo';
			                  break;
			                case '04':
			                  $mes_fin = 'Abril';
			                  break;
			                case '05':
			                  $mes_fin = 'Mayo';
			                  break;
			                case '06':
			                  $mes_fin = 'Junio';
			                  break;
			                case '07':
			                  $mes_fin = 'Julio';
			                  break;
			                case '08':
			                  $mes_fin = 'Agosto';
			                  break;
			                case '09':
			                  $mes_fin = 'Septiembre';
			                  break;
			                case '10':
			                  $mes_fin = 'Octubre';
			                  break;
			                case '11':
			                  $mes_fin = 'Noviembre';
			                  break;
			                case '12':
			                  $mes_fin = 'Diciembre';
			                  break;
			              }

			    

                  $array_ciclo = array_add($array_ciclo, $row_ciclo->id, $mes_inicio . ' hasta ' . $mes_fin);
              }
              return $array_ciclo;

          }

    }
    public static function active_all()
    {
        return Cycle::whereEstatus(1)->get();
    }

}
