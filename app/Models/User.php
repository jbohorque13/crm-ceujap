<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profesion()
    {
        return $this->belongsTo('App\Models\Profesion','profesion_id','id');
    
    }

    public static function get_email(){
        $to_emails = '';

        $usuarios = User::get();

        for($i=0; $i < count($usuarios); $i++){
            if($i == 0){
                $to_emails .= $usuarios[$i]->email;
            }
            else{
                $to_emails .= ', ' . $usuarios[$i]->email;
            }
        }

        return $to_emails;
    }

    
}
