<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipModulesDiplomados extends Model
{
    protected $table = 'relationship_modules_diplomados';

    public function modules()
    {
        return $this->hasMany('App\Models\Module','id','modulo_id');
    }

}
