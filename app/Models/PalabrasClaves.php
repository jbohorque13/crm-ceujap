<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PalabrasClaves extends Model
{
    protected $table = 'palabras_claves';

    public static function dropdown()
    {
        return PalabrasClaves::lists('nombre','id');
    }
    public static function active_all()
    {
        return PalabrasClaves::get();
    }
}
