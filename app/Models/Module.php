<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    public static function dropdown()
    {
        return Module::lists('codigo','id');
    }
    public static function active_all()
    {
        return Module::get();
    }

}
