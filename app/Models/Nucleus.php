<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nucleus extends Model
{
    protected $table = 'nucleus';

    public function diplomado()
    {
        return $this->hasMany('App\Models\RelationshipNucleusDiplomados','id','diplomado_id');
    }
    public static function dropdown()
    {
        return Nucleus::whereEstatus(1)->lists('nombre_sede','id');
    }
    public static function active_all()
    {
        return Nucleus::whereEstatus(1)->get();
    }
}
