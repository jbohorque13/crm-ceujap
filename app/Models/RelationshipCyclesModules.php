<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipCyclesModules extends Model
{
    protected $table = 'relationship_cycles_modules';
}
