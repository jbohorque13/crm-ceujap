<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class ResultsQuestionnaire extends Model
{
    protected $table = 'results_questionnaires';
     
    public function diplomado()
    {
        return $this->belongsTo('App\Models\Diplomado','diplomado_id','id');
    }
    public function facilitador()
    {
        return $this->belongsTo('App\Models\Facilitador','facilitador_id','id');
    }
	public function modulo()
    {
        return $this->belongsTo('App\Models\Module','modulo_id','id');
    }

    public function nucleo()
    {
        return $this->belongsTo('App\Models\Nucleus','nucleo_id','id');
    }


	public static function facilitadores()
    {
        $facilitadores = DB::table('facilitators')
                ->join('results_questionnaires', function($join) {
                    $join->on('facilitators.id', '=', 'results_questionnaires.facilitador_id');
                })        
                ->get();
    }

   /* public static function facilitadores()
    {
        $facilitadores = DB::table('facilitators')
                ->join('results_questionnaires', function($join) {
                    $join->on('facilitators.id', '=', 'results_questionnaires.facilitador_id');
                })        
                ->get();
    }
*/
}
