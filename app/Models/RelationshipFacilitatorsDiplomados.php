<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipFacilitatorsDiplomados extends Model
{
    protected $table = 'relationship_facilitators_diplomados';

    public function facilitators()
    {
        return $this->hasMany('App\Models\Facilitador','id','facilitador_id');
    }
}
