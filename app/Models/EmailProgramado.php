<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailProgramado extends Model
{
    protected $table = 'emails_programmers';
    
    public static function upload_image($image, $email_id){
        
        if(isset($image["imagen_uno"]["name"]) && $image["imagen_uno"]["name"] != '')
            {

                $tmp_name = $image["imagen_uno"]["tmp_name"];

                $name = str_replace(' ', '_', $image["imagen_uno"]["name"]);

                $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                $name = time().'_'.$name; 

                $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/emails/'.$email_id;

                if(!file_exists($filename))
                {
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/emails/'.$email_id, 0777, true);
                }

                if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')   
                {            
                    if(move_uploaded_file($tmp_name, "images/emails/".$email_id."/".$name))
                    {
                        $email_last          = EmailProgramado::find($email_id);
                        $email_last->imagen_uno = "images/emails/".$email_id."/".$name;
                        if($email_last->save()){
                            return true;
                        }
                        else{
                            return false; 
                        }
                    }
                    else{
                        return false;
                    }
                }
                else
                {
                    return false;
                    Session::flash('msg-error-imagen'. 'El formato que intentaste subir de imagen no es valido');
                }
            }
            
    }
    
    public static function active_all()
    {
        return EmailProgramado::whereEstatus(1)->get(); 
    }
}
