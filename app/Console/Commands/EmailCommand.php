<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Http\Helpers\EmailHelper;
use Session;
use App\Models\EmailProgramado;

class EmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    
    protected $signature = 'command:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de correo masivos cada cierto tiempo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = EmailProgramado::find(6);
//        foreach($emails as $row_email){
//            $row_email->diplomados = 1;
//            $row_email->save();
//        }
        $email->diplomados = 1;
        $email->save();
        
        $this->info('el diplomado fue cambiado');   
        
    }
}
